#include "pch.h"
#include "WebInterface.h"
#include "JsonFuctions.h"
#include <iostream>
#include <vector>
#include "ConfigurationPlan.h"
#include "AStar.h"
#include "GuidePointGenerator.h"
#include "RoutePointGenerator.h"
#include "Classes.h"
#include "Calculate.h"
#include "MotionSimulation.h"
#include <cstring>
#include <unordered_map>
#include "CLogFile.h"
#ifdef  _DEBUG  
#include <fstream>
//cout << "get-json" << json << endl;
#endif

using namespace std;

//************************************
// Method:    GenerateIntelligentLayoutPlan
// FullName:  GenerateIntelligentLayoutPlan
// Access:    public 
// Returns:   char * 输入的json数据
// Qualifier:智能布列方案接口
// Parameter: char * info	结果数据-json格式
//************************************
void GenerateIntelligentLayoutPlan(char *info, char** outdata)
{
	
	//通过静态方法获取日志对象
	auto& logfile = *GetLogObject();
	
	logfile.Write("开始调用GenerateIntelligentLayoutPlan。\n");
	std::string info_str(info);
	
	//气垫船信息
	vector<hovercraft_info> Hovercraft;
	//障碍物载具信息
	std::vector<Car_Info>  obstacles_info;
	//目标载具编号与类型
	std::vector < std::vector<std::vector<int>>> cars_info;
	//载具编号-类型映射
	std::unordered_map<int, int> cars_id_type;

	//长度单位 px/m
	double len_unit;
	//输出的布列方案
	std::vector<Layout_Plan_End> layout_plan;
	//各个批次的布列方案
	std::vector < std::vector<Layout_Plan_End>> batch_layout_plan;
	logfile.Write("获取json数据：\n %s \n", info);
	logfile.Write("开始解析json数据。\n");
	
	//分批次对载具进行布列
	//1、分批次获取载具信息
	
	if (info != nullptr && strlen(info) != 0)
		//解析json数据
		ParseJsonForLayoutPlan(info_str,obstacles_info, cars_info, len_unit, cars_id_type);
	logfile.Write("结束解析json数据。\n");
	logfile.Write("获取输入：目标载具批次总数（%d），障碍物个数( %d )，长度单位px/m（%f）。\n", cars_info.size(), obstacles_info.size(), len_unit);
	//2、对当前批次进行布列
	//调用接口，获取布列方案
	logfile.Write("开始生成布列方案。\n");
	for (int i = 0; i < cars_info.size(); ++i) {
		auto cur_batch_layout_plan = ConfigurationPlanCreation(cars_info[i], Hovercraft, obstacles_info);
		
		if (cur_batch_layout_plan.empty()) {
			//如果任意批次的载具无法布列，则总的布列方案也为空，表示布列失败
			layout_plan.clear();
			batch_layout_plan.clear();
			break;
		}
		batch_layout_plan.push_back(cur_batch_layout_plan);
		//3、更新障碍物信息，将布列好的载具当做为障碍物
		for (auto vihicle : cur_batch_layout_plan) {
			Car_Info car;
			if (cars_id_type[vihicle.vehicle_id] == 2)
			{
				car.car_length = 9.6;
				car.car_width = 3.4;
			}
			else
			{
				car.car_length = 8.2;
				car.car_width = 3.4;
			}
			car.car_center.x = vihicle.end_coordinates_x;
			car.car_center.y = vihicle.end_coordinates_y;
			car.angle = vihicle.direction;
			obstacles_info.push_back(car);
		}
	}
	logfile.Write("结束生成布列方案。\n");
	//4、整合各个批次的布列方案
	int series_number_offset = 0;
	for (auto& cur_layout_plan : batch_layout_plan) {
		for (auto& vihicle : cur_layout_plan) {
			vihicle.series_number += series_number_offset;
			layout_plan.push_back(vihicle);
		}
		series_number_offset += cur_layout_plan.size();
	}

#ifdef  _DEBUG  
	//测试
	//将障碍物与此时载具的位置存储，并输出到文件中
	FILE* fd = NULL;
	if ((fd = fopen("ConfigurationPlan.csv", "wt+")) != NULL)
	{
		for (auto& end_point : layout_plan)
		{
			//获取坐标以及车长、宽
			PPoint _center(end_point.end_coordinates_x, end_point.end_coordinates_y);
			vector<PPoint> points(4);
			double length_tmp = 0, width_tmp = 0;
		
			if (cars_id_type[end_point.vehicle_id] == 2)
			{
				length_tmp = 9.6;
				width_tmp = 3.4;
			}
			else
			{
				length_tmp = 8.2;
				width_tmp = 3.4;
			}

			//根据中心点坐标获得边界点坐标并存储
			CalculateCarBoundaryPoint(length_tmp, width_tmp, _center, end_point.direction, points[0], points[1], points[2], points[3]);
			fprintf(fd, "%f,%f,%f,%f,%f\n", points[0].x, points[1].x, points[2].x, points[3].x, points[0].x);
			fprintf(fd, "%f,%f,%f,%f,%f\n", points[0].y, points[1].y, points[2].y, points[3].y, points[0].y);
		}
	}
	fclose(fd);
	fd = NULL;
#endif


	//结束
	//将结果打包为json数据
	logfile.Write("开始打包json数据。\n");
	string result_json=CreateJsonForLayoutPlan(layout_plan, len_unit);
	logfile.Write("结束打包json数据。\n");
	const char * result_char = result_json.c_str();
	logfile.Write("输出json数据：\n %s \n", result_char);
	if(outdata !=nullptr)
		strcpy(*outdata, result_char);
	logfile.Write("结束调用GenerateIntelligentLayoutPlan。\n");

}
//************************************
// Method:    GenerateIntelligentPathPlan
// FullName:  GenerateIntelligentPathPlan
// Access:    public 
// Returns:   char *	输入的json数据
// Qualifier:智能路径规划接口
// Parameter: char * info	输入的json数据
//************************************
void GenerateIntelligentPathPlan(char *info, char** outdata)
{
	//通过静态方法获取日志对象
	auto& logfile = *GetLogObject();
	logfile.Write("开始调用GenerateIntelligentPathPlan。\n");
	std::string info_str(info);
	//障碍物载具信息
	std::vector<Car_Info>  obstacles_info;
	//目标载具基本信息
	Ipp_Input car_info;
	//长度单位 px/m
	double len_unit;

	//计算出的路径导引点
	std::vector<PPoint> guide_points;
	//花费的时间与路径状态
	int path_status=1;
	double cost_time=0;
	logfile.Write("获取json数据：\n %s \n", info);
	logfile.Write("开始解析json数据。\n");
	if (info != nullptr && strlen(info) != 0)
		//解析json数据
		ParseJsonForIntelligentPathPlan(info_str,obstacles_info, car_info, len_unit);
	logfile.Write("结束解析json数据。\n");
	logfile.Write("获取输入：目标载具类型( %d )、起点方向（ %f ）、终点方向（ %f ）、速度（ %f ）、起点坐标（ %f, %f ）、终点坐标（ %f, %f ）。\n", 
		car_info.target_vehicle_type,car_info.start_direction,car_info.end_direction,car_info.speed,car_info.start.x, car_info.start.y, car_info.end.x, car_info.end.y);
	logfile.Write("获取输入：障碍物个数( %d )，长度单位px/m（%f）。\n", obstacles_info.size(), len_unit);
	//调用接口,计算
	logfile.Write("开始使用A星算法路径搜索器生成栅格路径。\n");
	//使用A星算法路径搜索器生成栅格路径
	Astar astar(92, 428, obstacles_info);
	double car_width = 0, car_length = 0;
	//设置中心点栅格与边界栅格的距离
	if (car_info.target_vehicle_type == 2)
	{
		astar.SetCarInfo(23, 23, 8, 9);
		car_width = 3.6;
		car_length = 9.4;
	}
	else if(car_info.target_vehicle_type == 3)
	{
		astar.SetCarInfo(19, 20, 8, 8);
		car_width = 3.4;
		car_length = 8;
	}
	else if (car_info.target_vehicle_type == 4)
	{
		astar.SetCarInfo(23, 24, 7, 8);
		car_width = 3.2;
		car_length = 9.6;
	}
	//设置与障碍物的安全栅格距离
	astar.SetCollisionRadius(2);
	//获取起点与终点,并将将起点与终点的直角坐标转换为栅格坐标
	auto start = PPointToPoint(car_info.start);
	auto end = PPointToPoint(car_info.end);
	//A*算法找寻路径
	list<Point> path;
	path = astar.GetPath(start, end, false);
	logfile.Write("结束使用A星算法路径搜索器生成栅格路径。\n");
	logfile.Write("栅格路径的栅格个数：%d。\n",path.size());

	//找不到栅格路径，标记路径不可用
	if (path.empty())
	{
		path_status = 1;
		//如果找不到栅格路径，输出当前地图
		logfile.Write("起点栅格：（%d, %d） ，终点栅格：（%d , %d）。\n", start.x,start.y,end.x,end.y);
#ifdef _DEBUG
		astar.printPathToGraphInCSV(path, start, end);
#endif	
	}		
	else
	{
		//使用导引点生成器生成导引点序列
		GuidePointGenerator gpg(obstacles_info, car_length, car_width);
		gpg.GenerateGuidePointsByPathSearch(start, end, path);
		if (gpg.GetPathStatus())
		{
			//路径可用，设置相应的信息
			path_status = 2;
			
			//获取导引点数组
			auto tmp= gpg.GetGuidePoints();
			for (auto& p : tmp) {
				guide_points.push_back(p);
			}
			logfile.Write("直角坐标系下的路径导引点个数：%d。\n", guide_points.size());
			//求花费时间
			//路线点生成
			RoutePointGenerator rpg(obstacles_info, car_length, car_width, car_info.speed, 1.5);
			rpg.SetGuidePoints(guide_points, car_info.start_direction, car_info.end_direction);
			rpg.GenerateRoutePoints();
			auto counts = rpg.GetCountsOfRoutePoints();
			//计算时间 路径点个数*时间单位
			cost_time = counts * 0.1;
#ifdef _DEBUG
			rpg.PrintRoutePointsToGSV();
#endif				
		}
		else
		{
			//如果发生了碰撞，输出当前的碰撞情况
			//路线生成
			//路线点生成
			//RoutePointGenerator rpg(obstacles_info, car_length, car_width, car_info.speed, 1.5);
			//rpg.SetGuidePoints(gpg.GetGuidePoints(), car_info.start_direction, car_info.end_direction);
			//rpg.GenerateRoutePoints();
			//rpg.GetBadGuidePoints();
			////输出数据到
			//rpg.PrintRectangleOfObstaclesToGSV();
			//rpg.PrintRoutePointsToGSV();
			//rpg.PrintRectangleOfCollisionToGSV();
			path_status = 1;
		}
			
	}
	logfile.Write("生成路径导引点完成，路径状态为：%d。(1-不可用 2-可用)\n", path_status);
	//将结果打包为json数据
	logfile.Write("开始打包json数据。\n");
	string result_json=CreateJsonForIntelligentPathPlan(guide_points,path_status,cost_time, len_unit);
	logfile.Write("结束打包json数据。\n");
#ifdef  _DEBUG  
	fstream json_data("json_path.txt");
	if (json_data.is_open())
	{
		json_data << result_json;
	}
	json_data.close();
	//cout << result_json;
#endif
	const char * result_char = result_json.c_str();
	logfile.Write("输出json数据：\n %s \n", result_char);
	if (outdata != nullptr)
		strcpy(*outdata, result_char);
	logfile.Write("结束调用GenerateIntelligentPathPlan。\n");
}
//************************************
// Method:    ArtificialPathPlanCheck
// FullName:  ArtificialPathPlanCheck
// Access:    public 
// Returns:   char *	输入的json数据
// Qualifier:人工路径规划检查接口
// Parameter: char * info	输入的json数据
//************************************
void ArtificialPathPlanCheck(char *info, char** outdata)
{
	//通过静态方法获取日志对象
	auto& logfile = *GetLogObject();
	logfile.Write("开始调用ArtificialPathPlanCheck。\n");
	std::string info_str(info);
	//障碍物载具信息
	std::vector<Car_Info>  obstacles_info;
	//目标载具导引点数组
	std::vector<PPoint> guide_points;
	//目标载具基本信息
	Ipp_Input car_info;
	//长度单位 px/m
	double len_unit;

	//计算出的会碰撞的路径导引点序号数组
	std::vector<int> bad_guide_points;
	//花费的时间与路径状态
	int path_status = 1;
	double cost_time = 0;
	logfile.Write("获取json数据：\n %s \n", info);
	logfile.Write("开始解析json数据。\n");
	if (info != nullptr && strlen(info) != 0)
		//解析json数据
		ParseJsonForPathPlanCheck(info_str, obstacles_info, guide_points,car_info, len_unit);
	logfile.Write("结束解析json数据。\n");
	logfile.Write("获取输入：目标载具类型( %d )、起点方向（ %f ）、终点方向（ %f ）、速度（ %f ）、起点坐标（ %f, %f ）、终点坐标（ %f, %f ）。\n",
		car_info.target_vehicle_type, car_info.start_direction, car_info.end_direction, car_info.speed, car_info.start.x, car_info.start.y, car_info.end.x, car_info.end.y);
	logfile.Write("获取输入：障碍物个数( %d )，长度单位px/m（%f）。\n", obstacles_info.size(), len_unit);
	//调用接口,计算
	logfile.Write("开始使用路线点生成器生成路线点。\n");
	double car_width = 0, car_length = 0;
	if (car_info.target_vehicle_type == 2)
	{
		car_width = 3.4;
		car_length = 9.6;
	}
	else
	{
		car_width = 3.4;
		car_length = 8.2;
	}

	//路线点生成
	RoutePointGenerator rpg(obstacles_info, car_length, car_width, car_info.speed, 1.5);
	rpg.SetGuidePoints(guide_points, car_info.start_direction, car_info.end_direction);
	rpg.GenerateRoutePoints();
	if (rpg.GetPathStatus())
	{
		//路径可用，设置相应的信息
		path_status = 2;		
		//求花费时间
		auto counts = rpg.GetCountsOfRoutePoints();
		//计算时间 路径点个数*时间单位
		cost_time = counts * 0.1;
	}
	else
	{
		path_status = 1;
		//获取所有碰撞导引点的序号
		auto points = rpg.GetBadGuidePoints();
		for (auto& p : points)
		{
			auto ite = find(guide_points.begin(), guide_points.end(),p);
			if(ite != guide_points.end())
				bad_guide_points.push_back(ite - guide_points.begin() + 1);
		}

	}
	logfile.Write("结束使用路线点生成器生成路线点。\n");
	logfile.Write("获取路线碰撞状态（0-碰撞 1-不碰撞）：%d。\n", rpg.GetPathStatus());
	//将结果打包为json数据
	logfile.Write("开始打包json数据。\n");
	string result_json = CreateJsonForPathPlanCheck(bad_guide_points, path_status, cost_time, len_unit);
	logfile.Write("结束打包json数据。\n");
#ifdef  _DEBUG  
	fstream json_data("json_path.txt");
	if (json_data.is_open())
	{
		json_data << result_json;
	}
	json_data.close();
	//cout << result_json;
#endif
	const char * result_char = result_json.c_str();
	logfile.Write("输出json数据：\n %s \n", result_char);
	if (outdata != nullptr)
		strcpy(*outdata, result_char);
	logfile.Write("结束调用ArtificialPathPlanCheck。\n");
}
//************************************
// Method:    Vehicle_Simulation_Demonstration
// FullName:  Vehicle_Simulation_Demonstration
// Access:    public	输入的json数据
// Returns:   char *
// Qualifier:单个载具仿真演示接口
// Parameter: char * info	输入的json数据
//************************************
void  Vehicle_Simulation_Demonstration(char *info, char** outdata)
{
	//通过静态方法获取日志对象
	auto& logfile = *GetLogObject();
	logfile.Write("开始调用Vehicle_Simulation_Demonstration。\n");
	if (info == nullptr) return;
	std::string info_str(info);
	//障碍物载具信息
	std::vector<Car_Info>  obstacles_info;
	//目标载具导引点数组
	std::vector<PPoint> guide_points;
	//目标载具基本信息
	Ipp_Input car_info;
	//长度单位 px/m
	 double len_unit;

	//路线点数组
	std::vector<RoutePoint>  route_points;
	//碰撞路线点序号
	size_t first_collison_id = 0;


	//若传入的参数不为空表示第，此时才解析输入参数
	if (strcmp(info, "null")!=0 && strlen(info) != 0 && strcmp(info, "{}") != 0)
	{
		logfile.Write("获取json数据：\n %s \n", info);
		logfile.Write("开始解析json数据。\n");
		//解析json数据
		ParseJsonForSimulation_Demonstration(info_str, obstacles_info, guide_points, car_info, len_unit);
		logfile.Write("结束解析json数据。\n");
		logfile.Write("获取输入：目标载具类型( %d )、起点方向（ %f ）、终点方向（ %f ）、速度（ %f ）。\n",
			car_info.target_vehicle_type, car_info.start_direction, car_info.end_direction, car_info.speed);
		logfile.Write("获取输入：障碍物个数( %d )，导引点个数（%d） ,长度单位px/m（%f） 。\n", obstacles_info.size(),guide_points.size(), len_unit);

	}


	//生成所有的路径点
	//如果没有输入的数据，不做任何计算
	if ( guide_points.empty() || car_info.target_vehicle_type == 0)
		;
	else
	{
		//坞舱路线点计算

		logfile.Write("开始计算路径点。\n");

	//车辆舱路线点计算
	//调用接口,计算
		double car_width = 0, car_length = 0;
		if (car_info.target_vehicle_type == 2)
		{
			car_width = 3.6;
			car_length = 9.4;
		}
		else if (car_info.target_vehicle_type == 3)
		{
			car_width = 3.4;
			car_length = 8;
		}
		else if (car_info.target_vehicle_type == 4)
		{
			car_width = 3.2;
			car_length = 9.6;
		}

		//路线点生成
		RoutePointGenerator rpg(obstacles_info, car_length, car_width, car_info.speed, 1.5);
		rpg.SetGuidePoints(guide_points, car_info.start_direction, car_info.end_direction);
		rpg.GenerateRoutePoints();
		route_points = rpg.GetRoutePoints();
		//根据路线点是否碰撞获得碰撞点序号
		if (rpg.GetPathStatus() == false)
			first_collison_id = rpg.GetFirstCollisonRoutePointId();
	}

	logfile.Write("将结果打包为json数据。\n");
	//将结果打包为json数据
	string result_json = CreateJsonForSimulation_Demonstration(route_points, first_collison_id, len_unit);
	const char * result_char = result_json.c_str();
	/*logfile.Write("输出json数据：\n %s \n", result_char);*/
	logfile.Write("当前路线点数组大小：( %d )。\n", route_points.size());
	if (outdata != nullptr)
		strcpy(*outdata, result_char);
	
	logfile.Write("结束调用Vehicle_Simulation_Demonstration。\n");
}
//************************************
// Method:    Vehicle_Scheduling_Simulation
// FullName:  Vehicle_Scheduling_Simulation
// Access:    public 
// Returns:   char *	输入的json数据
// Qualifier:载具调度仿真接口
// Parameter: char * info	输入的json数据
//************************************
void  Vehicle_Scheduling_Simulation(char *info, char** outdata)
{
	//通过静态方法获取日志对象
	auto& logfile = *GetLogObject();
	logfile.Write("开始调用Vehicle_Simulation_Demonstration。\n");
	std::string info_str(info);
	//障碍物载具信息
	static std::vector<Car_Info>  obstacles_info;
	//所有目标载具的导引点数组
	static  std::vector<Car_Guide_Points> all_guide_points;
	//所有目标载具基本信息
	static std::vector<Ipp_Input> all_cars_info;
	//目标载具的编号
	static  std::vector<int> vehicles_id;
	//长度单位 px/m
	static double len_unit = 1;


	//路线点序号,自增
	static size_t  route_point_id = 1;
	//载具编号，自变
	static int vehicle_id = 0;
	//接口调用计数，用于计算花费时间
	static int time_counts = 0;
	//已调度载具计数，用于计算分数
	static int vehicle_counts = 0;
	//当前装备类型
	static int cur_vehicle_type = 2;
	//成绩
	double grade = 0;
	//花费时间
	double time_used = 0;
	//存储所有载具的路线点
	static std::vector<Info_Route_Points> all_route_points_info;


	//路线点状态
	int route_point_status = 2;
	//路线点坐标
	PPoint coordinate;
	//路线点方向
	double direction = 0;
	//若传入的参数不为空表示第一次调用接口，此时才解析输入参数
	if (strcmp(info, "null") != 0 && strlen(info) != 0 && strcmp(info, "{}") != 0)
	{
		logfile.Write("清除参数。\n");
		//只要有输入，则表示重新请求数据，首先清除所有的数据
		std::vector<Car_Info> tmp1;
		obstacles_info.clear();
		obstacles_info.swap(tmp1);

		std::vector<Car_Guide_Points> tmp2;
		all_guide_points.clear();
		all_guide_points.swap(tmp2);

		std::vector<Ipp_Input> tmp3;
		all_cars_info.clear();
		all_cars_info.swap(tmp3);

		static  std::vector<int> tmp4;
		vehicles_id.clear();
		vehicles_id.swap(tmp4);

		len_unit = 1;

		route_point_id = 1;
		vehicle_id = 0;
		time_counts = 0;
		vehicle_counts = 0;
		std::vector<Info_Route_Points> tmp5;
		all_route_points_info.clear();
		all_route_points_info.swap(tmp5);
		cur_vehicle_type = 2;

		

		//解析数据
		logfile.Write("获取json数据：\n %s \n", info);
		logfile.Write("开始解析json数据。\n");
		//解析json数据
		ParseJsonForScheduling_Simulation(info_str, obstacles_info, all_guide_points, all_cars_info, vehicles_id, len_unit);
		logfile.Write("结束解析json数据。\n");
		logfile.Write("获取输入：目标载具个数( %d )，目标载具导引点序列个数（%d）。\n", all_cars_info.size(), all_guide_points.size());
		logfile.Write("获取输入：障碍物个数( %d )，长度单位px/m（%f）。\n", obstacles_info.size(), len_unit);
	}
		
	
	
	
	//计数增加1
	time_counts++;
	if(!all_route_points_info.empty())
		logfile.Write("当前载具碰撞路线点编号： %d ,当前载具路线点个数： %d，路线数组的大小： %d\n",all_route_points_info.front().first_collison_id, all_route_points_info.front().route_points.size(),all_route_points_info.size() );
	else
		logfile.Write("所有载具的路线点数组 all_route_points_info 为空！\n");

	logfile.Write("当前载具编号： %d ，当前路线点序号： %d 。\n", vehicle_id, route_point_id);
	logfile.Write("接口调用计数： %d 。（计数 * 0.1s为总时间）\n", time_counts);
	logfile.Write("已调度载具计数： %d ，所有载具个数： %d （两者之比为分数）。\n", vehicle_counts, vehicles_id.size());
	//根据route_point_id判断是否需要计算vehicle_id载具的路径点序列
	if (route_point_id == 1)
	{
		//如果没有输入的数据，设置状态为4
		if (obstacles_info.empty() || all_guide_points.empty() || all_cars_info.empty() || vehicles_id.empty())
			route_point_status = 4;
		else
		{
			//获得vehicle_id的信息
		//获得vehicle_id的下标
		//如果是第一次调用，取第一个载具的编号
			if (vehicle_id == 0)
				vehicle_id = vehicles_id.front();
			auto ite = find(vehicles_id.begin(), vehicles_id.end(), vehicle_id);
			if (ite != vehicles_id.end())
			{
				auto v_id = ite - vehicles_id.begin();
				//利用下标获取信息
				Ipp_Input car_info = all_cars_info.at(v_id);
				auto guide_points = all_guide_points.at(v_id).guide_points;

				int first_collison_id = 0;
				double car_width = 0, car_length = 0;

				//设置当前载具的类型
				cur_vehicle_type = car_info.target_vehicle_type;

				if (car_info.target_vehicle_type == 2)
				{
					car_width = 3.4;
					car_length = 9.6;
				}
				else
				{
					car_width = 3.4;
					car_length = 8.2;
				}

				//路线点生成
				RoutePointGenerator rpg(obstacles_info, car_length, car_width, car_info.speed, 1.5);
				rpg.SetGuidePoints(guide_points, car_info.start_direction, car_info.end_direction);
				rpg.GenerateRoutePoints();
				Info_Route_Points tem;
				//根据路线点是否碰撞获得碰撞点序号
				if (rpg.GetPathStatus() == false)
					first_collison_id = rpg.GetFirstCollisonRoutePointId();
				tem.vehicle_id = vehicle_id;
				tem.first_collison_id = first_collison_id;
				tem.route_points = rpg.GetRoutePoints();
				all_route_points_info.push_back(tem);
			}
		}
		
	}
	//当前载具对应的路线位于路线数组的位置，即下标
	size_t i = 0;
	//路线点存在
	if (route_point_status != 4)
	{
		//根据vehicle_id提取路线点序列
		for (; i < all_route_points_info.size(); ++i)
		{
			//找到vehicle_id对应的信息
			if (all_route_points_info.at(i).vehicle_id == vehicle_id)
			{
				auto info = all_route_points_info.at(i);
				//若该路线发生碰撞
				if (info.first_collison_id != 0)
				{
					if (route_point_id < info.first_collison_id)
						route_point_status = 2;
					else if (route_point_id == info.first_collison_id)
						route_point_status = 1;
				}
				else//没有碰撞
				{
					if (route_point_id < info.route_points.size())
						route_point_status = 2;
					else if (route_point_id == info.route_points.size())
						route_point_status = 3;
				}
				//路线点存在时才设置对应的数据
				if ((route_point_id - 1) < info.route_points.size())
				{
					coordinate = info.route_points.at(route_point_id - 1).coordinate;
					direction = info.route_points.at(route_point_id - 1).angle_z;
				}
				//退出循环
				break;
			}
		}
	}
	
	//如果当前载具到达终点，即状态为3，判断此时是否已经调度完成了，如果完成了则设状态为5
	if (route_point_status == 3)
	{
		auto ite = find(vehicles_id.begin(), vehicles_id.end(), vehicle_id);
		if ((vehicles_id.end() - ite) == 1)
			route_point_status = 5;
	}

	//当载具碰撞或者调度完成时，即状态为1、5时，计算分数与时间
	if (route_point_status == 1 || route_point_status == 5)
	{
		time_used = time_counts * 0.1;
		grade = static_cast<double>(vehicle_counts) / static_cast<double>(vehicles_id.size());
	}
	//将结果打包为json数据
	string result_json = CreateJsonForScheduling_Simulation(vehicle_id, route_point_id, route_point_status, coordinate, direction,grade,time_used, len_unit, cur_vehicle_type);
	const char * result_char = result_json.c_str();
	logfile.Write("输出json数据：\n %s \n", result_char);
	if (outdata != nullptr)
		strcpy(*outdata, result_char);
	

	//更新接口内部状态
	//如果发生碰撞，即路线点的状态为1，或路线点不存在，清除所有的静态信息，同时将所有的状态值都复位
	if (route_point_status == 1 || route_point_status == 4)
	{
		//清除数据
		std::vector<Car_Info> tmp1;
		obstacles_info.swap(tmp1);

		std::vector<Car_Guide_Points> tmp2;
		all_guide_points.swap(tmp2);

		std::vector<Ipp_Input> tmp3;
		all_cars_info.swap(tmp3);

		static  std::vector<int> tmp4;
		vehicles_id.swap(tmp4);

		len_unit = 1;

		route_point_id = 1;
		vehicle_id = 0;
		time_counts = 0;
		vehicle_counts = 0;
		std::vector<Info_Route_Points> tmp5;
		all_route_points_info.swap(tmp5);
		cur_vehicle_type = 2;
	}
	//如果当前载具到达终点
	if (route_point_status == 3)
	{
		//删除对应的路径点序列信息
		all_route_points_info.erase(all_route_points_info.begin() + i);
		auto ite = find(vehicles_id.begin(), vehicles_id.end(), vehicle_id);
		
		//将当前载具设置为障碍物，即在障碍物数组中增加位于终点位置的当前载具
		Car_Info new_obstacle;
		//提取当前载具在载具数组中的下标
		auto v_id = ite - vehicles_id.begin();
		//利用下标获取当前载具信息
		Ipp_Input car_info = all_cars_info.at(v_id);
		new_obstacle.angle = car_info.end_direction;
		new_obstacle.car_center = car_info.end;
		if (car_info.target_vehicle_type == 2)
		{
			new_obstacle.car_width = 3.4;
			new_obstacle.car_length = 9.6;
		}
		else
		{
			new_obstacle.car_width = 3.4;
			new_obstacle.car_length = 8.2;
		}
		//添加障碍物
		obstacles_info.push_back(new_obstacle);


		//更新vehicle_id、route_point_id和vehicle_counts
		//当前载具为最后一个载具
		if ((vehicles_id.end() - ite) == 1)
			route_point_status = 5;
		else
		{
			//改变vehicle_id 为下一载具的id，同时将route_point_id置为1
			vehicle_id = *(ite + 1);
			route_point_id = 1;
			vehicle_counts++;
		}

	}

	if (route_point_status == 2) route_point_id++;
	//如果调度完成，清除所有的静态信息，同时将所有的状态值都复位
	if (route_point_status == 5)
	{
	
		std::vector<Car_Info> tmp1;
		obstacles_info.swap(tmp1);

		std::vector<Car_Guide_Points> tmp2;
		all_guide_points.swap(tmp2);

		std::vector<Ipp_Input> tmp3;
		all_cars_info.swap(tmp3);

		static  std::vector<int> tmp4;
		vehicles_id.swap(tmp4);

		len_unit = 1;

		route_point_id = 1;
		vehicle_id = 0;
		time_counts = 0;
		vehicle_counts = 0;
		std::vector<Info_Route_Points> tmp5;
		all_route_points_info.swap(tmp5);
		cur_vehicle_type = 2;
	}
	logfile.Write("结束调用Vehicle_Scheduling_Simulation。\n");
}


//************************************
// Method:    VehicleLocationRequest
// FullName:  VehicleLocationRequest
// Access:    public 
// Returns:   void
// Qualifier:车辆调度单个位置请求接口
// Parameter: char * info
// Parameter: char * * outdata
//************************************
void VehicleLocationRequest(char* info, char** outdata) {
	//通过静态方法获取日志对象
	auto& logfile = *GetLogObject();
	//logfile.Write("开始调用VehicleLocationRequest。\n");
	if (info == nullptr) return;
	std::string info_str(info);
	//障碍物载具信息
	std::vector<Car_Info>  obstacles_info;
	//目标载具导引点数组
	std::vector<PPoint> guide_points;
	//目标载具基本信息
	Ipp_Input car_info;
	//目标载具编号
	int vihicle_id = 0;
	static int old_vihicle_id = 0;
	//路线点编号
	int coordinate_id = 0;
	//路线点状态
	int route_point_status = 4;
	//路线点数组
	static std::vector<RoutePoint>  route_points;
	//碰撞路线点序号
	static size_t first_collison_id = 0;

	string result_json;


	//若传入的参数不为空表示第，此时才解析输入参数
	if (strcmp(info, "null") != 0 && strlen(info) != 0 && strcmp(info, "{}") != 0)
	{
		//logfile.Write("获取json数据：\n %s \n", info);
		//logfile.Write("开始解析json数据。\n");
		//解析json数据
		ParseJsonForVehicleLocationRequest(info_str, obstacles_info, guide_points, car_info, vihicle_id, coordinate_id);
		//logfile.Write("结束解析json数据。\n");
		if (coordinate_id == 1) {
			logfile.Write("VehicleLocationRequest获取输入：目标载具类型( %d )、起点方向（ %f ）、终点方向（ %f ）、速度（ %f ）。\n",
				car_info.target_vehicle_type, car_info.start_direction, car_info.end_direction, car_info.speed);
			logfile.Write("VehicleLocationRequest获取输入：障碍物个数( %d )，导引点个数（%d） ,载具序号（%d），路径点序号（%d） ，碰撞路径点序号（%d）。\n", obstacles_info.size(), guide_points.size(), vihicle_id, coordinate_id, first_collison_id);
			logfile.Write("VehicleLocationRequest当前路线点数组大小：( %d )。\n", route_points.size());
		}
	}
	//载具id为0表示清除静态局部变量的数据
	if (vihicle_id == 0) {
		first_collison_id = 0;
		old_vihicle_id = 0;
		route_points.clear();
		route_point_status = 4;
		result_json = CreateJsonForVehicleLocationRequest(route_points, route_point_status, coordinate_id);
#ifdef  _DEBUG  
		cout << "result_json" << result_json << endl;
#endif
		const char * result_char = result_json.c_str();
		/*if (coordinate_id == 1) {
			logfile.Write("VehicleLocationRequest输出json数据：\n %s \n", result_char);
		}*/
		if (outdata != nullptr)
			strcpy(*outdata, result_char);
		return;

	}

	//判断是否是已经请求过的载具，如果已经请求过，则直接获取数据
	if (vihicle_id != 0 && vihicle_id == old_vihicle_id) {
		if (!route_points.empty() && coordinate_id <= route_points.size())
		{
			if (coordinate_id == route_points.size()) {
				route_point_status = 3;
			}
			else if (first_collison_id == 0) {
				route_point_status = 2;
			}
			else if (first_collison_id != 0 && coordinate_id < first_collison_id) {
				route_point_status = 2;
			}
			else if (first_collison_id != 0 && coordinate_id == first_collison_id) {
				route_point_status = 1;
			}
		}
	}
	if (route_point_status != 4) {
		result_json = CreateJsonForVehicleLocationRequest(route_points, route_point_status, coordinate_id);
#ifdef  _DEBUG  
		cout << "result_json" << result_json << endl;
#endif
		const char * result_char = result_json.c_str();
		/*if (coordinate_id == 1) {
			logfile.Write("VehicleLocationRequest输出json数据：\n %s \n", result_char);
		}*/
		if (outdata != nullptr)
			strcpy(*outdata, result_char);
		return;
	}

	//如果没有请求过当前载具的路线点,则重新生成所有的路径点
	//如果没有输入的数据，不做任何计算
	if (guide_points.empty() || car_info.target_vehicle_type == 0)
		;
	else
	{
		//坞舱路线点计算
		//logfile.Write("开始计算路径点。\n");
		//车辆舱路线点计算
		//调用接口,计算
		double car_width = 3.6, car_length = 9.4;
		if (car_info.target_vehicle_type == 2)
		{
			car_width = 3.6;
			car_length = 9.4;
		}
		else if (car_info.target_vehicle_type == 3)
		{
			car_width = 3.4;
			car_length = 8;
		}
		else if (car_info.target_vehicle_type == 4)
		{
			car_width = 3.2;
			car_length = 9.6;
		}

		//路线点生成
		RoutePointGenerator rpg(obstacles_info, car_length, car_width, car_info.speed, 1.5);
		rpg.SetGuidePoints(guide_points, car_info.start_direction, car_info.end_direction);
		rpg.GenerateRoutePoints();
		route_points = rpg.GetRoutePoints();
		//根据路线点是否碰撞获得碰撞点序号
		if (rpg.GetPathStatus() == false)
			first_collison_id = rpg.GetFirstCollisonRoutePointId();
		old_vihicle_id = vihicle_id;
	}

	//根据情况设定路线点状态
	if (coordinate_id == 0) {
		route_point_status = 4;
	}
	else if (coordinate_id == route_points.size()) {
		route_point_status = 3;
	}
	else {
		if (first_collison_id == 0) {
			if (coordinate_id < route_points.size()) {
				route_point_status = 2;
			}
		}
		else {
			if (coordinate_id < first_collison_id) {
				route_point_status = 2;
			}
			else if (coordinate_id == first_collison_id) {
				route_point_status = 1;
			}
		}

	}

	//将结果打包为json数据
	//logfile.Write("将结果打包为json数据。\n");
	result_json = CreateJsonForVehicleLocationRequest(route_points, route_point_status, coordinate_id);
#ifdef  _DEBUG  
	cout << "result_json" << result_json << endl;
#endif
	const char * result_char = result_json.c_str();
	if (coordinate_id == 1) {
		//logfile.Write("输出json数据：\n %s \n", result_char);
	}
	if (outdata != nullptr)
		strcpy(*outdata, result_char);

	//logfile.Write("结束调用VehicleLocationRequest。\n");
}


//************************************
// Method:    HovercraftPassivelyRequest
// FullName:  HovercraftPassivelyRequest
// Access:    public 
// Returns:   void
// Qualifier:气垫船牵引进舱单个位置请求接口
// Parameter: char * info
// Parameter: char * * outdata
//************************************
void HovercraftPassivelyRequest(char* info, char** outdata) {
	//通过静态方法获取日志对象
	auto& logfile = *GetLogObject();
	//logfile.Write("开始调用HovercraftPassivelyRequest。\n");
	if (info == nullptr) return;
	std::string info_str(info);

	//输入的信息
	int left_gear = 0, right_gear = 0;
	double cur_x = 0;

	
	string result_json;

	//若传入的参数不为空表示第，此时才解析输入参数
	if (strcmp(info, "null") != 0 && strlen(info) != 0 && strcmp(info, "{}") != 0)
	{
		//logfile.Write("获取json数据：\n %s \n", info);
		//logfile.Write("开始解析json数据。\n");
		//解析json数据
		ParseJsonForHovercraftPassivelyRequest(info_str, left_gear, right_gear, cur_x);
		//logfile.Write("结束解析json数据。\n");
		//logfile.Write("获取输入：start_x( %f )，左档位（%d），右档位（%d） 。\n", cur_x, left_gear, right_gear);
	}

	//计算
	auto towed_ship_info = Towing(left_gear,right_gear,cur_x);
	
	//将结果打包为json数据
	//logfile.Write("将结果打包为json数据。\n");
	//区分方向
	if (left_gear > right_gear) {
		towed_ship_info.angle = fabs(towed_ship_info.angle);
	}
	else {
		towed_ship_info.angle = 0 - fabs(towed_ship_info.angle);
	}
	result_json = CreateJsonForHovercraftPassivelyRequest(towed_ship_info.x, towed_ship_info.angle);
#ifdef  _DEBUG  
	cout << "result_json" << result_json << endl;
#endif
	const char * result_char = result_json.c_str();
	//logfile.Write("输出json数据：\n %s \n", result_char);
	if (outdata != nullptr)
		strcpy(*outdata, result_char);

	//logfile.Write("结束调用HovercraftPassivelyRequest。\n");
}

//************************************
// Method:    HovercraftInitiativeRequest
// FullName:  HovercraftInitiativeRequest
// Access:    public 
// Returns:   void
// Qualifier:气垫船自行进舱单个位置请求接口
// Parameter: char * info
// Parameter: char * * outdata
//************************************
void HovercraftInitiativeRequest(char* info, char** outdata){
	//通过静态方法获取日志对象
	auto& logfile = *GetLogObject();
	//logfile.Write("开始调用HovercraftPassivelyRequest。\n");
	if (info == nullptr) return;
	std::string info_str(info);

	//输入的信息
	double start_x, end_x;
	//目标载具编号
	int vihicle_id = 0;
	static int old_vihicle_id = 0;
	//路线点编号
	int coordinate_id = 0;
	//路线点状态
	int route_point_status = 4;
	//路线点数组
	static std::vector<RoutePoint>  route_points;

	string result_json;

	//若传入的参数不为空表示第，此时才解析输入参数
	if (strcmp(info, "null") != 0 && strlen(info) != 0 && strcmp(info, "{}") != 0)
	{
		//logfile.Write("获取json数据：\n %s \n", info);
		//logfile.Write("开始解析json数据。\n");
		//解析json数据
		ParseJsonForHovercraftInitiativeRequest(info_str, start_x, end_x, vihicle_id, coordinate_id);
		//logfile.Write("结束解析json数据。\n");
		//logfile.Write("获取输入：start_x( %f )，end_x（%f） ,载具序号（%d），路径点序号（%d） 。\n", start_x, end_x, vihicle_id, coordinate_id);
		//logfile.Write("当前路线点数组大小：( %d )。\n", route_points.size());
	}

	//判断是否是已经请求过的载具，如果已经请求过，则直接获取数据
	if (vihicle_id != 0 && vihicle_id == old_vihicle_id) {
		if (!route_points.empty() && coordinate_id <= route_points.size())
		{
			if (coordinate_id < route_points.size()) {
				route_point_status = 2;
			}
			else {
				route_point_status = 3;
			}
		}
	}
	if (route_point_status != 4) {
		result_json = CreateJsonForHovercraftInitiativeRequest(route_points, route_point_status, coordinate_id);
#ifdef  _DEBUG  
		cout << "result_json" << result_json << endl;
#endif
		const char * result_char = result_json.c_str();
		//logfile.Write("输出json数据：\n %s \n", result_char);
		if (outdata != nullptr)
			strcpy(*outdata, result_char);
		return;
	}
	else
	{
		//如果没有请求过当前载具的路线点,则重新生成所有的路径点
		route_points = GetRoutePointsOfHovercraft_Navigate(start_x, end_x, 16, 1);
		old_vihicle_id = vihicle_id;
	}

	//根据情况设定路线点状态
	if (coordinate_id == 0 || coordinate_id > route_points.size()) {
		route_point_status = 4;
	}
	else if (coordinate_id == route_points.size()) {
		route_point_status = 3;
	}
	else if (coordinate_id < route_points.size()) {
		route_point_status = 2;
	}

	//将结果打包为json数据
	//logfile.Write("将结果打包为json数据。\n");
	result_json = CreateJsonForHovercraftInitiativeRequest(route_points, route_point_status, coordinate_id);
#ifdef  _DEBUG  
	cout << "result_json" << result_json << endl;
#endif
	const char * result_char = result_json.c_str();
	//logfile.Write("输出json数据：\n %s \n", result_char);
	if (outdata != nullptr)
		strcpy(*outdata, result_char);

	//logfile.Write("结束调用Vehicle_Simulation_Demonstration。\n");
	}