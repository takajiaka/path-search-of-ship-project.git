#pragma once
#include <vector>
#include <iomanip>
#include "Classes.h"
//气垫船自行行驶进舱仿真路线点获取函数
std::vector<RoutePoint> GetRoutePointsOfHovercraft_Navigate(double startx, double endx, double const_y, double speed);
//气垫船牵引进/出舱仿真路线点获取函数
TowedShip Towing(int L, int R, double x);
