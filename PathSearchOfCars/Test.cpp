#include "pch.h"
#include "Test.h"
#include "fstream"
#include <iomanip>
using namespace std;
//从文件读入到string里
string readFileIntoString(const char * filename)
{
	ifstream ifile(filename);
	//将文件读入到ostringstream对象buf中
	ostringstream buf;
	char ch;
	while (buf&&ifile.get(ch))
		buf.put(ch);
	//返回与流对象buf关联的字符串
	return buf.str();
}
//************************************
// Method:    AllTest
// FullName:  AllTest
// Access:    public 
// Returns:   void
// Qualifier:测试所有生成器的功能
//************************************
void AllTest()
{
	double duration;
	clock_t start_t, finish;

	//障碍物载具信息序列
	vector<Car_Info> obstacle_info;
	obstacle_info.reserve(10);
	//添加障碍物载具
	AddCarObastacle(obstacle_info, 3, 3, 0, 1);
	AddCarObastacle(obstacle_info, 2, 3, 0, 1);
	//AddCarObastacle(obstacle_info, 1, 4, 0, 1);
	//AddCarObastacle(obstacle_info, 2, 4, 0, 1);
	AddCarObastacle(obstacle_info, 1, 3, 0, 1);
	AddCarObastacle(obstacle_info, 2, 5, 0, 1);
	AddCarObastacle(obstacle_info, 3, 5, 0, 1);
	AddCarObastacle(obstacle_info, 4, 5, 0, 1);
	AddCarObastacle(obstacle_info, 2, 7, 0, 1);
	AddCarObastacle(obstacle_info, 4, 6, 0, 1);
	//A星搜索
	Astar astar(91, 360, obstacle_info);
#ifdef  _DEBUG  
	astar.printGraphToCSV();
#endif

	//设置中心点栅格与边界栅格的距离
	astar.SetCarInfo(23, 24, 8, 8);
	//设置与障碍物的安全栅格距离
	astar.SetCollisionRadius(2);
	//设置起点与终点
	Point end = GetEndPointByRow_Col(2, 6, 1);
	Point start(45, 30);
	//A*算法找寻路径
	list<Point> path;
	start_t = clock();
	path = astar.GetPath(start, end, false);
	finish = clock();
#ifdef  _DEBUG  
	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
	cout << "搜寻路径时间花费：" << duration << endl;
	astar.printPathToGraphInCSV(path, start, end);
#endif


	start_t = clock();
	if (path.empty())
	{
		cout << "找不到路径，退出函数！" << endl;
		return ;
	}
	//导引点生成
	GuidePointGenerator rg(obstacle_info, 9.6, 3.4);
	rg.GenerateGuidePointsByPathSearch(start, end, path);
#ifdef  _DEBUG  
	rg.PrintRectangleOfObstaclesToGSV();
#endif
	
	//路线生成
	RoutePointGenerator cg(obstacle_info, 9.6, 3.4, 4.2, 1.5);
	cg.SetGuidePoints(rg.GetGuidePoints(), 0, 0);
	cg.GenerateRoutePoints();
	cg.GetBadGuidePoints();
#ifdef  _DEBUG  
	//输出数据
	cg.PrintRectangleOfObstaclesToGSV();
	cg.PrintRoutePointsToGSV();
	cg.PrintRectangleOfCollisionToGSV();
	finish = clock();

	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
	cout << "生成路线时间花费：" << duration << endl;
#endif
}


//************************************
// Method:    PaperPathPlanTest
// FullName:  PaperPathPlanTest
// Access:    public 
// Returns:   void
// Qualifier:论文路径规划仿真
//************************************
void PaperPathPlanTest() {
	double duration;
	clock_t start_t, finish;

	//障碍物载具信息序列
	vector<Car_Info> obstacle_info;
	obstacle_info.reserve(18);
	//首先添加支柱作为障碍物
	std::vector<PPoint> pillars;//支柱的中心点坐标，一共18个
	for (int i = 1; i < 11; ++i) {
		pillars.push_back(PPoint(18 * i + 6, 16));
	}
	for (int i = 1; i < 4; ++i) {
		pillars.push_back(PPoint(15 * i + 40, 24));
	}
	for (int i = 1; i < 4; ++i) {
		pillars.push_back(PPoint(20 * i + 80, 24));
	}
	pillars.push_back(PPoint(10, 8));
	pillars.push_back(PPoint(10, 20));

	for(const auto& pillar : pillars){
		Car_Info obstacle;
		obstacle.car_center = pillar;
		obstacle.angle = 0;
		obstacle.car_length = 1;
		obstacle.car_width = 1;
		obstacle_info.push_back(obstacle);
	}
	//读取文件，获得已经放置的车辆信息以及目标车辆的信息
	PPoint start_point,end_point;
	int car_type;
	double car_radius = 0,car_speed = 0;
	int head, heel, left, right;
	double car_length = 0, car_width = 0;
	GetObstacleCarInfoFromCSVFile(obstacle_info, start_point, end_point, car_type, car_speed);
	//根据车辆型号设置车辆数据
	switch (car_type)
	{
	case 1:
		car_length = 30 * 0.2;
		car_width = 15 * 0.2;
		head = 14;
		heel = 15;
		left = right = 7;
		car_radius = 8.7;
		break;
	case 2:
		car_length = 24 * 0.2;
		car_width = 14 * 0.2;
		head = 11;
		heel = 12;
		left = 6;
		right = 7;
		car_radius = 6.6;
		break;
	case 3:
		car_length = 35 * 0.2;
		car_width = 15 * 0.2;
		head = 17;
		heel = 17;
		left = right = 7;
		car_radius = 10.5;
		break;
	case 4:
		car_length = 44 * 0.2;
		car_width = 24 * 0.2;
		head = 21;
		heel = 22;
		left = 11;
		right = 12;
		car_radius = 13.6;
		break;
	}

	//初始化栅格地图
	std::vector<std::vector<int>> grid_map;//栅格地图
	std::vector<int> temp(LEN_COUNTS, 0);
	grid_map = std::vector<std::vector<int>>(WIDTH_COUNTS, temp);

	//初始化障碍物信息,障碍物信息确定的，直接在栅格地图中将对应的栅格置为1
	std::vector<Point> gride_pillars;	//支柱的栅格坐标

	for (const auto& center_point : pillars) {
		//取支柱中心栅格的栅格坐标
		int row = (32 - center_point.y) * 5;
		int col = center_point.x * 5 - 1;
		gride_pillars.push_back(Point(row, col));
	}
	for (const auto& g_center_point : gride_pillars) {
		//支柱为正方形，长宽均为5个栅格
		for (int i = g_center_point.x - 2; i <= g_center_point.x + 2; ++i) {
			for (int j = g_center_point.y - 2; j <= g_center_point.y + 2; ++j) {
				//将对应栅格置为1，表示已经覆盖了障碍物
				grid_map[i][j] = 1;
			}
		}
	}

	//初始化障碍物车辆信息
	//将对应的栅格置为1
	for (size_t i = 18; i < obstacle_info.size(); ++i) {
		auto car_info = obstacle_info[i];
		double x = car_info.car_center.x + car_info.car_length / 2;
		double y = car_info.car_center.y + car_info.car_width / 2;

		int col = x * 5 - 1;
		int row = (32 - y) * 5;

		int width = car_info.car_width / 0.2;
		int length = car_info.car_length / 0.2;
	
		for (int i = row; i <= row + width - 1; ++i) {
			for (int j = col - length + 1; j <= col; ++j) {
				grid_map[i][j] = 1;
			}
		}
	}
	


	//A星搜索
	Astar astar(grid_map);
	//Astar astar( LEN_COUNTS,WIDTH_COUNTS, obstacle_info);
	astar.printGraphToCSV();

	//设置车辆中心点栅格与边界栅格的距离
	astar.SetCarInfo(6, 6, 3, 3);
	/*PPointToPoint(,160)*/
	
	//设置与障碍物的安全栅格距离
	astar.SetCollisionRadius(2);
	//设置起点与终点
	Point end = PPointToPoint(end_point, 0.2, 160);
	Point start = PPointToPoint(start_point, 0.2, 160);
	//A*算法找寻路径
	list<Point> path;
	start_t = clock();
	path = astar.GetPath(start, end, false);
	finish = clock();
	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
	cout << "搜寻路径时间花费：" << duration << endl;
	astar.printPathToGraphInCSV(path, start, end);
	

//#ifdef  _DEBUG  
//	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
//	cout << "搜寻路径时间花费：" << duration << endl;
//	astar.printPathToGraphInCSV(path, start, end);
//#endif


	start_t = clock();
	if (path.empty())
	{
		cout << "找不到路径，退出函数！" << endl;
		return;
	}
	//导引点生成
	GuidePointGenerator rg(obstacle_info, car_length, car_width);
	rg.GenerateGuidePointsByPathSearch(start, end, path);
	//输出导引点
	/*rg.PrintRectangleOfObstaclesToGSV();
	rg.PrintGuidePointsToCSV();*/
//#ifdef  _DEBUG  
//	rg.PrintRectangleOfObstaclesToGSV();
//#endif

	//路线生成
	//car_radius = 5;
	car_speed = 1;
	obstacle_info.clear();
	//根据障碍物载具序列、载具长宽、载具速度、转弯半径构造
	RoutePointGenerator cg(obstacle_info, car_length, car_width, car_speed, car_radius);
	cg.SetGuidePoints(rg.GetGuidePoints(), 0, 0);
	cg.GenerateRoutePoints();
	auto counts = cg.GetCountsOfRoutePoints();
	
	cout << "路径点个数为:" <<counts<< endl;
	//cg.GetBadGuidePoints();

	//cg.PrintRectangleOfObstaclesToGSV();
	cg.PrintRoutePointsToGSV();

	PrintCarMovingInfoToCSVFile(cg.GetRoutePoints(), car_length, car_width);
//#ifdef  _DEBUG  
//	//输出数据
//	cg.PrintRectangleOfObstaclesToGSV();
//	cg.PrintRoutePointsToGSV();
//	cg.PrintRectangleOfCollisionToGSV();
//	finish = clock();
//
//	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
//	cout << "生成路线时间花费：" << duration << endl;
//#endif
}

void GetObstacleCarInfoFromCSVFile(vector<Car_Info>& obstacle_info, PPoint& start, PPoint& end, int& type, double& car_speed) {
	ifstream inFile("car_info.csv", ios::in);
	if (!inFile)
	{
		cout << "打开文件失败！" << endl;
		exit(1);
	}
	string line;
	string field;
	//获取目标车辆的信息 起点坐标、终点坐标、类型、速度
	getline(inFile, line);
	istringstream sin(line);
	getline(sin, field, ',');
	start.x = atof(field.c_str());
	getline(sin, field, ',');
	start.y = atof(field.c_str());
	getline(sin, field, ',');
	end.x = atof(field.c_str());
	getline(sin, field, ',');
	end.y = atof(field.c_str());

	getline(sin, field, ',');
	type = atoi(field.c_str());
	getline(sin, field, ',');
	car_speed = atof(field.c_str());


	//读取已经放置车辆的信息
	while (getline(inFile, line))//getline(inFile, line)表示按行读取CSV文件中的数据
	{
		string field;
		istringstream sin(line); 

		Car_Info obstacle;
		obstacle.angle = 0;
		getline(sin, field, ','); 
		obstacle.car_center.x = atof(field.c_str());

		getline(sin, field, ','); 
		obstacle.car_center.y = atof(field.c_str());

		getline(sin, field, ','); 
		obstacle.car_length = atof(field.c_str());

		getline(sin, field, ',');
		obstacle.car_width = atof(field.c_str());
		
		obstacle_info.push_back(obstacle);
	}
	inFile.close();
}

void PrintCarMovingInfoToCSVFile(std::vector<RoutePoint> route_points,double car_length, double car_width) {
	//将转折点转换，并输出到文件中
	FILE* fd = NULL;
	if ((fd = fopen("car_moving_info.csv", "wt+")) == NULL) {
		return;
	}
	for (size_t i = 0; i < route_points.size(); ++i) {
		RoutePoint route_point = route_points.at(i);
		if (i == 0 || i == route_points.size() - 1 || i % 100 == 0) {
			vector<PPoint> cur_car_points(4);
			CalculateCarBoundaryPoint(car_length, car_width, route_point.coordinate, route_point.angle_z, cur_car_points[0], cur_car_points[1], cur_car_points[2], cur_car_points[3]);
			fprintf(fd, "%f,%f,%f,%f,%f\n", cur_car_points[0].x, cur_car_points[1].x, cur_car_points[2].x, cur_car_points[3].x, cur_car_points[0].x);
			fprintf(fd, "%f,%f,%f,%f,%f\n", cur_car_points[0].y, cur_car_points[1].y, cur_car_points[2].y, cur_car_points[3].y, cur_car_points[0].y);
			
		}
	
		
	}
	fclose(fd);
	fd = NULL;
}

//************************************
// Method:    WebInterfacesTest
// FullName:  WebInterfacesTest
// Access:    public 
// Returns:   void
// Qualifier:测试所有web接口的功能
//************************************
void WebInterfacesTest()
{
	double duration;
	clock_t start_t, finish;

	//障碍物载具信息序列
	vector<Car_Info> obstacle_info;
	obstacle_info.reserve(10);
	//添加障碍物载具
	AddCarObastacle(obstacle_info, 3, 3, 0, 1);
	//AddCarObastacle(obstacle_info, 2, 3, 0, 1);
	AddCarObastacle(obstacle_info, 4, 3, 0, 1);
	AddCarObastacle(obstacle_info, 2, 4, 0, 1);
	AddCarObastacle(obstacle_info, 2, 5, 0, 1);
	AddCarObastacle(obstacle_info, 4, 5, 0, 1);
	AddCarObastacle(obstacle_info, 2, 7, 0, 1);
	AddCarObastacle(obstacle_info, 3, 7, 0, 1);
	//A星搜索
	Astar astar(91, 360, obstacle_info);
	astar.printGraphToCSV();
	//设置中心点栅格与边界栅格的距离
	astar.SetCarInfo(23, 24, 8, 8);
	//设置与障碍物的安全栅格距离
	astar.SetCollisionRadius(2);
	//设置起点与终点
	Point end = GetEndPointByRow_Col(4, 7, 1);
	Point start(45, 30);
	//A*算法找寻路径
	list<Point> path;
	start_t = clock();
	path = astar.GetPath(start, end, false);
	finish = clock();

#ifdef  _DEBUG  
	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
	cout << "搜寻路径时间花费：" << duration << endl;
	astar.printPathToGraphInCSV(path, start, end);
#endif

	start_t = clock();
	//导引点生成
	GuidePointGenerator rg(obstacle_info, 9.6, 3.4);
	rg.GenerateGuidePointsByPathSearch(start, end, path);
	auto json = createJsonfuncForTest1(obstacle_info);
	
	//auto json = createJsonfuncForTest2(obstacle_info, rg.GetGuidePoints());
	//auto json = createJsonfuncForTest4(obstacle_info, rg.GetGuidePoints(), 1);
	//auto json = createJsonfuncForTest3(obstacle_info, rg.GetGuidePoints());
#ifdef  _DEBUG  
	fstream json_data("json.txt");
	if (json_data.is_open())
	{
		json_data << json;
	}
	json_data.close();
	//cout << "get-json" << json << endl;
#endif
	GenerateIntelligentLayoutPlan(const_cast<char *>(json.c_str()),nullptr);
	//Vehicle_Simulation_Demonstration(const_cast<char *>(json.c_str()));

	//auto j= GenerateIntelligentPathPlan(const_cast<char *>(json.c_str()));
	/*char input[] = "{}";
	auto j = GenerateIntelligentPathPlan(input);*/
	//ArtificialPathPlanCheck(const_cast<char *>(json.c_str()));
	//Vehicle_Simulation_Demonstration(const_cast<char *>(json.c_str()));
	/*obstacle_info.clear();
	rg.GetGuidePoints().clear();
	for (int i = 2; i < 100; ++i)
	{
		auto json = createJsonfuncForTest4(obstacle_info, rg.GetGuidePoints(), i);

		Vehicle_Simulation_Demonstration(const_cast<char *>(json.c_str()));
	}*/
}
void Test_GenerateIntelligentLayoutPlan()
{
	double duration;
	clock_t start_t, finish;

	//障碍物载具信息序列
	vector<Car_Info> obstacle_info;
	obstacle_info.reserve(10);
	//添加障碍物载具
	AddCarObastacle(obstacle_info, 3, 3, 0, 1);
	//AddCarObastacle(obstacle_info, 2, 3, 0, 1);
	AddCarObastacle(obstacle_info, 4, 3, 0, 1);
	AddCarObastacle(obstacle_info, 2, 4, 0, 1);
	AddCarObastacle(obstacle_info, 2, 5, 0, 1);
	AddCarObastacle(obstacle_info, 4, 5, 0, 1);
	AddCarObastacle(obstacle_info, 2, 7, 0, 1);
	AddCarObastacle(obstacle_info, 3, 7, 0, 1);
	//A星搜索
	Astar astar(91, 360, obstacle_info);
	astar.printGraphToCSV();
	//设置中心点栅格与边界栅格的距离
	astar.SetCarInfo(23, 24, 8, 8);
	//设置与障碍物的安全栅格距离
	astar.SetCollisionRadius(2);
	//设置起点与终点
	Point end = GetEndPointByRow_Col(4, 7, 1);
	Point start(45, 30);
	//A*算法找寻路径
	list<Point> path;
	start_t = clock();
	path = astar.GetPath(start, end, false);
	finish = clock();

#ifdef  _DEBUG  
	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
	cout << "搜寻路径时间花费：" << duration << endl;
	astar.printPathToGraphInCSV(path, start, end);
#endif

	start_t = clock();
	//导引点生成
	GuidePointGenerator rg(obstacle_info, 9.6, 3.4);
	rg.GenerateGuidePointsByPathSearch(start, end, path);
	auto json = createJsonfuncForTest1(obstacle_info);
#ifdef  _DEBUG  
	rg.PrintRectangleOfObstaclesToGSV();
	fstream json_data("json.txt");
	if (json_data.is_open())
	{
		json_data << json;
	}
	json_data.close();
	cout << "get-json" << json << endl;
#endif
	//读文件获取json数据
	auto errorJson = readFileIntoString("E:\ConfigurationJson.json");
	GenerateIntelligentLayoutPlan(const_cast<char *>(errorJson.c_str()), nullptr);
	
}
void Test_GenerateIntelligentPathPlan()
{
//	double duration;
//	clock_t start_t, finish;
//
//	//障碍物载具信息序列
//	vector<Car_Info> obstacle_info;
//	obstacle_info.reserve(10);
//	//添加障碍物载具
//	AddCarObastacle(obstacle_info, 3, 3, 0, 1);
//	AddCarObastacle(obstacle_info, 4, 3, 0, 1);
//	//AddCarObastacle(obstacle_info, 1, 4, 0, 1);
//	AddCarObastacle(obstacle_info, 2, 4, 0, 1);
//	//AddCarObastacle(obstacle_info, 1, 3, 0, 1);
//	AddCarObastacle(obstacle_info, 2, 5, 0, 1);
//	//AddCarObastacle(obstacle_info, 1, 5, 0, 1);
//	AddCarObastacle(obstacle_info, 4, 5, 0, 1);
//	AddCarObastacle(obstacle_info, 2, 7, 0, 1);
//	AddCarObastacle(obstacle_info, 3, 7, 0, 1);
//	//A星搜索
//	Astar astar_test(91, 360, obstacle_info);
//	astar_test.printGraphToCSV();
//	//设置中心点栅格与边界栅格的距离
//	astar_test.SetCarInfo(23, 24, 8, 8);
//	//设置与障碍物的安全栅格距离
//	astar_test.SetCollisionRadius(2);
//	//设置起点与终点
//	Point end = GetEndPointByRow_Col(4, 7, 1);
//	Point start(45, 0);
//	//A*算法找寻路径
//	list<Point> path;
//	start_t = clock();
//	path = astar_test.GetPath(start, end, false);
//	finish = clock();
//
//#ifdef  _DEBUG  
//	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
//	cout << "搜寻路径时间花费：" << duration << endl;
//	astar_test.printPathToGraphInCSV(path, start, end);
//#endif
//
//	start_t = clock();
//	//导引点生成
//	GuidePointGenerator rg(obstacle_info, 9.6, 3.4);
//	rg.GenerateGuidePointsByPathSearch(start, end, path);
//	auto json = createJsonfuncForTest2(obstacle_info, rg.GetGuidePoints());
//#ifdef  _DEBUG  
//	rg.PrintRectangleOfObstaclesToGSV();
//	fstream json_data("json.txt");
//	if (json_data.is_open())
//	{
//		json_data << json;
//	}
//	json_data.close();
//	cout << "get-json" << json << endl;
//#endif
	//读文件获取json数据
	auto errorJson = readFileIntoString("E:\errorJson.json");
	GenerateIntelligentPathPlan(const_cast<char *>(errorJson.c_str()) ,nullptr);

}
void Test_ArtificialPathPlanCheck()
{
	double duration;
	clock_t start_t, finish;

	//障碍物载具信息序列
	vector<Car_Info> obstacle_info;
	obstacle_info.reserve(10);
	//添加障碍物载具
	AddCarObastacle(obstacle_info, 3, 3, 0, 1);
	//AddCarObastacle(obstacle_info, 2, 3, 0, 1);
	AddCarObastacle(obstacle_info, 4, 3, 0, 1);
	AddCarObastacle(obstacle_info, 2, 4, 0, 1);
	AddCarObastacle(obstacle_info, 2, 5, 0, 1);
	AddCarObastacle(obstacle_info, 4, 5, 0, 1);
	AddCarObastacle(obstacle_info, 2, 7, 0, 1);
	AddCarObastacle(obstacle_info, 3, 7, 0, 1);
	//A星搜索
	Astar astar_test(91, 360, obstacle_info);
	astar_test.printGraphToCSV();
	//设置中心点栅格与边界栅格的距离
	astar_test.SetCarInfo(23, 24, 8, 8);
	//设置与障碍物的安全栅格距离
	astar_test.SetCollisionRadius(2);
	//设置起点与终点
	Point end = GetEndPointByRow_Col(4, 7, 1);
	Point start(45, 30);
	//A*算法找寻路径
	list<Point> path;
	start_t = clock();
	path = astar_test.GetPath(start, end, false);
	finish = clock();

#ifdef  _DEBUG  
	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
	cout << "搜寻路径时间花费：" << duration << endl;
	astar_test.printPathToGraphInCSV(path, start, end);
#endif

	start_t = clock();
	//导引点生成
	GuidePointGenerator rg(obstacle_info, 9.6, 3.4);
	rg.GenerateGuidePointsByPathSearch(start, end, path);
	auto json = createJsonfuncForTest3(obstacle_info, rg.GetGuidePoints());
#ifdef  _DEBUG  
	fstream json_data("json.txt");
	if (json_data.is_open())
	{
		json_data << json;
	}
	json_data.close();
	cout << "get-json" << json << endl;
#endif
	ArtificialPathPlanCheck(const_cast<char *>(json.c_str()), nullptr);
}
void Test_Vehicle_Simulation_Demonstration()
{
	double duration;
	clock_t start_t, finish;

	//障碍物载具信息序列
	vector<Car_Info> obstacle_info;
	obstacle_info.reserve(10);
	//添加障碍物载具
	AddCarObastacle(obstacle_info, 3, 3, 0, 1);
	//AddCarObastacle(obstacle_info, 2, 3, 0, 1);
	AddCarObastacle(obstacle_info, 4, 3, 0, 1);
	AddCarObastacle(obstacle_info, 2, 4, 0, 1);
	AddCarObastacle(obstacle_info, 2, 5, 0, 1);
	AddCarObastacle(obstacle_info, 4, 5, 0, 1);
	AddCarObastacle(obstacle_info, 2, 7, 0, 1);
	AddCarObastacle(obstacle_info, 3, 7, 0, 1);
	//A星搜索
	Astar astar_test(91, 360, obstacle_info);
	astar_test.printGraphToCSV();
	//设置中心点栅格与边界栅格的距离
	astar_test.SetCarInfo(23, 24, 8, 8);
	//设置与障碍物的安全栅格距离
	astar_test.SetCollisionRadius(2);
	//设置起点与终点
	Point end = GetEndPointByRow_Col(4, 7, 1);
	Point start(45, 30);
	//A*算法找寻路径
	list<Point> path;
	start_t = clock();
	path = astar_test.GetPath(start, end, false);
	finish = clock();

#ifdef  _DEBUG  
	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
	cout << "搜寻路径时间花费：" << duration << endl;
	astar_test.printPathToGraphInCSV(path, start, end);
#endif

	start_t = clock();
	//导引点生成
	GuidePointGenerator rg(obstacle_info, 9.6, 3.4);
	rg.GenerateGuidePointsByPathSearch(start, end, path);
	auto json = createJsonfuncForTest4(obstacle_info, rg.GetGuidePoints());
#ifdef  _DEBUG  
	fstream json_data("json.txt");
	if (json_data.is_open())
	{
		json_data << json;
	}
	json_data.close();
	cout << "get-json" << json << endl;
#endif
	//auto null_json = createNullJson();
	//Vehicle_Simulation_Demonstration(const_cast<char *>(null_json.c_str()), nullptr);
	//Vehicle_Simulation_Demonstration(const_cast<char *>(json.c_str()),nullptr);
	//
	//obstacle_info.clear();
	//rg.GetGuidePoints().clear();
	//for (int i = 2; i < 200; ++i)
	//{
	//	auto null_json = createNullJson();
	//	//cout << null_json << endl;
	//	Vehicle_Simulation_Demonstration(const_cast<char *>(null_json.c_str()), nullptr);
	//}
	//Vehicle_Simulation_Demonstration(const_cast<char *>(json.c_str()), nullptr);
	//for (int i = 2; i < 10; ++i)
	//{
	//	auto null_json = createNullJson();
	//	//cout << null_json << endl;
	//	Vehicle_Simulation_Demonstration(const_cast<char *>(null_json.c_str()), nullptr);
	//}


	//读文件获取json数据
	auto errorJson=readFileIntoString("E:\errorJson.json");
	Vehicle_Simulation_Demonstration(const_cast<char *>(errorJson.c_str()), nullptr);
	//for (int i = 2; i < 100; ++i)
	//{
	//	auto null_json = createNullJson();
	//	//cout << null_json << endl;
	//	Vehicle_Simulation_Demonstration(const_cast<char *>(null_json.c_str()), nullptr);
	//}
}
void Test_Vehicle_Scheduling_Simulation()
{
	//读文件获取json数据
	auto errorJson = readFileIntoString("E:\Simu.json");
	Vehicle_Scheduling_Simulation(const_cast<char *>(errorJson.c_str()), nullptr);
	Vehicle_Scheduling_Simulation(const_cast<char *>(errorJson.c_str()), nullptr);
	for (int i = 2; i < 100; ++i)
	{
		auto null_json = createNullJson();
		//cout << null_json << endl;
		Vehicle_Scheduling_Simulation(const_cast<char *>(null_json.c_str()), nullptr);
	}
}
void Test_GetRoutePointsOfHovercraft()
{
	auto result= GetRoutePointsOfHovercraft_Navigate(0, 100, 10, 2);
	for (auto& p : result)
	{
		cout << p.coordinate.x << setw(4) << p.coordinate.y << setw(4) << p.angle_z << endl;
	}
}

void Test_VehicleLocationRequest() {
//	double duration;
//	clock_t start_t, finish;
//
//	//障碍物载具信息序列
//	vector<Car_Info> obstacle_info;
//	obstacle_info.reserve(10);
//	//添加障碍物载具
//	AddCarObastacle(obstacle_info, 3, 3, 0, 1);
//	//AddCarObastacle(obstacle_info, 2, 3, 0, 1);
//	AddCarObastacle(obstacle_info, 4, 3, 0, 1);
//	AddCarObastacle(obstacle_info, 2, 4, 0, 1);
//	AddCarObastacle(obstacle_info, 2, 5, 0, 1);
//	AddCarObastacle(obstacle_info, 4, 5, 0, 1);
//	AddCarObastacle(obstacle_info, 2, 7, 0, 1);
//	AddCarObastacle(obstacle_info, 3, 7, 0, 1);
//	//A星搜索
//	Astar astar_test(91, 360, obstacle_info);
//	astar_test.printGraphToCSV();
//	//设置中心点栅格与边界栅格的距离
//	astar_test.SetCarInfo(23, 24, 8, 8);
//	//设置与障碍物的安全栅格距离
//	astar_test.SetCollisionRadius(2);
//	//设置起点与终点
//	Point end = GetEndPointByRow_Col(4, 7, 1);
//	Point start(45, 30);
//	//A*算法找寻路径
//	list<Point> path;
//	start_t = clock();
//	path = astar_test.GetPath(start, end, false);
//	finish = clock();
//
//#ifdef  _DEBUG  
//	duration = (double)(finish - start_t) / CLOCKS_PER_SEC;
//	cout << "搜寻路径时间花费：" << duration << endl;
//	astar_test.printPathToGraphInCSV(path, start, end);
//#endif
//
//	start_t = clock();
//	//读文件获取json数据
////	auto errorJson = readFileIntoString("E:\Simu.json");
////#ifdef  _DEBUG  
////	cout << "get-json" << errorJson << endl;
////#endif
////	VehicleLocationRequest(const_cast<char *>(errorJson.c_str()), nullptr);
//
//	//导引点生成
//	GuidePointGenerator rg(obstacle_info, 9.6, 3.4);
//	rg.GenerateGuidePointsByPathSearch(start, end, path);
//
//	int vehicle_id = 1, route_point_id = 1;
//
//	auto json = createJsonfuncForTestVehicleLocationRequest(obstacle_info, rg.GetGuidePoints(), vehicle_id, route_point_id);
//#ifdef  _DEBUG  
//	cout << "get-json" << json << endl;
//#endif
//	VehicleLocationRequest(const_cast<char *>(json.c_str()), nullptr);
//	return;
//	vehicle_id = 2;
//	json = createJsonfuncForTestVehicleLocationRequest(obstacle_info, rg.GetGuidePoints(), vehicle_id,route_point_id);
//#ifdef  _DEBUG  
//	cout << "get-json" << json << endl;
//#endif
	auto errorJson = readFileIntoString("E:\Simu.json");
	VehicleLocationRequest(const_cast<char *>(errorJson.c_str()), nullptr);
	/*for (int i = 2; i < 163; ++i)
	{
		obstacle_info.clear();
		vector<PPoint> tmp;
		auto json = createJsonfuncForTestVehicleLocationRequest(obstacle_info, tmp, vehicle_id, i);
#ifdef  _DEBUG  
		cout << "get-json" << json << endl;
#endif
		VehicleLocationRequest(const_cast<char *>(json.c_str()), nullptr);
	}*/
}
void Test_HovercraftInitiativeRequest() {
	
	int vehicle_id = 1, route_point_id = 1;
	double start_x = 0.0, end_x = 100.0;
	auto json = createJsonfuncForTestHovercraftInitiativeRequest(start_x, end_x, vehicle_id, route_point_id);
#ifdef  _DEBUG  
	cout << "get-json" << json << endl;
#endif
	HovercraftInitiativeRequest(const_cast<char *>(json.c_str()), nullptr);

	vehicle_id = 2;
	for (int i = 987; i < 995; ++i)
	{
		auto json = createJsonfuncForTestHovercraftInitiativeRequest(start_x, end_x, vehicle_id, i);
		HovercraftInitiativeRequest(const_cast<char *>(json.c_str()), nullptr);
	}

}
void Test_HovercraftPassivelyRequest() {
	double cur_x = 1.2;
	int left_gear = 1, right_gear = 2;
	auto json = createJsonfuncForTestHovercraftPassivelyRequest(cur_x, left_gear, right_gear);
#ifdef  _DEBUG  
	cout << "get-json" << json << endl;
#endif
	HovercraftPassivelyRequest(const_cast<char *>(json.c_str()), nullptr);
}


void Test_Log()
{
	CLogFile logfile(1);

	// 打开日志文件，如果"/tmp/log"不存在，就创建它，但是要确保当前用户具备创建目录的权限。
	if (logfile.Open("C:\\log.txt") == false)
	{
		printf("logfile.Open(/tmp/log/demo42.log) failed.\n"); return ;
	}

	logfile.Write("demo42程序开始运行。\n");

	logfile.WriteEx("ok\n");
	

	logfile.Write("dfafafa行结束。\n");
}

//************************************
// Method:    GetEndIdByRow_Col
// FullName:  GetEndIdByRow_Col
// Access:    public 
// Returns:   int
// Qualifier:辅助函数：根据行列号来生成终点的栅格编号，地图被划分为4行7列，每行大小为17，间隔为4个栅格，每列大小为48，间隔为3个栅格
// Parameter: int row_id
// Parameter: int col_id
// Parameter: int car_type
//************************************
Point GetEndPointByRow_Col(int row_id, int col_id, int car_type)
{
	int i, j;
	int x = (row_id - 1) * 17 + 4 * row_id;
	int y = (col_id - 1) * 48 + 3 * col_id;
	if (car_type == 1)
	{
		i = x + 8;
		j = y + 23;
	}
	else
	{
		i = x + 8;
		j = y + 20;
	}
	return Point(i, j);
}

//************************************
// Method:    GenerateCarCenterPoint
// FullName:  GenerateCarCenterPoint
// Access:    public 
// Returns:   PPoint
// Qualifier:辅助函数：产生载具中心点坐标
// Parameter: int row_id	行号
// Parameter: int col_id	列号
// Parameter: int car_type	载具类型 1 2
//************************************
PPoint GenerateCarCenterPoint(int row_id, int col_id, int car_type)
{
	if (row_id > 4 || col_id > 7 || row_id < 1 || col_id < 1)
		return PPoint(0, 0);
	//默认方向为0
	//地图被划分为4行7列，每行大小为17，间隔为4个栅格，每列大小为48，间隔为3个栅格
	//确定中心点栅格坐标，方向为横向
	int x, y;
	if (car_type == 1)
	{
		x = (row_id - 1) * 17 + 4 * row_id + 8;
		y = (col_id - 1) * 48 + 3 * col_id + 24;
	}
	else
	{
		x = (row_id - 1) * 17 + 4 * row_id + 8;
		y = (col_id - 1) * 48 + 3 * col_id + 20;
	}

	//根据栅格坐标获得直角坐标
	double px = y * 0.2 + 0.1;
	double py = 91 * 0.2 - x * 0.2 - 0.1;
	return  PPoint(px, py);
}
//************************************
// Method:    AddCarObastacle
// FullName:  AddCarObastacle
// Access:    public 
// Returns:   void
// Qualifier:辅助函数：在对应的行和列上添加载具障碍物
// Parameter: vector<car_info> & obstacle
// Parameter: int row_id 行号
// Parameter: int col_id 列号
// Parameter: double angle 倾斜角度，非弧度，以x轴正向为0度，逆时针旋转
// Parameter: int car_type 载具类型
//************************************
void AddCarObastacle(vector<Car_Info>& obstacle, int row_id, int col_id, double angle, int car_type)
{
	Car_Info cur_info;

	PPoint car_center;
	car_center = GenerateCarCenterPoint(row_id, col_id, car_type);
	cur_info.car_center = car_center;
	cur_info.angle = angle;
	if (car_type == 1)
	{
		cur_info.car_length = 9.6;
		cur_info.car_width = 3.4;
	}
	else
	{
		cur_info.car_length = 8.2;
		cur_info.car_width = 3.4;
	}
	obstacle.push_back(cur_info);
}

