/*!
 * \file Test.h
 * \date 2021/01/9 10:20
 *
 * \author lzk
 * Contact: 1274962676@qq.com
 *
 * \brief 单元测试模块
 *
 * TODO: long description
 *		（1）实现有各个接口的测试函数；
 * \note
*/
#pragma once
#include <iostream>
#include <vector>
#include "ConfigurationPlan.h"
#include "AStar.h"
#include <math.h>
#include <algorithm>    // std::max
#include "GuidePointGenerator.h"
#include "RoutePointGenerator.h"
#include "Calculate.h"
#include <time.h>
#include "Classes.h"
#include "JsonFuctions.h"
#include "WebInterface.h"
#include "MotionSimulation.h"
#include "CLogFile.h"
const int LEN_COUNTS = 1030;
const int WIDTH_COUNTS = 160;
//接口测试函数
Point GetEndPointByRow_Col(int row_id, int col_id, int car_type);
PPoint GenerateCarCenterPoint(int row_id, int col_id, int car_type);
void AllTest();

void PaperPathPlanTest();
void GetObstacleCarInfoFromCSVFile(std::vector<Car_Info>& obstacle_info, PPoint& start, PPoint& end,int& type,double& car_speed);
void PrintCarMovingInfoToCSVFile(std::vector<RoutePoint> route_points, double car_length, double car_width);

void AddCarObastacle(std::vector<Car_Info>& obstacle, int row_id, int col_id, double angle, int car_type);
//测试所有web接口的功能
void WebInterfacesTest();
void Test_GenerateIntelligentLayoutPlan();
void Test_GenerateIntelligentPathPlan();
void Test_ArtificialPathPlanCheck();
void Test_Vehicle_Simulation_Demonstration();
void Test_Vehicle_Scheduling_Simulation();
void Test_GetRoutePointsOfHovercraft();

void Test_VehicleLocationRequest();
void Test_HovercraftInitiativeRequest();
void Test_HovercraftPassivelyRequest();
void Test_Log();
	
