/*!
 * \file RoutePointGenerator.h
 * \date 2020/12/07 22:03
 *
 * \author lzk
 * Contact: 1274962676@qq.com
 *
 * \brief 路线点生成器类
 *
 * TODO: long description
 *		（1）RoutePointGenerator类集成了路线点（载具某一时刻下的 坐标和方向）生成的各个函数，用于以导引点（转弯点）序列求载具实际行驶的各个路线点；
*		（2）项目中RoutePointGenerator类的输入由GuidePointGenerator类生成；
 *		（3）项目中GuidePointGenerator类的输出由WebInterface的接口接收；
 * \note
*/
#pragma once
#include "GuidePointGenerator.h"
#include "Classes.h"

//路线点生成器
class RoutePointGenerator final
{
public:
	//删除默认构造函数和复制构造函数
	RoutePointGenerator()=delete;
	RoutePointGenerator(const RoutePointGenerator &) = delete;
	RoutePointGenerator& operator=(const RoutePointGenerator&) = delete;
	//根据障碍物载具序列、载具长宽、载具速度、转弯半径构造
	RoutePointGenerator(std::vector<Car_Info>& obstacle_info, const double length, const double width, double speed,double radius);
	//根据全部障碍物载具四个边界点的信息、载具长宽构造、转弯半径、
	RoutePointGenerator(std::vector<std::vector<PPoint>>& cars_boundary_points, const double length, const double width, double radius);
	//设置导引点，起点和终点方向
	void SetGuidePoints(const std::vector<PPoint>& _guide_points,double d_begin, double d_end);
	//生成路径点序列
	void GenerateRoutePoints();

	//获取路径点序列
	std::vector<RoutePoint> GetRoutePoints() const;
	//获取路径点个数
	int GetCountsOfRoutePoints() const;
	//获取碰撞导引点
	std::vector<PPoint> GetBadGuidePoints();
	//获取转弯碰撞导引点
	std::vector<PPoint> GetBadTurningGuidePoints() const;
	//获得当前路线的状态
	bool GetPathStatus();
	//第一次发生碰撞时的路径点序号，值从1开始
	int GetFirstCollisonRoutePointId();


	//测试使用
	//将离散的路径点序列输出到文件中
	void PrintRoutePointsToGSV();
	//将第一次发生碰撞时的矩形输出到文件中
	void PrintRectangleOfCollisionToGSV();
	//将障碍物矩形输出到文件中
	void PrintRectangleOfObstaclesToGSV();
	//默认构造函数
	~RoutePointGenerator() = default;
private:
	//判断三个导引点能否进行平滑
	bool Judge3pointsmooth(PPoint A, PPoint B, PPoint C);
	//判断四个导引点能否进行连续平滑
	bool Judge4pointsmooth(PPoint A, PPoint B, PPoint C, PPoint D);
	//辅助函数
	double atan3(double a, double b);
	//通过导引点获得路线各个直线/曲线方程
	void GenerateRouteCurveEquationByGuidePoints();
	//根据路线各个直线/曲线方程、时间间隔获得离散的路径点序列
	void GenerateRoutePointsByRouteCurveEquation();
	//转向处圆弧信息生成
	void CurveGeneration(PPoint front, PPoint middle, PPoint back);
	//生成直线方程并存储
	void StraightLineGeneration_Storage(PPoint front, PPoint back);
	//检测所有路径点是否会触碰障碍物，并存储相关信息
	bool CollisionCheckForRoutePoints(RoutePoint route_point);


	//全部障碍物载具四个边界点的信息
	std::vector<std::vector<PPoint>> cars_boundary_points;
	//导引点序列
	std::vector<PPoint> guide_points;
	//路径点序列
	std::vector<RoutePoint> route_points;
	//路径各个曲线的方程 1为圆 2 为直线斜率方程 3 为水平线
	std::vector<std::vector<double>> route_curve_equation;
	
	//第一次发生碰撞时的路径点序号，从1开始
	size_t first_collison_id=0;
	//发生碰撞的路径点对应的导引点序列,如果直线段中路线点发生碰撞，则直线段的两个导引点都要添加
	std::vector<PPoint> bad_guide_points;
	//路径点碰撞发生在转弯时，其对应的导引点
	std::vector<PPoint> bad_turning_guide_points;


	//起点终点方向
	double end_direction=0;
	double begin_direction=0;
	//路径是否可用 true-可用 false—发生碰撞，不可用
	bool is_available = true;
	//载具长宽
	double car_length=4, car_width=4;
	//载具速度
	double car_speed=4.2;
	//载具转弯半径
	double R = 1.5;
	//Pi值
	const double pi = 3.1415926;
	//圆心坐标(X,Y),切点坐标M1(Mx1,My1),M2(Mx2,My2)
	double X=0, Y=0, Mx1 = 0, My1 = 0, Mx2 = 0, My2 = 0;
	//车辆转过角度Θ
	double Theta=0;
};

