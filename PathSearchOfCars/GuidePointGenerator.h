/*!
 * \file GuidePointGenerator.h
 * \date 2020/12/07 22:03
 *
 * \author lzk
 * Contact: 1274962676@qq.com
 *
 * \brief 导引点生成器类
 *
 * TODO: long description
 *		（1）GuidePointGenerator类集成了路线导引点（转弯点）生成的各个函数，用于将栅格路径转换为导引点（转弯点）序列；
 *		（2）项目中GuidePointGenerator类的输入由Astart类生成；
 *		（3）项目中GuidePointGenerator类的输出由RoutePointGenerator类接收；
 * \note
*/
#pragma once
#include <iostream>
#include <cmath>
#include <math.h>
#include <vector>
#include <list>
#include "AStar.h"
#include "Classes.h"


//导引点生成器
class GuidePointGenerator final
{
public:
	//删除默认构造函数和复制构造函数
	GuidePointGenerator() = delete;
	GuidePointGenerator(const GuidePointGenerator&) = delete;
	GuidePointGenerator& operator=(const GuidePointGenerator&) = delete;
	//根据障碍物载具序列、载具长宽构造
	explicit GuidePointGenerator(std::vector<Car_Info>& obstacle_info, const double length, const double width);

	//根据栅格路径获得导引点序列
	void GenerateGuidePointsByPathSearch(Point start, Point end, std::list<Point> &path);
	//根据外部导引点序列初始化导引点序列
	void GenerateGuidePointsByGuidePoints(std::vector<PPoint> outside_guide_points);
	//将障碍物矩形输出到文件中
	void PrintRectangleOfObstaclesToGSV();
	//将离散的导引点序列输出到文件中
	void PrintGuidePointsToCSV(const std::string filename = "GuidePoints.csv") const;
	
	//输出生成的导引点序列
	std::vector<PPoint>& GetGuidePoints();
	//获取导引点路径状态
	bool GetPathStatus();
	//使用默认析构函数
	~GuidePointGenerator() = default;
private:
	/*导引点生成函数*/
	void GetTurningPointsByPath(Point start, Point end, std::list<Point> &path, std::vector<Point> &nodesOfDirectionChange);
	
	
	/*优化导引序列的函数*/
	//优化导引点序列，删除多余导引点
	void GuidePointsSieveOptimization();

	bool GuidePointsSieve();
	//优化导引点序列,调整所有碰撞导引点位置，调整每一个导引点坐标，使其尽可能远离障碍物
	void BadGuidePointsAdjustmentOptimization();
	//优化导引点序列,处理所有碰撞导引点，调整每一个生成路线点时会发生碰撞的导引点的坐标，使得最终路线不碰撞
	void BadGuidePointsDisposeOptimization();
		//辅助函数
	//检测载具在两个导引点之间运动是否会触碰障碍物
	bool CollisionCheckForGuidePointsOptimization(PPoint guide_point1, PPoint guide_point2) const;
	//两个用于调整导引点的函数
	PPoint FindNewPointToReplaceCurPoint(PPoint pre, PPoint cur, PPoint next, std::vector<std::vector<PPoint>>& cars_boundary_points, double car_length, double car_width, std::vector<PPoint>& shrink_pre_adjust_points, std::vector<PPoint>& extend_next_reverse_adjust_points);
	PPoint AddNewPointToTransition(PPoint pre, PPoint cur, PPoint next, std::vector<std::vector<PPoint>>& cars_boundary_points, double car_length, double car_width, std::vector<PPoint>& shrink_pre_adjust_points, std::vector<PPoint>& extend_next_reverse_adjust_points);


	//全部障碍物载具四个边界点的信息
	std::vector<std::vector<PPoint>> cars_boundary_points;
	//导引点序列
	std::vector<PPoint> guide_points;

	//导引点是否可用 true-可用 false—发生碰撞，不可用
	bool is_available = true;

	//载具长宽
	double car_length, car_width;                   

};


