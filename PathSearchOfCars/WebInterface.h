/*!
 * \file WebInterface.h
 * \date 2021/01/9 10:20
 *
 * \author lzk
 * Contact: 1274962676@qq.com
 *
 * \brief 前端页面的接口模块
 *
 * TODO: long description
 *		（1）实现有与前端交互的接口函数；
 *		（2）接口函数的输入输出格式都为json；
 *		（2）项目中WebInterface接口函数的输入输出的json都由JsonFuctions处理；
 * \note
*/
#pragma once

//与前端页面的接口
extern"C" _declspec(dllexport) void GenerateIntelligentLayoutPlan(char* dest, char** outdata);
extern"C" _declspec(dllexport) void GenerateIntelligentPathPlan(char* dest, char** outdata);
extern"C" _declspec(dllexport) void ArtificialPathPlanCheck(char* dest, char** outdata);
extern"C" _declspec(dllexport) void Vehicle_Simulation_Demonstration(char* dest, char** outdata);
extern"C" _declspec(dllexport) void Vehicle_Scheduling_Simulation(char* dest, char** outdata);
extern"C" _declspec(dllexport) void VehicleLocationRequest(char* dest, char** outdata);
extern"C" _declspec(dllexport) void HovercraftInitiativeRequest(char* dest, char** outdata);
extern"C" _declspec(dllexport) void HovercraftPassivelyRequest(char* dest, char** outdata);

