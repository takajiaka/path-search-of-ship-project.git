#include "pch.h"
#include "MotionSimulation.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <time.h> 

using namespace std;
//创建气垫船类
class ACS
{
public:
	void set(double, double[]);//设置气垫船状态:{φ，ψ，ξ，η，u，v，p，r}
	void reset();//清空气垫船仿真数据，恢复默认设置
	void hit();  //碰撞
	double Getfactor(double);//获取使气垫船维持匀速状态的factor，即动力参数，形参（当前速度）
	void StraightRun(double, double, double);//气垫船直线行进，形参(x起点，x终点，匀速行驶速度)
	vector<double> GetCoordinateXOfResult();
private:
	//动态输出数据（气垫船运动数据记录）
	vector<double> RollAngle;   //横摇角
	vector<double> HeadingAngle;//艏向角度
	vector<double> Ox;          //大地坐标系x坐标
	vector<double> Oy;          //大地坐标系y坐标
	vector<double> Ov;          //大地坐标系总速度

	//气垫船操纵参数
	double y[8] = { 0,0,0,0,0,0,0,0 };//气垫船运动状态{φ，ψ，ξ，η，u，v，p，r}
	double factor = 3;   //动力参数
	double ar = 0;       //空气舵舵角（不大于PI/6,取值在0-0.5之间）
	double Vw = 0;       //环境绝对风速
	double Bw = 0;       //环境绝对风向角(弧度制,0-2pi)
	double hitv = 0;     //撞击速度
	double hitangle = 0; //撞击角度

	//气垫船仿真参数
	double t = 0;  //启动时间
	double h = 0.1;//计算步长
	double eps = 0.0000001; //精度
	double steps = 10000;   //默认最大仿真步数（仿真时间=steps*h）

	//气垫船四自由度模型参数
	double M = 150000;  //气垫船质量
	double g = 9.8;     //重力常数
	double La = 33;     //气垫船气垫长度
	double B = 16;      //气垫船气垫宽度
	double Sa = 312;    //气垫船水平面正投影面积
	double T = 10000;   //单个螺旋桨推力
	double Sd = 4;      //螺旋桨导管面积
	double SR = 3.1;    //空气舵面积
	double XR = -16, ZR = -2;      //两舵轴x，z位置坐标（运动坐标系）
	double Xf = 0, Yf = 0, Zf = 0; //垫升风扇位置坐标（运动坐标系）
	double Ix = 5672000;   //绕x轴转动惯量
	double Iz = 20570000;  //绕z轴转动惯量
	double Pa = 1.225;     //空气密度
	const double Q = 418;  //垫升风扇入口流量
	const double PI = atan(1.) * 4.;//圆周率

	//解算方程相关函数
	double Oxv(); //大地坐标系x轴速度
	double Oyv(); //大地坐标系y轴速度
	double Va();  //相对风速
	double Ba();  //相对风向角
	double sign(double var); //取符号函数
	double uaa();  //空气舵相关变量
	double Cxa();  //气动力系数
	double Cya();
	double Cmxa();
	double Cmza();
	double CRx();  //舵系数
	double CRy();
	void runge_kutta(double, double, int, double[], double);  //龙格库塔计算
	void rktf(double, double[], int, double[]);  //运动微分方程
};
void ACS::reset()
{
	int i;
	RollAngle.clear();   //清空仿真数据
	HeadingAngle.clear();
	Ox.clear();
	Oy.clear();
	Ov.clear();
	for (i = 0; i < 8; i++)
	{
		y[i] = 0;
	}
	factor = 1;
	ar = 0;
	Vw = 0;
	Bw = 0;
	hitv = 0;
	hitangle = 0;
}
//获取匀速状态factor
double ACS::Getfactor(double thev)
{
	return -0.0000299 * thev * thev * thev + 0.0134 * thev * thev + 0.0212 * thev + 0.00782;
}

//碰撞过程
void ACS::hit()
{
	y[4] = hitv * cos(hitangle) * cos(hitangle);
	y[5] = -1 * hitv * cos(hitangle) * sin(hitangle);
}
//大地坐标系x轴速度
double ACS::Oxv()
{
	return y[4] * cos(y[1]) - y[5] * sin(y[1]) * cos(y[0]);
}
//大地坐标系y轴速度
double ACS::Oyv()
{
	return y[4] * sin(y[1]) + y[5] * cos(y[1]) * cos(y[0]);
}
//相对风速
double ACS::Va()
{
	double midVa;
	midVa = sqrt(pow((y[4] + Vw * cos(Bw - y[1])), 2) + pow((y[5] + Vw * sin(Bw - y[1])), 2));
	return midVa;
}
//相对风向角
double ACS::Ba()
{
	double midBa = 0, del = 0;
	if ((y[4] + Vw * cos(Bw - y[1]) == 0))del = 0.00000000000000001;
	if ((y[5] + Vw * sin(Bw - y[1]) >= 0) && (y[4] + Vw * cos(Bw - y[1]) + del >= 0))
		midBa = atan((y[5] + Vw * sin(Bw - y[1])) / (y[4] + Vw * cos(Bw - y[1]) + del));
	else if ((y[5] + Vw * sin(Bw - y[1]) >= 0) && (y[4] + Vw * cos(Bw - y[1]) + del <= 0))
		midBa = (PI / 2) - atan((y[5] + Vw * sin(Bw - y[1])) / (y[4] + Vw * cos(Bw - y[1]) + del));
	else if ((y[5] + Vw * sin(Bw - y[1]) <= 0) && (y[4] + Vw * cos(Bw - y[1]) + del >= 0))
		midBa = atan((y[5] + Vw * sin(Bw - y[1])) / (y[4] + Vw * cos(Bw - y[1]) + del));
	else if ((y[5] + Vw * sin(Bw - y[1]) <= 0) && (y[4] + Vw * cos(Bw - y[1]) + del <= 0))
		midBa = -(PI / 2) - atan((y[5] + Vw * sin(Bw - y[1])) / (y[4] + Vw * cos(Bw - y[1]) + del));
	return midBa;
}
//取符号函数
double ACS::sign(double var)
{
	if (var > 0) return 1;
	else if (var == 0)return 0;
	else return -1;
}
double ACS::uaa()
{
	double miduaa;
	miduaa = Va() * cos(Ba()) + sqrt(fabs(T) / (Pa * Sd));
	return miduaa;
}

//气动力系数
double ACS::Cxa()
{
	double varcxa;
	if (fabs(Ba()) < 2 * PI / 9)
		varcxa = -0.0001326 * (Ba() * 180 / PI) * (Ba() * 180 / PI) + 0.5;
	else if (fabs(Ba()) < 22 * PI / 45)
		varcxa = -0.0000284 * fabs((Ba() * 180 / PI) * (Ba() * 180 / PI) * (Ba() * 180 / PI)) + 0.005 * (Ba() * 180 / PI) * (Ba() * 180 / PI) - 0.324 * fabs((Ba() * 180 / PI)) + 6.835;
	else
		varcxa = -0.0000877 * (Ba() * 180 / PI) * (Ba() * 180 / PI) + 0.018358 * (Ba() * 180 / PI) + 2.2423;
	return varcxa;
}
double ACS::Cya()
{
	return -0.013858 * Ba() * 180 / PI - 0.000077 * fabs(Ba() * 180 / PI) * Ba() * 180 / PI;
}
double ACS::Cmxa()
{
	return -0.0000000087 * Ba() * 0 / PI;
}
double ACS::Cmza()
{
	double varcmza;
	if (fabs(Ba()) < PI / 3)
		varcmza = 0.00167 * Ba() * 180 / PI;
	else if (fabs(Ba()) < 2 * PI / 3)
		varcmza = -0.000000230 * (Ba() * 180 / PI) * (Ba() * 180 / PI) * (Ba() * 180 / PI) + 0.0000615 * (Ba() * 180 / PI) * fabs(Ba() * 180 / PI) - 0.00876 * (Ba() * 180 / PI) + 0.4481 * sign(Ba() * 180 / PI);
	else
		varcmza = -0.00167 * (180 * sign(Ba() * 180 / PI) - Ba() * 180 / PI);
	return varcmza;
}
//舵系数
double ACS::CRx()
{
	double varcrx = 0;
	if (fabs(Ba()) <= 23 * PI / 180)
		varcrx = 0.0096 + 0.00116 * fabs(ar) + 0.000333 * ar * ar;
	else
		varcrx = -3.379 + 0.2516 * fabs(ar) - 0.00415 * ar * ar;
	return varcrx;
}
double ACS::CRy()
{
	double varcry = 0;
	if (fabs(Ba()) <= 25 * PI / 180)
		varcry = 0.063 * ar;
	else
		varcry = 43.75 * sign(ar) - 2.937 * ar + 0.05 * ar * fabs(ar);
	return varcry;
}
/**************************************************************************
变步长四阶龙格——库塔求解微分方程方法
函数名：runge_kutta

t           积分起始点。
h           积分步长。
n           一阶微分方程组中方程个数，也是未知函数个数。
y[n]        存放n个未知函数在起始点t处的函数值。
返回n个未知函数在t+h处的函数值。
eps         控制精度要求。
f           计算微分方程组中各方程右端函数值的函数名。
**************************************************************************/
void ACS::runge_kutta(double t, double h, int n, double y[], double eps)
{
	int m, i, j, k;
	double hh, p, dt, x, tt, q, a[4], *g, *b, *c, *d, *e;
	g = new double[n];
	b = new double[n];
	c = new double[n];
	d = new double[n];
	e = new double[n];
	hh = h; m = 1; p = 1.0 + eps; x = t;
	for (i = 0; i <= n - 1; i++) c[i] = y[i];
	while (p >= eps)
	{
		a[0] = hh / 2.0; a[1] = a[0]; a[2] = hh; a[3] = hh;
		for (i = 0; i <= n - 1; i++)
		{
			g[i] = y[i]; y[i] = c[i];
		}
		dt = h / m; t = x;
		for (j = 0; j <= m - 1; j++)
		{
			rktf(t, y, n, d);
			for (i = 0; i <= n - 1; i++)
			{
				b[i] = y[i]; e[i] = y[i];
			}
			for (k = 0; k <= 2; k++)
			{
				for (i = 0; i <= n - 1; i++)
				{
					y[i] = e[i] + a[k] * d[i];
					b[i] = b[i] + a[k + 1] * d[i] / 3.0;
				}
				tt = t + a[k];
				rktf(tt, y, n, d);
			}
			for (i = 0; i <= n - 1; i++) y[i] = b[i] + hh * d[i] / 6.0;
			t = t + dt;
		}
		p = 0.0;
		for (i = 0; i <= n - 1; i++)
		{
			q = fabs(y[i] - g[i]);
			if (q > p) p = q;
		}
		hh = hh / 2.0; m = m + m;
	}
	delete[] g; delete[] b; delete[] c; delete[] d; delete[] e;
	return;
}
/**************************************************************************
微分方程
函数名： rktf
y[0]-y[7]表示φ，ψ，ξ，η，u，v，p，r顺序对应
d[0]-d[7]表示φ，ψ，ξ，η，u，v，p，r的导函数
**************************************************************************/
void ACS::rktf(double t, double y[], int n, double d[])
{
	t = t; n = n;

	d[0] = y[6];
	d[1] = y[7] * cos(y[0]);
	d[2] = y[4] * cos(y[1]) - y[5] * sin(y[1]) * cos(y[0]);
	d[3] = y[4] * sin(y[1]) + y[5] * cos(y[1]) * cos(y[0]);

	d[4] = (((-0.5 * Pa * (pow(Va(), 2)) * Cxa() * Sa) + (2 * factor * T) - (CRx() * Pa * pow(uaa(), 2) * SR) - (Pa * Q * Va() * cos(Ba())) - 4 * y[4] - 0.5 * y[4] * y[4] * Sa - 0.1*M*hitv*sin(fabs(hitangle))) / M) + y[5] * y[7];
	d[5] = (((0.5 * Pa * pow(Va(), 2) * Cya() * Sa) + (CRy() * Pa * pow(uaa(), 2) * SR) - (Pa * Q * Va() * sin(Ba()))) / M) - y[4] * y[7];
	d[6] = (((0.5 * Pa * pow(Va(), 2) * Cmxa() * Sa * La) - (CRy() * Pa * pow(uaa(), 2) * SR * ZR) + (Zf * Pa * Q * Va() * sin(Ba())) - g * M * y[0]) / Ix) + 0.0006 * sin(t) + 0.0001 * cos(20 * t) + 0.00002 * sin(0.07 * t);
	d[7] = (((0.5 * Pa * pow(Va(), 2) * Cmza() * Sa * La) + +(CRy() * Pa * pow(uaa(), 2) * SR * XR) - (Xf * Pa * Q * Va() * sin(Ba())) + (Yf * Pa * Q * Va() * cos(Ba())) + M * hitv * sin(hitangle) * 0.5 * sqrt(La * La + B * B) * sin(0.5 * PI - atan(B / La) - fabs(hitangle))) / Iz);
	return;
}
void ACS::StraightRun(double startx, double endx, double speed)
{
	int  j;
	y[2] = startx;
	//加速过程
	for (j = 1; j <= steps; j++)
	{
		runge_kutta(t, h, 8, y, eps);
		RollAngle.push_back(y[0]);
		HeadingAngle.push_back(y[1]);
		Ox.push_back(y[2]);
		Oy.push_back(y[3]);
		Ov.push_back(sqrt(pow(Oxv(), 2) + pow(Oyv(), 2)));
		if (Ov[Ov.size() - 1] >= speed)break;
		t = t + h;
	}
	factor = Getfactor(speed);
	//匀速过程
	for (j = 1; j <= steps; j++)
	{
		runge_kutta(t, h, 8, y, eps);
		if (y[2] >= endx)break;
		RollAngle.push_back(y[0]);
		HeadingAngle.push_back(y[1]);
		Ox.push_back(y[2]);
		Oy.push_back(y[3]);
		Ov.push_back(sqrt(pow(Oxv(), 2) + pow(Oyv(), 2)));
		t = t + h;
	}
	//输出显示
	/*for (i = 0; i < Ox.size(); i++)
		cout << Ox[i] << endl;
	cout << Ov.size() << endl;
	cout << Ov[Ox.size() - 1] << endl;*/
}
vector<double> ACS::GetCoordinateXOfResult()
{
	return Ox;
}
//************************************
// Method:    GetRoutePointsOfHovercraft_Navigate
// FullName:  GetRoutePointsOfHovercraft_Navigate
// Access:    public 
// Returns:   std::vector<RoutePoint>
// Qualifier:气垫船自行行驶进舱仿真路线点获取函数
// Parameter: double startx	起始x
// Parameter: double endx	终止x
// Parameter: double const_y	恒定值y 
// Parameter: double speed	气垫船速度 1m/s
//************************************
std::vector<RoutePoint> GetRoutePointsOfHovercraft_Navigate(double startx, double endx, double const_y,double speed)
{
	std::vector<RoutePoint> route_point_of_hovercraft;
	ACS one;  //创建气垫船对象
	one.StraightRun(startx, endx, speed);//启动气垫船
	auto array_of_x = one.GetCoordinateXOfResult();
	for (auto x : array_of_x)
	{
		PPoint coordinate(x,const_y);
		RoutePoint tmp(coordinate,0,0,0);
		route_point_of_hovercraft.push_back(tmp);
	}
	return route_point_of_hovercraft;
}

//************************************
// Method:    Towing
// FullName:  Towing
// Access:    public 
// Returns:   TowedShip	气垫船下一位置x与角度angle，如果档位输入不合理，则输出的x位置为-1
// Qualifier:	根据当前绞车挡位和气垫船位置输出下一时刻（经过dt时间间隔后）的气垫船位置和艏向角
// Parameter: int L	左档位(L:0~3)
// Parameter: int R	右档位(R:0~3)
// Parameter: double x	气垫船当前x坐标（x）
//************************************
TowedShip Towing(int L, int R, double x)
{
	TowedShip towed_ship;
	towed_ship.x = -1;

	const double rate[4] = { 0, 0.5, 1, 1.5 };//挡位与速率对照表
	const double dt = 0.1; //时间间隔
	if (L > R)
	{
		towed_ship.x = rate[L] * dt + x;
		towed_ship.angle = (R - L) * 1.5;
	}
	else
	{
		towed_ship.x = rate[R] * dt + x;
		towed_ship.angle = (R - L) * 1.5;
	}
	return towed_ship;
}
