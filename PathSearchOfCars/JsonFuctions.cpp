#include "pch.h"
#include "JsonFuctions.h"

std::string createJson()
{
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;

	/*root["Name"] = "Liming";
	root["Age"] = 26;*/
	for (int i = 0; i < 5; ++i)
	{
		Json::Value c_info;
		c_info["vehicle_id"] = i;
		c_info["vehicle_type"] = 2;
		cars_info[i] = c_info;
	}
	root["cars_info"] = cars_info;
	for (int i = 0; i < 5; ++i)
	{
		Json::Value o_info;
		o_info["direction"] = 15.6;
		o_info["vehicle_type"] = i;
		o_info["coordinates_x"] = 2;
		o_info["coordinates_y"] = 6;
		obstacles_info[i] = o_info;
	}
	root["obstacles_info"] = obstacles_info;


	/*lang[0] = "C++";
	lang[1] = "Java";
	root["Language"] = lang;

	mail["Netease"] = "lmshao@163.com";
	mail["Hotmail"] = "liming.shao@hotmail.com";
	root["E-mail"] = mail;
*/
	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();
	#ifdef  _DEBUG  
	std::cout << "Json:\n" << jsonStr << std::endl;
	#endif 
	
	return jsonStr;
}
bool parseJson(const std::string &info)
{
	if (info.empty())
		return false;

	bool res;
	JSONCPP_STRING errs;
	Json::Value root, obstacles_info, cars_info;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		std::cerr << "parseJson err. " << errs << std::endl;
	}
	obstacles_info = root["obstacles_info"];
	#ifdef  _DEBUG  
	std::cout << "obstacles_info: ";
	#endif

	int a = 0;
	for (size_t i = 0; i < obstacles_info.size(); ++i) {
		//std::cout << obstacles_info[i] << " ";
		auto info = obstacles_info[i];
		#ifdef  _DEBUG  
		std::cout << info["vehicle_type"].asInt() << std::endl;
		#endif
		

	}
#ifdef  _DEBUG  
	std::cout << std::endl;
#endif
	



	cars_info = root["cars_info"];
	std::cout << "ocars_info: ";
	for (size_t i = 0; i < cars_info.size(); ++i) {
		std::cout << cars_info[i] << " ";
	}
#ifdef  _DEBUG  
	std::cout << std::endl;
#endif

	return true;
}
void JsonTest()
{
	auto str = createJson();
	parseJson(str);
}
std::string createNullJson()
{
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	//std::cout << "Json:\n" << jsonStr << std::endl;
	return jsonStr;
}
std::string createJsonfuncForTest1(std::vector<Car_Info>& obstacle_info)
{
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	root["length_unit"] = 1;
	for (int i = 0; i < 5; ++i)
	{
		Json::Value c_info1;
		c_info1["vehicle_id"] = i+1;
		c_info1["vehicle_type"] = 2;
		cars_info[i] = c_info1;
	}
	for (int i = 5; i < 10; ++i)
	{
		Json::Value c_info1;
		c_info1["vehicle_id"] = i + 1;
		c_info1["vehicle_type"] = 3;
		cars_info[i] = c_info1;
	}
	for (int i = 10; i < 50; ++i)
	{
		Json::Value c_info1;
		c_info1["vehicle_id"] = i + 1;
		c_info1["vehicle_type"] = 4;
		cars_info[i] = c_info1;
	}
	root["cars_info"] = cars_info;
	for (size_t i = 0; i < obstacle_info.size(); ++i)
	{
		Json::Value c_info;
		c_info["vehicle_type"] = 2;
		c_info["coordinates_x"] = obstacle_info.at(i).car_center.x * 1;
		c_info["coordinates_y"] = obstacle_info.at(i).car_center.y * 1;
		c_info["direction"] = obstacle_info.at(i).angle;
		obstacles_info[i] = c_info;
	}
	root["obstacles_info"] = obstacles_info;


	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

#ifdef  _DEBUG  

	//std::cout << "Json:\n" << jsonStr << std::endl;
#endif
	return jsonStr;
}
std::string createJsonfuncForTest2(std::vector<Car_Info>& obstacle_info, std::vector<PPoint>& guide_points)
{
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	root["length_unit"] = 1;
	root["target_vehicle_type"]=2;
	root["start_coordinates_x"]= guide_points.front().x * 1;
	root["start_coordinates_y"] = guide_points.front().y  * 1;
	root["end_coordinates_x"] = guide_points.back().x  * 1;
	root["end_coordinates_y"] = guide_points.back().y  * 1;
	root["start_direction"] = 0;
	root["end_direction"] = 0;
	root["car_speed"] = 4.2;
	for (size_t i = 0; i < obstacle_info.size(); ++i)
	{
		Json::Value c_info;
		c_info["vehicle_type"] = 2;
		c_info["coordinates_x"] = obstacle_info.at(i).car_center.x * 1;
		c_info["coordinates_y"] = obstacle_info.at(i).car_center.y * 1;
		c_info["direction"] = obstacle_info.at(i).angle;
		obstacles_info[i] = c_info;
	}
	root["obstacles_info"] = obstacles_info;
	

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	//std::cout << "Json:\n" << jsonStr << std::endl;
	return jsonStr;

}
std::string createJsonfuncForTest3(std::vector<Car_Info>& obstacle_info, std::vector<PPoint>& guide_points)
{
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	root["length_unit"] = 1;
	root["target_vehicle_type"] = 2;
	root["start_direction"] = 0;
	root["end_direction"] = 0;
	root["car_speed"] = 4.2;
	for (size_t i = 0; i < obstacle_info.size(); ++i)
	{
		Json::Value c_info;
		c_info["vehicle_type"] = 2;
		c_info["coordinates_x"] = obstacle_info.at(i).car_center.x * 1;
		c_info["coordinates_y"] = obstacle_info.at(i).car_center.y * 1;
		c_info["direction"] = obstacle_info.at(i).angle;
		obstacles_info[i] = c_info;
	}
	root["obstacles_info"] = obstacles_info;

	for (size_t i = 0; i < guide_points.size(); ++i)
	{
		Json::Value c_info;
		c_info["guide_point_id"] = i + 1;
		c_info["coordinates_x"] = guide_points.at(i).x * 1;
		c_info["coordinates_y"] = guide_points.at(i).y * 1;
		cars_info[i] = c_info;
	}
	root["guide_points"] = cars_info;

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	//std::cout << "Json:\n" << jsonStr << std::endl;
	return jsonStr;
}
std::string createJsonfuncForTest4(std::vector<Car_Info>& obstacle_info, std::vector<PPoint>& guide_points)
{
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	root["length_unit"] = 1;
	root["vehicle_type"] = 2;
	root["start_direction"] = 0;
	root["end_direction"] = 0;
	root["car_speed"] = 4.2;
	for (size_t i = 0; i < obstacle_info.size(); ++i)
	{
		Json::Value c_info;
		c_info["vehicle_type"] = 2;
		c_info["coordinates_x"] = obstacle_info.at(i).car_center.x * 1;
		c_info["coordinates_y"] = obstacle_info.at(i).car_center.y * 1;
		c_info["direction"] = obstacle_info.at(i).angle;
		obstacles_info[i] = c_info;
	}
	root["obstacles_info"] = obstacles_info;

	for (size_t i = 0; i < guide_points.size(); ++i)
	{
		Json::Value c_info;
		c_info["guide_point_id"] = i + 1;
		c_info["coordinates_x"] = guide_points.at(i).x * 1;
		c_info["coordinates_y"] = guide_points.at(i).y * 1;
		cars_info[i] = c_info;
	}
	root["guide_points"] = cars_info;

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	//std::cout << "Json:\n" << jsonStr << std::endl;
	return jsonStr;
}


std::string createJsonfuncForTestVehicleLocationRequest(std::vector<Car_Info>& obstacle_info, std::vector<PPoint>& guide_points, int vehicle_id, int route_point_id) {
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	
	root["vehicle_type"] = 2;
	root["start_direction"] = 0;
	root["end_direction"] = 0;
	root["car_speed"] = 4.2;
	root["vehicle_id"] = vehicle_id;
	root["route_point_id"] = route_point_id;
	for (size_t i = 0; i < obstacle_info.size(); ++i)
	{
		Json::Value c_info;
		c_info["vehicle_type"] = 2;
		c_info["coordinates_x"] = obstacle_info.at(i).car_center.x ;
		c_info["coordinates_y"] = obstacle_info.at(i).car_center.y ;
		c_info["direction"] = obstacle_info.at(i).angle;
		obstacles_info[i] = c_info;
	}
	root["obstacles_info"] = obstacles_info;

	for (size_t i = 0; i < guide_points.size(); ++i)
	{
		Json::Value c_info;
		c_info["guide_point_id"] = i + 1;
		c_info["coordinates_x"] = guide_points.at(i).x * 1;
		c_info["coordinates_y"] = guide_points.at(i).y * 1;
		cars_info[i] = c_info;
	}
	root["guide_points"] = cars_info;

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	//std::cout << "Json:\n" << jsonStr << std::endl;
	return jsonStr;
}
std::string createJsonfuncForTestHovercraftInitiativeRequest(double start_x, double end_x, int vehicle_id, int route_point_id) {
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	root["start_x"] = start_x;
	root["end_x"] = end_x;
	root["vehicle_id"] = vehicle_id;
	root["route_point_id"] = route_point_id;

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	//std::cout << "Json:\n" << jsonStr << std::endl;
	return jsonStr;
}
std::string createJsonfuncForTestHovercraftPassivelyRequest(double cur_x, int left_gear, int right_gear) {
	std::string jsonStr;
	Json::Value root, obstacles_info, cars_info;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	root["cur_x"] = cur_x;
	root["left_gear"] = left_gear;
	root["right_gear"] = right_gear;
	
	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	//std::cout << "Json:\n" << jsonStr << std::endl;
	return jsonStr;
}

void changeCarsArray(size_t& vhicle_id,int vhicle_size, Json::Value& CarArray,int array_id,int ishovercraft,int hovercraft_id) {
	/*Json::Value cars_info, cur_cars, cur_cars_coor;
	cars_info["ishovercraft"] = ishovercraft;
	cars_info["hovercraft_id"] = hovercraft_id;
	size_t i = 0;
	for (; i < vhicle_size; ++i)
	{
		Json::Value c_info;
		c_info["vehicle_id"] = vhicle_id + 1+i;
		c_info["vehicle_type"] = 2;
		cur_cars[i] = c_info;
	}
	vhicle_id += vhicle_size;
	if (ishovercraft == 1) {
		for (int j = 0; j < vhicle_size; ++j)
		{
			Json::Value c_info;
			c_info["x_relative"] = 0;
			c_info["y_relative"] = 0;
			cur_cars_coor[j] = c_info;
		}

	}
	cars_info["cars_info"] = cur_cars;
	cars_info["cars_coordinate"] = cur_cars_coor;
	CarArray[array_id] = cars_info;*/
	Json::Value cars_info, cur_cars, cur_cars_coor;
	cars_info["hovercraft_id"] = hovercraft_id;
	size_t i = 0;
	for (; i < vhicle_size; ++i)
	{
		Json::Value c_info;
		c_info["vehicle_id"] = vhicle_id + 1 + i;
		c_info["vehicle_type"] = 2;
		c_info["moor_time"] = 3;
		c_info["unmoor_time"] = 6;
		cur_cars[i] = c_info;
	}
	vhicle_id += vhicle_size;
	//if (ishovercraft == 1) {
	//	for (int j = 0; j < vhicle_size; ++j)
	//	{
	//		Json::Value c_info;
	//		c_info["x_relative"] = 0;
	//		c_info["y_relative"] = 0;
	//		cur_cars_coor[j] = c_info;
	//	}

	//}
	cars_info["cars_info"] = cur_cars;
	CarArray[array_id] = cars_info;
}
//************************************
// Method:    initVihicleArray
// FullName:  initVihicleArray
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: size_t & vhicle_id 载具id
// Parameter: int vhicle_size	当前载具数组的载具数量
// Parameter: Json::Value & CarArray	
// Parameter: int array_id	数组id
// Parameter: int hovercraft_id	气垫船id，为0表示没有气垫船
//************************************
void initVihicleArray(size_t& vhicle_id, int vhicle_size, Json::Value& CarArray, int array_id, int hovercraft_id) {
	
	Json::Value cars_info, cur_cars, cur_cars_coor;
	cars_info["hovercraft_id"] = hovercraft_id;
	size_t i = 0;
	for (; i < vhicle_size; ++i)
	{
		Json::Value c_info;
		c_info["vehicle_id"] = vhicle_id + 1 + i;
		c_info["vehicle_type"] = 2;
		c_info["moor_time"] = 3;
		c_info["unmoor_time"] = 6;
		cur_cars[i] = c_info;
	}
	vhicle_id += vhicle_size;

	cars_info["cars_info"] = cur_cars;
	CarArray[array_id] = cars_info;
}
void initHovercraftInfo(size_t vhicle_start_id, int vhicle_size, Json::Value& HovercraftArray, int array_id, int hovercraft_id,int type) {

	Json::Value cars_info, times_info, cur_cars_coor;
	cars_info["hovercraft_id"] = hovercraft_id;

	for (int j = 0; j < vhicle_size; ++j)
	{
		Json::Value c_info;
		c_info["vhicle_id"] = vhicle_start_id + j;
		c_info["x_relative"] = 1;
		c_info["y_relative"] = 2;
		cur_cars_coor[j] = c_info;
	}

	cars_info["cars_coordinate"] = cur_cars_coor;
	cars_info["type"] = type;
	if (type == 1) {
		for (int j = 0; j < 4; ++j)
		{
			times_info[j] = 3;
		}
	}
	if (type == 2) {
		for (int j = 0; j < 5; ++j)
		{
			times_info[j] = 3;
		}
	}
	if (type == 3) {
		for (int j = 0; j < 4; ++j)
		{
			times_info[j] = 3;
		}
	}
	cars_info["times"] = times_info;
	HovercraftArray[array_id] = cars_info;
}
std::string createJsonfuncForNewCars()
{
	std::string jsonStr;
	Json::Value root, obstacles_info, CarArray, HovercraftArray;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;

	size_t i = 0;
	/*changeCarsArray(i,6,CarArray,0,0,0);
	changeCarsArray(i, 4, CarArray,1,1,1);
	changeCarsArray(i, 5, CarArray, 2, 0, 0);
	changeCarsArray(i, 3, CarArray, 3,1,2);*/
	initVihicleArray(i, 6, CarArray, 0, 0);
	initVihicleArray(i, 4, CarArray, 1, 1);
	initVihicleArray(i, 5, CarArray, 2, 0);
	initVihicleArray(i, 3, CarArray, 3, 2);
	initHovercraftInfo(7, 4, HovercraftArray, 0, 1, 1);
	initHovercraftInfo(16, 3, HovercraftArray, 1, 2, 2);

	root["vihicle_array"] = CarArray;
	root["hovercraft_array"] = HovercraftArray;
	

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	std::cout << "Json:\n" << jsonStr << std::endl;
	return jsonStr;
}
//************************************
// Method:    ParseJsonForLayoutPlan
// FullName:  ParseJsonForLayoutPlan
// Access:    public 
// Returns:   void
// Qualifier: 为智能布列方案接口解析输入的json数据
// Parameter: const std::string & info	输入的json数据
// Parameter: std::vector<car_info> & obstacles_info	输出的障碍物载具信息
// Parameter: std::vector<std::vector<int>> & cars_info	目标载具的信息
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
void ParseJsonForLayoutPlan(const std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector <std::vector<std::vector<int>>>& cars_info, double& len_unit, std::unordered_map<int, int>& cars_id_type)
{
	CLogFile logfile;
	// 打开日志文件，如果"/tmp/log"不存在，就创建它，但是要确保当前用户具备创建目录的权限。
	if (logfile.Open("log.txt") == false)
	{
		printf("logfile.Open(log.txt) failed.\n");
		return;
	}
	if (info.empty())
		return ;
	bool res;
	JSONCPP_STRING errs;
	Json::Value root, _obstacles_info, _cars_info;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		logfile.Write("ParseJsonForLayoutPlan解析json出错。\n");
		return;
		//std::cerr << "parseJson err. " << errs << std::endl;
	}
	//获取长度单位
	double px_to_m = 1;
	len_unit= root["length_unit"].asDouble();
	if(len_unit != 0)
		px_to_m = 1 / len_unit;
	//获取障碍物载具数据
	_obstacles_info = root["obstacles_info"];
	for (size_t i = 0; i < _obstacles_info.size(); ++i) {
		auto info = _obstacles_info[i];
		Car_Info car;
		if (info["vehicle_type"].asInt() == 2)
		{
			car.car_length = 9.6 ;
			car.car_width = 3.4;
		}
		else
		{
			car.car_length = 8.2;
			car.car_width = 3.4;
		}
		car.car_center.x = info["coordinates_x"].asDouble() * px_to_m;
		car.car_center.y = info["coordinates_y"].asDouble() * px_to_m;
		car.angle = info["direction"].asDouble();
		obstacles_info.push_back(car);
	}
	//获取目标载具数据
	//分批次获取载具
	_cars_info = root["cars_info"];
	
	for (size_t i = 0; i < _cars_info.size(); ++i) {
		auto info = _cars_info[i];
		std::vector<int> tmp(2);
		tmp[0] = info["vehicle_id"].asInt();
		tmp[1] = info["vehicle_type"].asInt();
		cars_id_type[tmp[0]] = tmp[1];
	}

	int batch_id = 1;
	while (true) {
		std::vector<std::vector<int>> batch_cars_info;
		for (size_t i = 0; i < _cars_info.size(); ++i) {
			auto info = _cars_info[i];
			if (info["batch_id"].asInt() == batch_id) {
				std::vector<int> tmp(2);
				tmp[0] = info["vehicle_id"].asInt();
				tmp[1] = info["vehicle_type"].asInt();
				batch_cars_info.push_back(tmp);
			}
		}
		if (batch_cars_info.empty()) break;
		cars_info.push_back(batch_cars_info);	
		batch_id++;
	}
	
}
//************************************
// Method:    createJsonForLayoutPlan
// FullName:  createJsonForLayoutPlan
// Access:    public 
// Returns:   std::string	打包的json数据
// Qualifier:将智能布列方案的输出打包为json数据
// Parameter: std::vector<layout_plan_end> & layout_plan	布列方案
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
std::string CreateJsonForLayoutPlan(std::vector<Layout_Plan_End>& layout_plan, double& len_unit)
{
	std::string jsonStr;
	Json::Value root,_layout_plan;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;

	int i = 0;
	for (auto & car : layout_plan)
	{
		Json::Value c_info;
		c_info["vehicle_id"] = car.vehicle_id;
		c_info["series_number"] = car.series_number;
		c_info["end_coordinates_x"] = car.end_coordinates_x * len_unit;
		c_info["end_coordinates_y"] = car.end_coordinates_y * len_unit;
		c_info["direction"] = car.direction;
		_layout_plan[i++] = c_info;
	}
	root["layout_plan"] = _layout_plan;
	
	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();
#ifdef  _DEBUG  

	std::cout << "Json:\n" << jsonStr << std::endl;
#endif
	
	return jsonStr;
}

//************************************
// Method:    ParseJsonForIntelligentPathPlan
// FullName:  ParseJsonForIntelligentPathPlan
// Access:    public 
// Returns:   void
// Qualifier:为智能路径规划接口解析输入的json数据
// Parameter: const std::string & info	输入的json数据
// Parameter: std::vector<car_info> & obstacles_info	障碍物载具信息
// Parameter: ipp_input & get_car_info	目标载具的信息
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
void ParseJsonForIntelligentPathPlan(const std::string &info, std::vector<Car_Info>&  obstacles_info, Ipp_Input& get_car_info, double& len_unit)
{
	CLogFile logfile;
	// 打开日志文件，如果"/tmp/log"不存在，就创建它，但是要确保当前用户具备创建目录的权限。
	if (logfile.Open("log.txt") == false)
	{
		printf("logfile.Open(log.txt) failed.\n");
		return;
	}
	if (info.empty())
		return;

	bool res;
	JSONCPP_STRING errs;
	Json::Value root, _obstacles_info;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		logfile.Write("ParseJsonForIntelligentPathPlan解析json出错。\n");
		return;
	}
	//获取长度单位
	double px_to_m = 1;
	len_unit = root["length_unit"].asDouble();
	if (len_unit != 0)
		px_to_m = 1 / len_unit;
	//获取基本数据
	get_car_info.target_vehicle_type = root["target_vehicle_type"].asInt();
	get_car_info.start.x = root["start_coordinates_x"].asDouble() * px_to_m;
	get_car_info.start.y = root["start_coordinates_y"].asDouble() * px_to_m;
	get_car_info.end.x = root["end_coordinates_x"].asDouble() * px_to_m;
	get_car_info.end.y = root["end_coordinates_y"].asDouble() * px_to_m;
	get_car_info.start_direction = root["start_direction"].asDouble();
	get_car_info.end_direction = root["end_direction"].asDouble();
	get_car_info.speed = root["car_speed"].asDouble();

	//获取障碍物载具数据
	_obstacles_info = root["obstacles_info"];
	for (size_t i = 0; i < _obstacles_info.size(); ++i) {
		auto info = _obstacles_info[i];
		Car_Info car;
		if (info["vehicle_type"].asInt() == 2)
		{
			car.car_length = 9.6;
			car.car_width = 3.4;
		}
		else
		{
			car.car_length = 8.2;
			car.car_width = 3.4;
		}
		car.car_center.x = info["coordinates_x"].asDouble() * px_to_m;
		car.car_center.y = info["coordinates_y"].asDouble() * px_to_m;
		car.angle = info["direction"].asDouble();
		obstacles_info.push_back(car);
	}
}
//************************************
// Method:    createJsonIntelligentPathPlan
// FullName:  createJsonIntelligentPathPlan
// Access:    public 
// Returns:   std::string
// Qualifier:将智能路径规划的输出打包为json数据
// Parameter: std::vector<PPoint> & guide_points	导引点数组
// Parameter: int path_status	路径状态 
// Parameter: double cost_time	花费时间
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
std::string CreateJsonForIntelligentPathPlan(std::vector<PPoint>& guide_points, int path_status, double cost_time, double& len_unit)
{
	std::string jsonStr;
	Json::Value root, _guide_points;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;

	root["path_status"] = path_status;
	root["cost_time"] = cost_time;

	int i = 1;
	for (auto & p : guide_points)
	{
		Json::Value guide_point;

		guide_point["guide_point_id"] = i;
		guide_point["coordinates_x"] = p.x * len_unit;
		guide_point["coordinates_y"] = p.y * len_unit;
		_guide_points[i-1] = guide_point;
		i++;
	}
	root["guide_points"] = _guide_points;

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

#ifdef  _DEBUG  
	std::cout << "Json:\n" << jsonStr << std::endl;
#endif
	return jsonStr;
}
//************************************
// Method:    ParseJsonForPathPlanCheck
// FullName:  ParseJsonForPathPlanCheck
// Access:    public 
// Returns:   void
// Qualifier:为路径规划检查接口解析输入的json数据
// Parameter: const std::string & info	输入的json数据
// Parameter: std::vector<car_info> & obstacles_info	障碍物载具数组
// Parameter: std::vector<PPoint> & guide_points	导引点数组
// Parameter: ipp_input & get_car_info
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
void ParseJsonForPathPlanCheck(const std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector<PPoint>& guide_points, Ipp_Input& get_car_info, double& len_unit)
{
	if (info.empty())
		return;

	bool res;
	JSONCPP_STRING errs;
	Json::Value root, _obstacles_info,_guide_points;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		std::cerr << "parseJson err. " << errs << std::endl;
	}
	//获取长度单位
	double px_to_m = 1;
	len_unit = root["length_unit"].asDouble();
	if (len_unit != 0)
		px_to_m = 1 / len_unit;
	//获取基本数据
	get_car_info.target_vehicle_type = root["target_vehicle_type"].asInt();
	get_car_info.start_direction = root["start_direction"].asDouble();
	get_car_info.end_direction = root["end_direction"].asDouble();
	get_car_info.speed = root["car_speed"].asDouble();

	//获取障碍物载具数据
	_obstacles_info = root["obstacles_info"];
	for (size_t i = 0; i < _obstacles_info.size(); ++i) {
		auto info = _obstacles_info[i];
		Car_Info car;
		if (info["vehicle_type"].asInt() == 2)
		{
			car.car_length = 9.6;
			car.car_width = 3.4;
		}
		else
		{
			car.car_length = 8.2;
			car.car_width = 3.4;
		}
		car.car_center.x = info["coordinates_x"].asDouble() * px_to_m;
		car.car_center.y = info["coordinates_y"].asDouble() * px_to_m;
		car.angle = info["direction"].asDouble();
		obstacles_info.push_back(car);
	}
	//获取导引点数组数据
	_guide_points = root["guide_points"];
	for (size_t i = 0; i < _guide_points.size(); ++i) {
		auto info = _guide_points[i];
		PPoint p;
		p.x = info["coordinates_x"].asDouble() * px_to_m;
		p.y = info["coordinates_y"].asDouble() * px_to_m;
		guide_points.push_back(p);
	}

}
//************************************
// Method:    CreateJsonForPathPlanCheck
// FullName:  CreateJsonForPathPlanCheck
// Access:    public 
// Returns:   std::string
// Qualifier:将路径规划检查接口的输出打包为json数据
// Parameter: std::vector<int> & bad_guide_points	发生碰撞的导引点
// Parameter: int path_status	路径状态
// Parameter: double cost_time	花费时间
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
std::string CreateJsonForPathPlanCheck(std::vector<int>& bad_guide_points, int path_status, double cost_time, double& len_unit)
{
	std::string jsonStr;
	Json::Value root, _bad_guide_points;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;

	root["path_status"] = path_status;
	root["cost_time"] = cost_time;

	int i = 1;
	for (auto & id : bad_guide_points)
	{
		Json::Value guide_point_id;
		guide_point_id["guide_point_id"] = id;

		_bad_guide_points[i - 1] = guide_point_id;
		i++;
	}
	root["bad_guide_points"] = _bad_guide_points;

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

#ifdef  _DEBUG  
	std::cout << "Json:\n" << jsonStr << std::endl;
#endif
	return jsonStr;
}
//************************************
// Method:    ParseJsonForSimulation_Demonstration
// FullName:  ParseJsonForSimulation_Demonstration
// Access:    public 
// Returns:   void
// Qualifier:为单个载具演示跟随路径行驶接口解析输入的json数据
// Parameter: std::string & info	输入的json数据
// Parameter: std::vector<car_info> & obstacles_info	障碍物载具数组
// Parameter: std::vector<PPoint> & guide_points	导引点数组
// Parameter: ipp_input & get_car_info	目标载具的的基本信息
// Parameter: int & route_point_id		请求的路径点序号
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
void ParseJsonForSimulation_Demonstration(std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector<PPoint>& guide_points, Ipp_Input& get_car_info, double& len_unit)
{
	if (info.empty())
		return;

	bool res;
	JSONCPP_STRING errs;
	Json::Value root, _obstacles_info, _guide_points;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		std::cerr << "parseJson err. " << errs << std::endl;
	}
	//获取长度单位
	double px_to_m = 1;
	len_unit = root["length_unit"].asDouble();
	if (len_unit != 0)
		px_to_m = 1 / len_unit;
	//获取基本数据
	get_car_info.target_vehicle_type = root["vehicle_type"].asInt();
	get_car_info.start_direction = root["start_direction"].asDouble();
	get_car_info.end_direction = root["end_direction"].asDouble();
	get_car_info.speed = root["car_speed"].asDouble();
	

	//获取障碍物载具数据
	_obstacles_info = root["obstacles_info"];
	for (size_t i = 0; i < _obstacles_info.size(); ++i) {
		auto info = _obstacles_info[i];
		Car_Info car;
		if (info["vehicle_type"].asInt() == 2)
		{
			car.car_length = 9.6;
			car.car_width = 3.4;
		}
		else
		{
			car.car_length = 8.2;
			car.car_width = 3.4;
		}
		car.car_center.x = info["coordinates_x"].asDouble() * px_to_m;
		car.car_center.y = info["coordinates_y"].asDouble() * px_to_m;
		car.angle = info["direction"].asDouble();
		obstacles_info.push_back(car);
	}
	//获取导引点数组数据
	_guide_points = root["guide_points"];
	////先获得坞舱起点，坐标为起点x坐标-100
	//{
	//	auto info = _guide_points[0];
	//	PPoint p;
	//	p.x = info["coordinates_x"].asDouble() * px_to_m -100;
	//	p.y = info["coordinates_y"].asDouble() * px_to_m;
	//	guide_points.push_back(p);
	//}
	
	for (size_t i = 0; i < _guide_points.size(); ++i) {
		auto info = _guide_points[i];
		PPoint p;
		p.x = info["coordinates_x"].asDouble() * px_to_m;
		p.y = info["coordinates_y"].asDouble() * px_to_m;
		
		guide_points.push_back(p);
	}

}


//************************************
// Method:    CreateJsonForSimulation_Demonstration
// FullName:  CreateJsonForSimulation_Demonstration
// Access:    public 
// Returns:   std::string
// Qualifier:将单个载具演示跟随路径行驶接口的输出打包为json数据
// Parameter: int route_point_id	路径点序号
// Parameter: int route_point_status	路径点状态
// Parameter: PPoint coordinate		路径点坐标
// Parameter: double direction		路径点方向
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
std::string CreateJsonForSimulation_Demonstration(std::vector<RoutePoint> routePoints, size_t first_collison_id, double& len_unit)
{
	std::string jsonStr;
	Json::Value root, points;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;

	if (routePoints.empty()) {
		root["route_status"] = 0;
	}
	else {
		root["route_status"] = 1;
		int i = 1;
		for (auto & p : routePoints)
		{
			Json::Value curPoint;

			curPoint["route_point_id"] = i;
			//根据是否碰撞计算路径点状态
			if (first_collison_id != 0) {
				if (i < first_collison_id)
					curPoint["route_point_status"] = 2;
				else
					curPoint["route_point_status"] = 1;
			}
			else {
				if (i < routePoints.size())
					curPoint["route_point_status"] = 2;
				else
					curPoint["route_point_status"] = 3;
			}

			curPoint["coordinates_x"] = p.coordinate.x * len_unit;
			curPoint["coordinates_y"] = p.coordinate.y * len_unit;
			curPoint["direction"] = p.angle_z;

			points[i - 1] = curPoint;
			i++;
			//如果到了最后一点或者遇到了碰撞点，则直接退出
			if (curPoint["route_point_status"] == 3 || curPoint["route_point_status"] == 1)
				break;
		}
		root["points"] = points;

		//如果发生碰撞或者调度完成，添加附加信息
		if (first_collison_id != 0)
			root["used_time"] = first_collison_id * 0.1;
		else
			root["used_time"] = routePoints.size() * 0.1;
	
	}
		
	
	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();
#ifdef  _DEBUG  
	std::cout << "Json:\n" << jsonStr << std::endl;
#endif
	
	return jsonStr;
}
//************************************
// Method:    ParseJsonForScheduling_Simulation
// FullName:  ParseJsonForScheduling_Simulation
// Access:    public 
// Returns:   void
// Qualifier:为多车调度接口解析输入的json数据
// Parameter: std::string & info	输入的json数据
// Parameter: std::vector<car_info> & obstacles_info	障碍物载具数组
// Parameter: std::vector<car_guide_points> & all_guide_points	所有目标载具导引点数组
// Parameter: std::vector<ipp_input> & all_cars_info	所有目标载具基本信息
// Parameter: std::vector<int> & vehicles_id	所有目标载具的编号数组
// Parameter: int & vehicle_id		请求路径点的载具编号
// Parameter: int & route_point_id	请求路径点的编号
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
//************************************
void ParseJsonForScheduling_Simulation(std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector<Car_Guide_Points>& all_guide_points, std::vector<Ipp_Input>& all_cars_info, std::vector<int>& vehicles_id, double& len_unit)
{
	if (info.empty())
		return;

	bool res;
	JSONCPP_STRING errs;
	Json::Value root, _obstacles_info, _all_guide_points,_all_cars_info;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		std::cerr << "parseJson err. " << errs << std::endl;
	}

	//获取长度单位
	double px_to_m = 1;
	len_unit = root["length_unit"].asDouble();
	if (len_unit != 0)
		px_to_m = 1 / len_unit;
	//获取障碍物载具数据
	_obstacles_info = root["obstacles_info"];
	for (size_t i = 0; i < _obstacles_info.size(); ++i) {
		auto info = _obstacles_info[i];
		Car_Info car;
		if (info["vehicle_type"].asInt() == 2)
		{
			car.car_length = 9.6;
			car.car_width = 3.4;
		}
		else
		{
			car.car_length = 8.2;
			car.car_width = 3.4;
		}
		car.car_center.x = info["coordinates_x"].asDouble() * px_to_m;
		car.car_center.y = info["coordinates_y"].asDouble() * px_to_m;
		car.angle = info["direction"].asDouble();
		obstacles_info.push_back(car);
	}

	//获取所有载具导引点数组数据
	_all_guide_points = root["all_guide_points"];
	for (size_t i = 0; i < _all_guide_points.size(); ++i) {
		auto info = _all_guide_points[i];
		Car_Guide_Points tem;
		tem.vehicle_id = info["vehicle_id"].asInt();
		auto points = info["guide_points"];
		for (size_t j = 0; j < points.size(); ++j)
		{
			auto a = points[j];
			PPoint p;
			p.x = a["coordinates_x"].asDouble() * px_to_m;
			p.y = a["coordinates_y"].asDouble() * px_to_m;
			tem.guide_points.push_back(p);
		}
		all_guide_points.push_back(tem);
	}

	//获取所有目标载具信息数据
	_all_cars_info = root["all_cars_info"];
	for (size_t i = 0; i < _all_cars_info.size(); ++i) {
		auto info = _all_cars_info[i];
		Ipp_Input car;
		auto id = info["vehicle_id"].asInt();
		vehicles_id.push_back(id);
		car.target_vehicle_type = info["target_vehicle_type"].asInt();
		car.start_direction = info["start_direction"].asDouble();
		car.end_direction = info["end_direction"].asDouble();
		car.speed = info["car_speed"].asDouble();
		all_cars_info.push_back(car);
	}

}
//************************************
// Method:    CreateJsonForScheduling_Simulation
// FullName:  CreateJsonForScheduling_Simulation
// Access:    public 
// Returns:   std::string
// Qualifier:将多车调度接口的输出打包为json数据
// Parameter: int vehicle_id	当前路径点对应的载具编号
// Parameter: int route_point_id	当前路径点对应的编号
// Parameter: int route_point_status	当前路径点状态
// Parameter: PPoint coordinate	当前路径点的坐标
// Parameter: double direction	当前路径点的方向
// Parameter: double & len_unit 长度单位 px/m 像素长度/实际长度
// Parameter: int vehicle_type 当前载具类型 默认为2
//************************************
std::string CreateJsonForScheduling_Simulation(int vehicle_id, int route_point_id, int route_point_status, PPoint coordinate, double direction, double grade,double time_used, double& len_unit, int vehicle_type)
{
	std::string jsonStr;
	Json::Value root, _bad_guide_points;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;

	root["vehicle_id"] = vehicle_id;
	root["route_point_id"] = route_point_id;
	root["route_point_status"] = route_point_status;
	root["coordinates_x"] = coordinate.x * len_unit;
	root["coordinates_y"] = coordinate.y * len_unit;
	root["vehicle_type"] = vehicle_type;
	root["direction"] = direction;
	if (route_point_status == 1 || route_point_status == 5)
	{
		root["used_time"] = time_used;
		root["grade"] = grade;
	}

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

#ifdef  _DEBUG  
	std::cout << "Json:\n" << jsonStr << std::endl;
#endif
	return jsonStr;

}


//作业态势相关的接口
void ParseJsonForVehicleLocationRequest(std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector<PPoint>& guide_points, Ipp_Input& car_info, int& vihicle_id, int& route_point_id) {
	if (info.empty())
		return;

	bool res;
	JSONCPP_STRING errs;
	Json::Value root, _obstacles_info, _guide_points;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		std::cerr << "parseJson err. " << errs << std::endl;
	}
	
	//获取基本数据
	vihicle_id = root["vehicle_id"].asInt();
	route_point_id = root["route_point_id"].asInt();
	car_info.target_vehicle_type = root["vehicle_type"].asInt();
	car_info.start_direction = root["start_direction"].asDouble();
	car_info.end_direction = root["end_direction"].asDouble();
	car_info.speed = root["car_speed"].asDouble();


	//获取障碍物载具数据
	_obstacles_info = root["obstacles_info"];
	for (size_t i = 0; i < _obstacles_info.size(); ++i) {
		auto info = _obstacles_info[i];
		Car_Info car;
		if (info["vehicle_type"].asInt() == 2)
		{
			car.car_length = 9.6;
			car.car_width = 3.4;
		}
		else
		{
			car.car_length = 8.2;
			car.car_width = 3.4;
		}
		car.car_center.x = info["coordinates_x"].asDouble() ;
		car.car_center.y = info["coordinates_y"].asDouble() ;
		car.angle = info["direction"].asDouble();
		obstacles_info.push_back(car);
	}
	//获取导引点数组数据
	_guide_points = root["guide_points"];
	////先获得坞舱起点，坐标为起点x坐标-100
	//{
	//	auto info = _guide_points[0];
	//	PPoint p;
	//	p.x = info["coordinates_x"].asDouble() * px_to_m -100;
	//	p.y = info["coordinates_y"].asDouble() * px_to_m;
	//	guide_points.push_back(p);
	//}

	for (size_t i = 0; i < _guide_points.size(); ++i) {
		auto info = _guide_points[i];
		PPoint p;
		p.x = info["coordinates_x"].asDouble() ;
		p.y = info["coordinates_y"].asDouble() ;

		guide_points.push_back(p);
	}
}

std::string CreateJsonForVehicleLocationRequest(const std::vector<RoutePoint>& route_point, const int& route_point_status, const int& route_point_id) {
	std::string jsonStr;
	Json::Value root, points;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	if (route_point_status != 4) {
		root["route_point_id"] = route_point_id;
		root["route_point_status"] = route_point_status;
		root["coordinates_x"] = route_point[route_point_id - 1].coordinate.x;
		root["coordinates_y"] = route_point[route_point_id - 1].coordinate.y;
		root["direction"] = route_point[route_point_id - 1].angle_z;
	}
	else {
		root["route_point_id"] = route_point_id;
		root["route_point_status"] = route_point_status;
		root["coordinates_x"] = 0;
		root["coordinates_y"] = 0;
		root["direction"] = 0;
	}

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();

	return jsonStr;

}

void ParseJsonForHovercraftInitiativeRequest(std::string &info, double& start_x, double& end_x, int& vihicle_id, int& route_point_id) {
	if (info.empty())
		return;

	bool res;
	JSONCPP_STRING errs;
	Json::Value root, _obstacles_info, _guide_points;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		std::cerr << "parseJson err. " << errs << std::endl;
	}

	//获取基本数据
	vihicle_id = root["vehicle_id"].asInt();
	route_point_id = root["route_point_id"].asInt();
	start_x = root["start_x"].asDouble();
	end_x = root["end_x"].asDouble();
}

std::string CreateJsonForHovercraftInitiativeRequest(const std::vector<RoutePoint>& route_point, const int& route_point_status, const int& route_point_id) {
	std::string jsonStr;
	Json::Value root;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	if (route_point_status != 4) {
		root["route_point_id"] = route_point_id;
		root["route_point_status"] = route_point_status;
		root["coordinates_x"] = route_point[route_point_id - 1].coordinate.x;
	}
	else {
		root["route_point_id"] = route_point_id;
		root["route_point_status"] = route_point_status;
		root["coordinates_x"] = 0;
	}

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();
//#ifdef  _DEBUG  
//	std::cout << "Json:\n" << jsonStr << std::endl;
//#endif

	return jsonStr;
}

void ParseJsonForHovercraftPassivelyRequest(std::string &info, int& left_gear, int& right_gear, double& cur_x) {
	if (info.empty())
		return;

	bool res;
	JSONCPP_STRING errs;
	Json::Value root, _obstacles_info, _guide_points;
	Json::CharReaderBuilder readerBuilder;

	std::unique_ptr<Json::CharReader> const jsonReader(readerBuilder.newCharReader());
	res = jsonReader->parse(info.c_str(), info.c_str() + info.length(), &root, &errs);
	if (!res || !errs.empty()) {
		std::cerr << "parseJson err. " << errs << std::endl;
	}

	//获取基本数据
	left_gear = root["left_gear"].asInt();
	right_gear = root["right_gear"].asInt();
	cur_x = root["cur_x"].asDouble();
}
std::string CreateJsonForHovercraftPassivelyRequest(double cur_x, double angle) {
	std::string jsonStr;
	Json::Value root, points;
	Json::StreamWriterBuilder writerBuilder;
	std::ostringstream os;
	root["angle"] = angle;
	root["coordinates_x"] = cur_x;

	std::unique_ptr<Json::StreamWriter> jsonWriter(writerBuilder.newStreamWriter());
	jsonWriter->write(root, &os);
	jsonStr = os.str();
//#ifdef  _DEBUG  
//	std::cout << "Json:\n" << jsonStr << std::endl;
//#endif

	return jsonStr;
}

