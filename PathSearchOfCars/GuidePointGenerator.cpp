#include "pch.h"
#include "GuidePointGenerator.h"
#include "RoutePointGenerator.h"
#include "Calculate.h"
using namespace std;

GuidePointGenerator::GuidePointGenerator(std::vector<Car_Info>& obstacle_info, const double length, const double width) :car_length(length), car_width(width)
{
	//利用障碍物信息获取全部障碍物载具四个边界点的信息
	cars_boundary_points.reserve(obstacle_info.size() * 4);
	for ( auto& car_info : obstacle_info)
	{
		//1、获取障碍物左后方坐标以及障碍物车长、宽、倾斜角
		PPoint _center=car_info.car_center;
		vector<PPoint> points(4);
		//2、根据左后方获得中心点坐标
		//CalculateCarCenterPoint(car_info.car_length,car_info.car_width,car_info.back_left,car_info.angle,_center);	
		//3、根据中心点坐标获得边界点坐标并存储
		CalculateCarBoundaryPoint(car_info.car_length, car_info.car_width,_center, car_info.angle, points[0], points[1], points[2], points[3]);
		cars_boundary_points.push_back(points);
		
	}

}
vector<PPoint>& GuidePointGenerator::GetGuidePoints()
{
	return guide_points;
}
bool GuidePointGenerator::GetPathStatus()
{
	return is_available;
}
//************************************
// Method:    GenerateGuidePointsByPathSearch
// FullName:  GuidePointGenerator::GenerateGuidePointsByPathSearch
// Access:    public 
// Returns:   void
// Qualifier:根据搜索算法得到的路径获得带有坐标的导引点
// Parameter: Point start 起点
// Parameter: Point end 终点
// Parameter: list<Point * > & path 路径栅格点序列
//************************************
void GuidePointGenerator::GenerateGuidePointsByPathSearch(Point start, Point end, list<Point> &path)
{
	//输入的路径为空直接退出，直接退出并设置状态为不可用
	if (path.empty())
	{
		is_available = false;
		return;
	}

	//转折点序列
	vector<Point> nodesOfDirectionChange;
	nodesOfDirectionChange.reserve(20);
	//通过路径获取转折点序列
	GetTurningPointsByPath(start, end, path, nodesOfDirectionChange);

	//根据转折点序列生成各个导引点的x和y坐标
	guide_points.clear();
	guide_points.reserve(nodesOfDirectionChange.size());
	for (auto node : nodesOfDirectionChange)
	{
		auto val = PointToPPoint(node);
		guide_points.push_back(val);
	}
	//存储导引点的坐标
	PrintGuidePointsToCSV();
	cout << "原始导引点个数为:" << guide_points.size() << endl;
#ifdef  _DEBUG  
	//存储导引点的坐标
	PrintGuidePointsToCSV();
#endif
	//导引点筛选，删除不必要的导引点
	GuidePointsSieveOptimization();
	PrintGuidePointsToCSV("GuideSieveOptimization.csv");
	cout << "删减之后导引点个数为:" << guide_points.size() << endl;
#ifdef  _DEBUG  
	PrintGuidePointsToCSV("GuideSieveOptimization.csv");
#endif
	//导引点调整
	//BadGuidePointsAdjustmentOptimization();
	
#ifdef  _DEBUG  
	PrintGuidePointsToCSV("GuidesAdjustmentOptimization.csv");
#endif
	//碰撞导引点处理
	//BadGuidePointsDisposeOptimization();
	
}

//************************************
// Method:    GenerateGuidePointsByGuidePoints
// FullName:  GuidePointGenerator::GenerateGuidePointsByGuidePoints
// Access:    public 
// Returns:   void
// Qualifier:据外部导引点序列初始化导引点序列
// Parameter: std::vector<PPoint> outside_guide_points 外部导引点序列
//************************************
void GuidePointGenerator::GenerateGuidePointsByGuidePoints(std::vector<PPoint> outside_guide_points)
{
	guide_points = outside_guide_points;
	//调用路线生成路线
	RoutePointGenerator rpg(cars_boundary_points, car_length, car_width, 1.5);
	rpg.SetGuidePoints(guide_points, 0, 0);
	rpg.GenerateRoutePoints();
	//设置路径状态
	if (rpg.GetPathStatus()) 
		is_available=true;
	else
		is_available = false;
}
void GuidePointGenerator::PrintRectangleOfObstaclesToGSV()
{
	//将障碍物与此时载具的位置存储，并输出到文件中
	FILE* fd = NULL;
	if ((fd = fopen("rectangle_of_obstacles.csv", "wt+")) != NULL)
	{
		for (auto point : cars_boundary_points)
		{

			fprintf(fd, "%f,%f,%f,%f,%f\n", point[0].x, point[1].x, point[2].x, point[3].x, point[0].x);
			fprintf(fd, "%f,%f,%f,%f,%f\n", point[0].y, point[1].y, point[2].y, point[3].y, point[0].y);
		}
	}
	fclose(fd);
	fd = NULL;
}
//************************************
// Method:    GetTurningPointsByPath
// FullName:  GuidePointGenerator::GetTurningPointsByPath
// Access:    private 
// Returns:   void
// Qualifier:根据路径栅格点序列获得转向点
// Parameter: Point start 起点
// Parameter: Point end 终点
// Parameter: list<Point * > & path 路径栅格点序列
// Parameter: vector<Point> & nodesOfDirectionChange 转向点序列
//************************************
void GuidePointGenerator::GetTurningPointsByPath(Point start, Point end, list<Point> &path, vector<Point> &nodesOfDirectionChange)
{
	//方向左上方为0，按行列顺序增加：0 1 2 3 4 5 6 7 表示 左上 上 右上  左 右 左下 下 右下
	//记录8个方向的行进栅格数
	vector<int> direction_counts(8, 0);
	Point last(0, 0);
	//转弯前行进距离是否满足要求的标志 true-满足 false-不满足
	bool preFlag = false;
	for (auto &p : path)
	{
		if (p.x == start.x && p.y == start.y)//如果是起始点,将起点保存
		{
			//保存起点			
			direction_counts[4]++;
			nodesOfDirectionChange.push_back(p);
			preFlag = true;
		}
		else if (p.x == end.x && p.y == end.y)//如果是终点,将终点保存
		{
			//保存终点
			nodesOfDirectionChange.push_back(p);
			break;
		}
		else//如果是中间栅格
		{
			//判断当前栅格是上一栅格的那个方向
			//方向左上方为0，按行列顺序增加：0 1 2 3 4 5 6 7 表示 左上 上 右上  左 右 左下 下 右下
			int direction_type = 0;
			for (int x = last.x - 1; x <= last.x + 1; x++)
				for (int y = last.y - 1; y <= last.y + 1; y++)
				{
					//相等的情况跳过
					if (x == last.x && y == last.y)
						continue;
					if (x == p.x && y == p.y)
					{
						x = last.x + 1;
						break;
					}
					else
						direction_type++;
				}
			//获取方向后，根据方向来进行判断
			//比较之前的方向,若发生转向
#ifdef  _DEBUG  

			if (direction_type >= 8 || direction_type < 0)
				cout << "direction_type" << endl;
#endif	
			if (direction_counts[direction_type] == 0)
			{
				//判断转向点的机制：满足两者之一，（1）在转向点之后行进一段距离；（2）在转向点之前行进一段距离
				//0、判断上一个转弯点是否满足（2）
				int max_len = *max_element(direction_counts.begin(), direction_counts.end());
				if (preFlag==false && max_len * 0.2 < car_length / 5 )
				{
					//上一个转弯点不满足（1）（2）任意一个，则删除它
					nodesOfDirectionChange.pop_back();
				}

				// 1、只要转向，就存储上一个栅格（当前转弯点）
				nodesOfDirectionChange.push_back(last);
				//2、判断（2）是否成立，设置prefalg标志
				if (max_len * 0.2 >= car_length / 5)
					preFlag = true;
				else
					preFlag = false;

				//其它方向计数置为0
				for (auto &direction : direction_counts)
					direction = 0;

			}
#ifdef  _DEBUG  

			if (direction_type >= 8 || direction_type < 0)
				cout << "direction_type" << endl;
#endif	
			direction_counts[direction_type]++;
			
		}
		//更新上一栅格
		last.x = p.x;
		last.y = p.y;
	}
}


//************************************
// Method:    PrintGuidePointsToCSV
// FullName:  GuidePointGenerator::PrintGuidePointsToCSV
// Access:    private 
// Returns:   void
// Qualifier: const 将导引点存储到csv文件中
// Parameter: const string filename 文件名
//************************************
void GuidePointGenerator::PrintGuidePointsToCSV(const string filename) const
{
	if (filename.size() == 0) return;
	//将转折点转换，并输出到文件中
	FILE* fd = NULL;
	if ((fd = fopen(filename.c_str(), "wt+")) != NULL)
	{
		for (const auto &node : guide_points)
		{
			fprintf(fd, "%f,%f\n", node.x, node.y);
		}
	}
	fclose(fd);
	fd = NULL;
}
//************************************
// Method:    GuidePointsOptimization
// FullName:  GuidePointGenerator::GuidePointsOptimization
// Access:    private 
// Returns:   void
// Qualifier:对导引点序列进行优化，删除不必要的导引点
//************************************
void GuidePointGenerator::GuidePointsSieveOptimization()
{
	//没有三个导引点，直接返回
	if (guide_points.size() < 3) return;
	bool if_break = true;
	while (true) {
		if(GuidePointsSieve() == false)
			break;
	}
	////没有三个导引点，直接返回
	//if (guide_points.size() < 3) return;
	////遍历导引点序列，依次找出相邻的三个点
	//for (auto ite1 = guide_points.begin(); ite1 != guide_points.end(); )
	//{
	//	//若已经遍历到倒数第2个或者1个，直接返回	
	//	if ((ite1 + 1) == guide_points.end() || (ite1 + 2) == guide_points.end())
	//		return;

	//	//如果第一个点到第三个点的路径没有障碍，则删除第二个点，否则继续
	//	auto ite2 = ite1 + 1,ite3=ite1+2;
	//	if (CollisionCheckForGuidePointsOptimization(*ite1, *ite3)==false)
	//		//删除点的同时第一个点不动
	//		guide_points.erase(ite2);
	//	else
	//		++ite1;	
	//}
}

bool GuidePointGenerator::GuidePointsSieve() {
	std::vector<PPoint> temp_guide_points;
	for (size_t i = 0; i < guide_points.size(); ++i) {
		for (size_t j = guide_points.size() - 1; j >= 0; --j) {
			if (CollisionCheckForGuidePointsOptimization(guide_points[i], guide_points[j]) == false) {
				for (int n = 0; n <= i; ++n) {
					temp_guide_points.push_back(guide_points[n]);
				}
				for (int n = j; n <= guide_points.size() - 1; ++n) {
					temp_guide_points.push_back(guide_points[n]);
				}	
				//如果没什么可以删除的点，就直接退出
				if (temp_guide_points.size() >= guide_points.size()) {
					return false;
				}
				guide_points = temp_guide_points;
				return true;
			}
		}
	}
	return false;
}
//************************************
// Method:    CollisionCheckForGuidePointsOptimization
// FullName:  GuidePointGenerator::CollisionCheckForGuidePointsOptimization
// Access:    private 
// Returns:   bool 是-返回true，否-返回false
// Qualifier:检测载具在两个导引点之间运动是否会触碰障碍物
// Parameter: PPoint guide_point1 
// Parameter: PPoint guide_point2
//************************************
bool GuidePointGenerator::CollisionCheckForGuidePointsOptimization(PPoint guide_point1, PPoint guide_point2) const
{
	//1、根据2个导引点坐标求它们之间与x轴正方向的夹角,非弧度,角度范围为-180~180
	double angle = atan2(guide_point2.y-guide_point1.y,guide_point2.x-guide_point1.x)/3.1415926*180;
	//角度转换 转换为[0,360)
	if (angle < 0)
		angle += 360;
	//2、根据导引点1、2分别获取四个边界点
	vector<PPoint> bp(8);
	CalculateCarBoundaryPoint(0, car_width, guide_point1, angle, bp[0], bp[1], bp[2], bp[3]);
	CalculateCarBoundaryPoint(0, car_width, guide_point2, angle, bp[4], bp[5], bp[6], bp[7]);
	//3、得到一个大的矩形
	vector<PPoint> bigRe(4);
	bigRe[0] = bp[4];
	bigRe[1] = bp[1];
	bigRe[2] = bp[2];
	bigRe[3] = bp[7];
	//4、判断大矩形是否与其它障碍物碰撞
	for (const auto& car_info : cars_boundary_points)
	{
		if(CalculateCollisionOfTwoCars(car_info,bigRe))
			return true;
	}
	return false;
}
//************************************
// Method:    GuidePointsAdjustmentOptimization
// FullName:  GuidePointGenerator::GuidePointsAdjustmentOptimization
// Access:    private 
// Returns:   void
// Qualifier:对所有碰撞导引点序列进行优化，导引点位置调整函数
//************************************
void GuidePointGenerator::BadGuidePointsAdjustmentOptimization()
{
	//获取在产生路线点时发生碰撞的对应导引点
	//调用路线生成器对象
	RoutePointGenerator rpg(cars_boundary_points, car_length, car_width, 1.5);
	rpg.SetGuidePoints(guide_points, 0, 0);
	rpg.GenerateRoutePoints();
	//如果没有发生碰撞，退出
	if (rpg.GetPathStatus()) return;
	//获得发生碰撞的导引点序列
	std::vector<PPoint> bad_guide_points = rpg.GetBadTurningGuidePoints();
	
	auto tmp_len= car_length / 2;
	std::vector<PPoint> all_boundary_points;
	all_boundary_points.reserve(cars_boundary_points.size() * 4);
	for (auto & car : cars_boundary_points)
	{
		for (auto p : car)
			all_boundary_points.emplace_back(p);
	}
	//对每一个转弯碰撞导引点
	for(auto bad_point : bad_guide_points)
	{
		auto ite_point = find(guide_points.begin(), guide_points.end(), bad_point);
		auto& point = (*ite_point);
		//提取所有距离小于车长的一半的载具边界点
		vector<PPoint> nearby_boundary_points;
		nearby_boundary_points.reserve(4);
		auto comp = [&point, tmp_len](PPoint& p)->bool {
			return LengthOfTwoPoints(point, p) < tmp_len;
		};
		copy_if(all_boundary_points.begin(), all_boundary_points.end(), back_inserter(nearby_boundary_points), comp);
		if (!nearby_boundary_points.empty())
		{
			//移动导引点的矢量
			PPoint vector_of_change(0,0);
			for (auto& boundary_point : nearby_boundary_points)
			{
				auto len = tmp_len - LengthOfTwoPoints(boundary_point, point);
				//两个点构成的矢量
				PPoint vector_of_points(point.x - boundary_point.x, point.y - boundary_point.y);
				//换算为单位矢量
				vector_of_points.x = vector_of_points.x / sqrt(pow(vector_of_points.x, 2) + pow(vector_of_points.y, 2));
				vector_of_points.y = vector_of_points.y / sqrt(pow(vector_of_points.x, 2) + pow(vector_of_points.y, 2));
				//改变移动导引点的矢量
				vector_of_change.x += len * vector_of_points.x;
				vector_of_change.y += len * vector_of_points.y;		
			}
			//根据矢量调整导引点坐标
			point.x += vector_of_change.x;
			point.y += vector_of_change.y;
		}
		
	}
}
//************************************
// Method:    BadGuidePointsDisposeOptimization
// FullName:  GuidePointGenerator::BadGuidePointsDisposeOptimization
// Access:    private 
// Returns:   void
// Qualifier:对导引点序列进行优化，处理碰撞导引点
//************************************
void GuidePointGenerator::BadGuidePointsDisposeOptimization()
{
	//1、获取在产生路线点时发生碰撞的对应导引点
	//调用路线生成对象
	RoutePointGenerator rpg(cars_boundary_points, car_length, car_width, 1.5);
	rpg.SetGuidePoints(guide_points,0, 0);
	rpg.GenerateRoutePoints();
	//如果没有发生碰撞，退出
	if (rpg.GetPathStatus()) return;
	//获得发生碰撞的导引点序列
	std::vector<PPoint> bad_guide_points=rpg.GetBadGuidePoints();

	//2、对每一个会碰撞导引点，进行调整，调整的原则是使碰撞导引点尽可能远离障碍物
	//2.1 2.2 按照两个不同的方向调整当前点；
	//2.3 在当前点无法调整的情况下，增加新的过渡点；
	//若无法调整，则将is_available置false表示当前导引点序列不可用
	//若可以调整，则用新点代替当前点，即从guide_points中替换该点并从bad_guide_points删除该点
	for (auto cur : bad_guide_points)
	{
		//找到当前点的前后点
		auto ite=find(guide_points.begin(), guide_points.end(), cur);
		if(ite==guide_points.end() || (ite+1)==guide_points.end() || ite == guide_points.begin()) continue;
		PPoint pre = *(ite - 1);
		PPoint next = *(ite + 1);
		//当前碰撞导引点可以调整的位置集合
		//记录下从当前点到前一点收缩调整的备选点
		std::vector<PPoint> shrink_pre_adjust_points;
		//记录下从后一点到当前点伸长调整的备选点
		std::vector<PPoint> extend_next_reverse_adjust_points;
		PPoint new_point;
		new_point=FindNewPointToReplaceCurPoint(pre, cur, next, cars_boundary_points, car_length, car_width, shrink_pre_adjust_points,extend_next_reverse_adjust_points);

		//如果找到合适的点，替换，并从bad_guide_points删除该点删除对应的点,继续下一个点
		if (new_point.x!=0 || new_point.y!=0)
		{
			replace(guide_points.begin(), guide_points.end(), cur, new_point);
			//当前点置为0 0
			cur.x = 0; cur.y = 0;
			continue;
		}
		
		//2.3 增加过渡点
		PPoint add_point;
		add_point = AddNewPointToTransition(pre, cur, next, cars_boundary_points, car_length, car_width, shrink_pre_adjust_points, extend_next_reverse_adjust_points);
		//如果找到合适的过渡点，在guide_points的cur之前插入过渡点并从bad_guide_points删除该点删除对应的点,继续下一个点
		if (new_point.x != 0 || new_point.y != 0)
		{
			guide_points.insert(ite, add_point);
			//当前点置为0 0
			cur.x = 0; cur.y = 0;
			continue;
		}
			
	}
	//3、如果还有碰撞导引点无法调整，设置当前导引点序列状态为不可用
	if (any_of(bad_guide_points.begin(), bad_guide_points.end(), [](const PPoint& p) {return !((abs(p.x) < 0.0001 && abs(p.y) < 0.0001));}))
		is_available = false;
	else
		is_available = true;

}


//************************************
// Method:    FindNewPointToReplaceCurPoint
// FullName:  GuidePointGenerator::FindNewPointToReplaceCurPoint
// Access:    private 
// Returns:   PPoint	用来替换的点  没找到替代点返回(0,0)
// Qualifier:找到可以替换cur的点
// Parameter: PPoint pre	前一点
// Parameter: PPoint cur	当前点
// Parameter: PPoint next	后一点
// Parameter: std::vector<std::vector<PPoint>> & cars_boundary_points	障碍物边界点
// Parameter: double car_length	目标车车长
// Parameter: double car_width	目标车车宽
// Parameter: std::vector<PPoint> & shrink_pre_adjust_points	记录下从当前点到前一点收缩调整的备选点
// Parameter: std::vector<PPoint> & extend_next_reverse_adjust_points	记录下从后一点到当前点伸长调整的备选点
//************************************
PPoint GuidePointGenerator::FindNewPointToReplaceCurPoint(PPoint pre, PPoint cur, PPoint next, std::vector<std::vector<PPoint>>& cars_boundary_points, double car_length, double car_width, std::vector<PPoint>& shrink_pre_adjust_points, std::vector<PPoint>& extend_next_reverse_adjust_points)
{
	//存储搜索的新点
	PPoint new_point;
	//2.1 沿前导引点进行收缩调整，在当前导引点与上一导引点的线段上，以朝向上一导引点的方向上找一个新的可用导引点
	//二分搜索
	//取两个边界，上界与下界
	PPoint pre_tem = pre;
	PPoint cur_tem = cur;
	PPoint mid((pre_tem.x + cur_tem.x) / 2, (pre_tem.y + cur_tem.y) / 2);
	
	bool is_find = false;
	//若两个边界之间距离大于 2.0，则不断搜索新的中点
	while (sqrt(abs((pre_tem.x - cur_tem.x)*(pre_tem.x - cur_tem.x) + (pre_tem.y - cur_tem.y)*(pre_tem.y - cur_tem.y))) > 2)
	{
		std::vector<PPoint> tem = { pre,mid,next };
		RoutePointGenerator rpg_tem(cars_boundary_points, car_length, car_width, 1.5);
		rpg_tem.SetGuidePoints(tem, 0, 0);
		rpg_tem.GenerateRoutePoints();
#ifdef  _DEBUG  
		rpg_tem.PrintRectangleOfObstaclesToGSV();
		rpg_tem.PrintRoutePointsToGSV();
		rpg_tem.PrintRectangleOfCollisionToGSV();
#endif
		
		//如果没有发生碰撞，
		if (rpg_tem.GetPathStatus())
		{
			//设置状态为，存储当前点
			is_find = true;
			new_point = mid;
			//调整上界
			pre_tem = mid;
		}
		else
		{
			//调整下界
			cur_tem = mid;
			//存储可以调整的位置
			shrink_pre_adjust_points.push_back(mid);
		}
		mid.x = (cur_tem.x + pre_tem.x) / 2;
		mid.y = (cur_tem.y + pre_tem.y) / 2;

	}

	//如果找到合适的点，返回该点
	if (is_find)
		return new_point;
	//存储最后一个mid
	shrink_pre_adjust_points.push_back(mid);
	//存储pre
	shrink_pre_adjust_points.push_back(pre);
	//2.2 沿下个导引点反向伸长调整，在当前导引点与一下一导引点的线段上，以朝向当前导引点的方向上找一个新的可用导引点
	//取两个边界，上界与下界
	pre_tem = next;
	cur_tem = cur;
	mid = PPoint((pre_tem.x + cur_tem.x) / 2, (pre_tem.y + cur_tem.y) / 2);
	//求反向点
	PPoint reserve_mid;
	reserve_mid.x = (mid.x - cur.x)*cos(3.1415926) - (mid.y - cur.y)*sin(3.1415926) + cur.x;
	reserve_mid.y = (mid.x - cur.x)*sin(3.1415926) + (mid.y - cur.y)*cos(3.1415926) + cur.y;
	//若两个边界之间距离大于 2.0，则不断搜索新的中点
	while (sqrt(abs((pre_tem.x - cur_tem.x)*(pre_tem.x - cur_tem.x) + (pre_tem.y - cur_tem.y)*(pre_tem.y - cur_tem.y))) > 2)
	{
		std::vector<PPoint> tem = { pre,reserve_mid,next };
		RoutePointGenerator rpg_tem(cars_boundary_points, car_length, car_width,1.5);
		rpg_tem.SetGuidePoints(tem, 0, 0);
		rpg_tem.GenerateRoutePoints();
#ifdef  _DEBUG  
		rpg_tem.PrintRectangleOfObstaclesToGSV();
		rpg_tem.PrintRoutePointsToGSV();
		rpg_tem.PrintRectangleOfCollisionToGSV();
#endif
		//如果没有发生碰撞，
		if (rpg_tem.GetPathStatus())
		{
			//设置状态为，存储当前点
			is_find = true;
			new_point = reserve_mid;
			//调整上界
			pre_tem = mid;
		}
		else
		{
			//调整下界
			cur_tem = mid;
			//存储可以调整的位置
			extend_next_reverse_adjust_points.push_back(reserve_mid);
		}
		mid.x = (cur_tem.x + pre_tem.x) / 2;
		mid.y = (cur_tem.y + pre_tem.y) / 2;
		reserve_mid.x = (mid.x - cur.x)*cos(3.1415926) - (mid.y - cur.y)*sin(3.1415926) + cur.x;
		reserve_mid.y = (mid.x - cur.x)*sin(3.1415926) + (mid.y - cur.y)*cos(3.1415926) + cur.y;

	}
	//如果找到合适的点，返回该点
	if (is_find)
		return new_point;

	//存储最后一个reserve_mid
	extend_next_reverse_adjust_points.push_back(reserve_mid);
	//存储最后一个reserve_next
	mid = next;
	reserve_mid.x = (mid.x - cur.x)*cos(3.1415926) - (mid.y - cur.y)*sin(3.1415926) + cur.x;
	reserve_mid.y = (mid.x - cur.x)*sin(3.1415926) + (mid.y - cur.y)*cos(3.1415926) + cur.y;
	extend_next_reverse_adjust_points.push_back(reserve_mid);
	//没找到替代点返回(0,0)
	return PPoint(0, 0);
}

//************************************
// Method:    FindNewPointsToReplacePre_CurPoint
// FullName:  GuidePointGenerator::FindNewPointsToReplacePre_CurPoint
// Access:    private 
// Returns:   std::pair<PPoint, PPoint>
// Qualifier:
// Parameter: PPoint pre	前一点
// Parameter: PPoint cur	当前点
// Parameter: PPoint next	后一点
// Parameter: PPoint next_next	后后点
// Parameter: std::vector<PPoint> & next_points	下一点的集合
// Parameter: std::vector<std::vector<PPoint>> & cars_boundary_points	障碍物边界点
// Parameter: double car_length	目标车车长
// Parameter: double car_width	目标车车宽
//************************************
//std::pair<PPoint, PPoint> GuidePointGenerator::FindNewPointsToReplacePre_CurPoint(PPoint pre, PPoint cur, PPoint next, PPoint next_next, std::vector<PPoint>& next_points, std::vector<std::vector<PPoint>>& cars_boundary_points, double car_length, double car_width)
//{
//	//存储搜索的当前点和下一点
//	PPoint new_point,next_point;
//	//2.1 沿前导引点进行收缩调整，在当前导引点与上一导引点的线段上，以朝向上一导引点的方向上找一个新的可用导引点
//	//二分搜索
//	//取两个边界，上界与下界
//	PPoint pre_tem = pre;
//	PPoint cur_tem = cur;
//	PPoint mid((pre_tem.x + cur_tem.x) / 2, (pre_tem.y + cur_tem.y) / 2);
//
//	//若两个边界之间距离大于 2.0，则不断搜索新的中点
//	while (sqrt(abs((pre_tem.x - cur_tem.x)*(pre_tem.x - cur_tem.x) + (pre_tem.y - cur_tem.y)*(pre_tem.y - cur_tem.y))) > 2)
//	{
//		for (auto next : next_points)
//		{
//			std::vector<PPoint> tem = { pre,mid,next ,next_next};
//			RoutePointGenerator rpg_tem(cars_boundary_points, car_length, car_width);
//			rpg_tem.SetGuidePoints(tem);
//			rpg_tem.GenerateRouteCurveEquationByGuidePoints();
//			rpg_tem.GenerateRoutePointsByRouteCurveEquation();
//			rpg_tem.PrintRectangleOfObstaclesToGSV();
//			rpg_tem.PrintRoutePointsToGSV();
//			rpg_tem.PrintRectangleOfCollisionToGSV();
//			//如果没有发生碰撞，
//			if (rpg_tem.GetPathStatus())
//			{
//				//如果找到合适的两个点，返回
//				return make_pair(mid, next);				
//			}
//
//		}
//		//调整下界
//		cur_tem = mid;
//		mid.x = (cur_tem.x + pre_tem.x) / 2;
//		mid.y = (cur_tem.y + pre_tem.y) / 2;
//
//	}
//	//2.2 沿下个导引点反向伸长调整，在当前导引点与一下一导引点的线段上，以朝向当前导引点的方向上找一个新的可用导引点
//	//取两个边界，上界与下界
//	pre_tem = next;
//	cur_tem = cur;
//	mid = PPoint((pre_tem.x + cur_tem.x) / 2, (pre_tem.y + cur_tem.y) / 2);
//	//求反向点
//	PPoint reserve_mid;
//	reserve_mid.x = (mid.x - cur.x)*cos(3.1415926) - (mid.y - cur.y)*sin(3.1415926) + cur.x;
//	reserve_mid.y = (mid.x - cur.x)*sin(3.1415926) + (mid.y - cur.y)*cos(3.1415926) + cur.y;
//	//若两个边界之间距离大于 2.0，则不断搜索新的中点
//	while (sqrt(abs((pre_tem.x - cur_tem.x)*(pre_tem.x - cur_tem.x) + (pre_tem.y - cur_tem.y)*(pre_tem.y - cur_tem.y))) > 2)
//	{
//		for (auto next_p : next_points)
//		{
//			std::vector<PPoint> tem = { pre,reserve_mid,next_p,next_next };
//			RoutePointGenerator rpg_tem(cars_boundary_points, car_length, car_width);
//			rpg_tem.SetGuidePoints(tem);
//			rpg_tem.GenerateRouteCurveEquationByGuidePoints();
//			rpg_tem.GenerateRoutePointsByRouteCurveEquation();
//			rpg_tem.PrintRectangleOfObstaclesToGSV();
//			rpg_tem.PrintRoutePointsToGSV();
//			rpg_tem.PrintRectangleOfCollisionToGSV();
//			//如果没有发生碰撞，
//			if (rpg_tem.GetPathStatus())
//			{
//				//如果找到合适的两个点，返回
//				return make_pair(reserve_mid, next_p);
//			}
//
//		}
//		//调整下界
//		cur_tem = mid;
//		mid.x = (cur_tem.x + pre_tem.x) / 2;
//		mid.y = (cur_tem.y + pre_tem.y) / 2;
//		reserve_mid.x = (mid.x - cur.x)*cos(3.1415926) - (mid.y - cur.y)*sin(3.1415926) + cur.x;
//		reserve_mid.y = (mid.x - cur.x)*sin(3.1415926) + (mid.y - cur.y)*cos(3.1415926) + cur.y;
//
//	}
//	//没找到替代点返回(0,0),(0,0)
//	return make_pair(PPoint(0,0), PPoint(0, 0));
//}
PPoint GuidePointGenerator::AddNewPointToTransition(PPoint pre, PPoint cur, PPoint next, std::vector<std::vector<PPoint>>& cars_boundary_points, double car_length, double car_width, std::vector<PPoint>& shrink_pre_adjust_points, std::vector<PPoint>& extend_next_reverse_adjust_points)
{
	//增加的点
	PPoint mid;
	for(auto& p1:shrink_pre_adjust_points)
		for (auto& p2 : extend_next_reverse_adjust_points)
		{
			//取中点
			mid.x = (p1.x + p2.x) / 2;
			mid.y = (p1.y + p2.y) / 2;
			//判断增加点后是否会碰撞
			std::vector<PPoint> tem = { pre,mid,cur,next };
			RoutePointGenerator rpg_tem(cars_boundary_points, car_length, car_width,1.5);
			if (LengthOfTwoPoints(mid, cur) > 2 && LengthOfTwoPoints(mid, pre) > 2)
			{
				rpg_tem.SetGuidePoints(tem ,0, 0);
				rpg_tem.GenerateRoutePoints();
#ifdef  _DEBUG  
				rpg_tem.PrintRectangleOfObstaclesToGSV();
				rpg_tem.PrintRoutePointsToGSV();
				rpg_tem.PrintRectangleOfCollisionToGSV();
#endif
				//如果没有发生碰撞，
				if (rpg_tem.GetPathStatus())
				{
					//如果找到合适的点，返回
					return mid;
				}
			}

			//取
			PPoint cmid=mid;
			mid.x = (p1.x + cmid.x) / 2;
			mid.y = (p1.y + cmid.y) / 2;
			//判断增加点后是否会碰撞
			if (LengthOfTwoPoints(mid, cur) > 2 && LengthOfTwoPoints(mid, pre) > 2)
			{
				tem = { pre,mid,cur,next };
				rpg_tem.SetGuidePoints(tem, 0, 0);
				rpg_tem.GenerateRoutePoints();
#ifdef  _DEBUG  
				rpg_tem.PrintRectangleOfObstaclesToGSV();
				rpg_tem.PrintRoutePointsToGSV();
				rpg_tem.PrintRectangleOfCollisionToGSV();
#endif
				//如果没有发生碰撞，
				if (rpg_tem.GetPathStatus())
				{
					//如果找到合适的两个点，返回
					return mid;
				}
			}

			mid.x = (p1.x + mid.x) / 2;
			mid.y = (p1.y + mid.y) / 2;
			//判断增加点后是否会碰撞
			if (LengthOfTwoPoints(mid, cur) > 2 && LengthOfTwoPoints(mid, pre) > 2)
			{
				tem = { pre,mid,cur,next };
				rpg_tem.SetGuidePoints(tem, 0, 0);
				rpg_tem.GenerateRoutePoints();
#ifdef  _DEBUG  
				rpg_tem.PrintRectangleOfObstaclesToGSV();
				rpg_tem.PrintRoutePointsToGSV();
				rpg_tem.PrintRectangleOfCollisionToGSV();
#endif
				//如果没有发生碰撞，
				if (rpg_tem.GetPathStatus())
				{
					//如果找到合适的两个点，返回
					return mid;
				}
			}

			
			//取
			mid.x = (cmid.x + p2.x)  / 2;
			mid.y = (cmid.y + p2.y)  / 2;
			if (LengthOfTwoPoints(mid, cur) > 2 && LengthOfTwoPoints(mid, pre) > 2)
			{
				//判断增加点后是否会碰撞
				tem = { pre,mid,cur,next };
				rpg_tem.SetGuidePoints(tem, 0, 0);
				rpg_tem.GenerateRoutePoints();
#ifdef  _DEBUG  
				rpg_tem.PrintRectangleOfObstaclesToGSV();
				rpg_tem.PrintRoutePointsToGSV();
				rpg_tem.PrintRectangleOfCollisionToGSV();
#endif
				//如果没有发生碰撞，
				if (rpg_tem.GetPathStatus())
				{
					//如果找到合适的两个点，返回
					return mid;
				}

			}
			
			mid.x = (mid.x + p2.x) / 2;
			mid.y = (mid.y + p2.y) / 2;
			if (LengthOfTwoPoints(mid, cur) > 2 && LengthOfTwoPoints(mid, pre) > 2)
			{
				//判断增加点后是否会碰撞
				tem = { pre,mid,cur,next };
				rpg_tem.SetGuidePoints(tem, 0, 0);
				rpg_tem.GenerateRoutePoints();
#ifdef  _DEBUG  
				rpg_tem.PrintRectangleOfObstaclesToGSV();
				rpg_tem.PrintRoutePointsToGSV();
				rpg_tem.PrintRectangleOfCollisionToGSV();
#endif
				//如果没有发生碰撞，
				if (rpg_tem.GetPathStatus())
				{
					//如果找到合适的两个点，返回
					return mid;
				}

			}


		}
	return PPoint(0, 0);
}