#include "pch.h"
#include "RoutePointGenerator.h"
#include "Calculate.h"
using namespace std;

const double R = 2;                 //圆的半径
const double epsilon = 0.0001;
const double pi = 3.1415;


//辅助函数，判断点在线段上
int Judgepointonline(PPoint A, PPoint B, PPoint C)
{
	PPoint q = C;
	PPoint p1 = A;
	PPoint p2 = B;
	double a, b, c;
	a = sqrt((A.x - C.x) * (A.x - C.x) + (A.y - C.y) * (A.y - C.y));
	b = sqrt((B.x - C.x) * (B.x - C.x) + (B.y - C.y) * (B.y - C.y));
	c = sqrt((A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y));
	if (abs(a + b - c) < 0.01)
		return 1;
	else return 0;
	/*if (((q.x - p1.x)*(p1.y - p2.y)) == ((p1.x - p2.x)*(q.y - p1.y))
		&& (q.x >= min(p1.x, p2.x) && q.x <= max(p1.x, p2.x))
		&& ((q.y >= min(p1.y, p2.y)) && (q.y <= max(p1.y, p2.y))))
		return 1;
	return 0;*/

}
//判断三个点能否进行平滑
bool RoutePointGenerator::Judge3pointsmooth(PPoint A, PPoint B, PPoint C)
{
	PPoint M1, M2;                           //两个切点
	PPoint C1, C2, C3, C4, Z;                //四个可能的圆心C1,C2,C3,C4及真正的圆心Z
//先求圆心
	double A1, B1, A2, B2, C11, C12, C21, C22;
	A1 = -(B.y - A.y) / (B.x - A.x + epsilon);
	B1 = 1;
	C11 = R * sqrt(A1 * A1 + B1 * B1) + B.y - B.x * (B.y - A.y) / (B.x - A.x + epsilon);
	C12 = -R * sqrt(A1 * A1 + B1 * B1) + B.y - B.x * (B.y - A.y) / (B.x - A.x + epsilon);
	A2 = -(C.y - B.y) / (C.x - B.x + epsilon);
	B2 = 1;
	C21 = R * sqrt(A2 * A2 + B2 * B2) + B.y - B.x * (C.y - B.y) / (C.x - B.x + epsilon);
	C22 = -R * sqrt(A2 * A2 + B2 * B2) + B.y - B.x * (C.y - B.y) / (C.x - B.x + epsilon);
	//圆心坐标的四种可能
	C1.x = (C11 * B2 - C21 * B1) / (A1 * B2 - A2 * B1);
	C1.y = (A1 * C21 - A2 * C11) / (A1 * B2 - A2 * B1);
	C2.x = (C11 * B2 - C22 * B1) / (A1 * B2 - A2 * B1);
	C2.y = (A1 * C22 - A2 * C11) / (A1 * B2 - A2 * B1);
	C3.x = (C12 * B2 - C21 * B1) / (A1 * B2 - A2 * B1);
	C3.y = (A1 * C21 - A2 * C12) / (A1 * B2 - A2 * B1);
	C4.x = (C12 * B2 - C22 * B1) / (A1 * B2 - A2 * B1);
	C4.y = (A1 * C22 - A2 * C12) / (A1 * B2 - A2 * B1);
	//通过判断角度是否在两条边之间来判断哪个圆心符合条件
	if (((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) < pi) && (atan3((A.y - B.y), (A.x - B.x)) > atan3((C1.y - B.y), (C1.x - B.x))) && (atan3((C1.y - B.y), (C1.x - B.x)) > (atan3((C.y - B.y), (C.x - B.x))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) < pi) && (atan3((C.y - B.y), (C.x - B.x)) > atan3((C1.y - B.y), (C1.x - B.x))) && (atan3((C1.y - B.y), (C1.x - B.x)) > (atan3((A.y - B.y), (A.x - B.x))))) || ((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) > pi) && ((atan3((C1.y - B.y), (C1.x - B.x)) > atan3((A.y - B.y), (A.x - B.x))) || (atan3((C1.y - B.y), (C1.x - B.x)) < (atan3((C.y - B.y), (C.x - B.x)))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) > pi) && ((atan3((C1.y - B.y), (C1.x - B.x)) > atan3((C.y - B.y), (C.x - B.x))) || (atan3((C1.y - B.y), (C1.x - B.x)) < (atan3((A.y - B.y), (A.x - B.x)))))))
	{
		Z.x = C1.x;
		Z.y = C1.y;
	}
	else if (((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) < pi) && (atan3((A.y - B.y), (A.x - B.x)) > atan3((C2.y - B.y), (C2.x - B.x))) && (atan3((C2.y - B.y), (C2.x - B.x)) > (atan3((C.y - B.y), (C.x - B.x))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) < pi) && (atan3((C.y - B.y), (C.x - B.x)) > atan3((C2.y - B.y), (C2.x - B.x))) && (atan3((C2.y - B.y), (C2.x - B.x)) > (atan3((A.y - B.y), (A.x - B.x))))) || ((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) > pi) && ((atan3((C2.y - B.y), (C2.x - B.x)) > atan3((A.y - B.y), (A.x - B.x))) || (atan3((C2.y - B.y), (C2.x - B.x)) < (atan3((C.y - B.y), (C.x - B.x)))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) > pi) && ((atan3((C2.y - B.y), (C2.x - B.x)) > atan3((C.y - B.y), (C.x - B.x))) || (atan3((C2.y - B.y), (C2.x - B.x)) < (atan3((A.y - B.y), (A.x - B.x)))))))
	{
		Z.x = C2.x;
		Z.y = C2.y;
	}
	else if (((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) < pi) && (atan3((A.y - B.y), (A.x - B.x)) > atan3((C3.y - B.y), (C3.x - B.x))) && (atan3((C3.y - B.y), (C3.x - B.x)) > (atan3((C.y - B.y), (C.x - B.x))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) < pi) && (atan3((C.y - B.y), (C.x - B.x)) > atan3((C3.y - B.y), (C3.x - B.x))) && (atan3((C3.y - B.y), (C3.x - B.x)) > (atan3((A.y - B.y), (A.x - B.x))))) || ((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) > pi) && ((atan3((C3.y - B.y), (C3.x - B.x)) > atan3((A.y - B.y), (A.x - B.x))) || (atan3((C3.y - B.y), (C3.x - B.x)) < (atan3((C.y - B.y), (C.x - B.x)))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) > pi) && ((atan3((C3.y - B.y), (C3.x - B.x)) > atan3((C.y - B.y), (C.x - B.x))) || (atan3((C3.y - B.y), (C3.x - B.x)) < (atan3((A.y - B.y), (A.x - B.x)))))))
	{
		Z.x = C3.x;
		Z.y = C3.y;
	}
	else
	{
		Z.x = C4.x;
		Z.y = C4.y;
	}
	//已知圆心求圆弧起止点坐标
	double k1, b1, k2, b2;
	k1 = (B.y - A.y) / (B.x - A.x + epsilon);
	b1 = B.y - B.x * (B.y - A.y) / (B.x - A.x + epsilon);
	k2 = (C.y - B.y) / (C.x - B.x + epsilon);
	b2 = B.y - B.x * (C.y - B.y) / (C.x - B.x + epsilon);
	M1.x = (Z.x + k1 * Z.y - b1 * k1) / (k1 * k1 + 1);
	M1.y = (k1 * Z.x + Z.y * k1 * k1 + b1) / (k1 * k1 + 1);
	M2.x = (Z.x + k2 * Z.y - b2 * k2) / (k2 * k2 + 1);
	M2.y = (k2 * Z.x + Z.y * k2 * k2 + b2) / (k2 * k2 + 1);
	//若两切点都在前后线段上，返回true，否则返回false
	if (Judgepointonline(A, B, M1) && Judgepointonline(B, C, M2))
		return true;
	else return false;
}

//判断四个点能否进行连续平滑
bool RoutePointGenerator::Judge4pointsmooth(PPoint A, PPoint B, PPoint C, PPoint D)
{
	PPoint M1, M2, M3, M4;                          //四个切点
	PPoint C1, C2, C3, C4, Z1, C5, C6, C7, C8, Z2;               //八个可能的圆心C1,C2,C3,C4;C5,C6,C7,C8及真正的圆心Z1,Z2
//先求两组圆心
	double A1, B1, A2, B2, C11, C12, C21, C22;
	//第一组
	A1 = -(B.y - A.y) / (B.x - A.x + epsilon);
	B1 = 1;
	C11 = R * sqrt(A1 * A1 + B1 * B1) + B.y - B.x * (B.y - A.y) / (B.x - A.x + epsilon);
	C12 = -R * sqrt(A1 * A1 + B1 * B1) + B.y - B.x * (B.y - A.y) / (B.x - A.x + epsilon);
	A2 = -(C.y - B.y) / (C.x - B.x + epsilon);
	B2 = 1;
	C21 = R * sqrt(A2 * A2 + B2 * B2) + B.y - B.x * (C.y - B.y) / (C.x - B.x + epsilon);
	C22 = -R * sqrt(A2 * A2 + B2 * B2) + B.y - B.x * (C.y - B.y) / (C.x - B.x + epsilon);
	//圆心坐标的四种可能
	C1.x = (C11 * B2 - C21 * B1) / (A1 * B2 - A2 * B1);
	C1.y = (A1 * C21 - A2 * C11) / (A1 * B2 - A2 * B1);
	C2.x = (C11 * B2 - C22 * B1) / (A1 * B2 - A2 * B1);
	C2.y = (A1 * C22 - A2 * C11) / (A1 * B2 - A2 * B1);
	C3.x = (C12 * B2 - C21 * B1) / (A1 * B2 - A2 * B1);
	C3.y = (A1 * C21 - A2 * C12) / (A1 * B2 - A2 * B1);
	C4.x = (C12 * B2 - C22 * B1) / (A1 * B2 - A2 * B1);
	C4.y = (A1 * C22 - A2 * C12) / (A1 * B2 - A2 * B1);
	//通过判断角度是否在两条边之间来判断哪个圆心符合条件
	if (((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) < pi) && (atan3((A.y - B.y), (A.x - B.x)) > atan3((C1.y - B.y), (C1.x - B.x))) && (atan3((C1.y - B.y), (C1.x - B.x)) > (atan3((C.y - B.y), (C.x - B.x))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) < pi) && (atan3((C.y - B.y), (C.x - B.x)) > atan3((C1.y - B.y), (C1.x - B.x))) && (atan3((C1.y - B.y), (C1.x - B.x)) > (atan3((A.y - B.y), (A.x - B.x))))) || ((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) > pi) && ((atan3((C1.y - B.y), (C1.x - B.x)) > atan3((A.y - B.y), (A.x - B.x))) || (atan3((C1.y - B.y), (C1.x - B.x)) < (atan3((C.y - B.y), (C.x - B.x)))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) > pi) && ((atan3((C1.y - B.y), (C1.x - B.x)) > atan3((C.y - B.y), (C.x - B.x))) || (atan3((C1.y - B.y), (C1.x - B.x)) < (atan3((A.y - B.y), (A.x - B.x)))))))
	{
		Z1.x = C1.x;
		Z1.y = C1.y;
	}
	else if (((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) < pi) && (atan3((A.y - B.y), (A.x - B.x)) > atan3((C2.y - B.y), (C2.x - B.x))) && (atan3((C2.y - B.y), (C2.x - B.x)) > (atan3((C.y - B.y), (C.x - B.x))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) < pi) && (atan3((C.y - B.y), (C.x - B.x)) > atan3((C2.y - B.y), (C2.x - B.x))) && (atan3((C2.y - B.y), (C2.x - B.x)) > (atan3((A.y - B.y), (A.x - B.x))))) || ((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) > pi) && ((atan3((C2.y - B.y), (C2.x - B.x)) > atan3((A.y - B.y), (A.x - B.x))) || (atan3((C2.y - B.y), (C2.x - B.x)) < (atan3((C.y - B.y), (C.x - B.x)))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) > pi) && ((atan3((C2.y - B.y), (C2.x - B.x)) > atan3((C.y - B.y), (C.x - B.x))) || (atan3((C2.y - B.y), (C2.x - B.x)) < (atan3((A.y - B.y), (A.x - B.x)))))))
	{
		Z1.x = C2.x;
		Z1.y = C2.y;
	}
	else if (((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) < pi) && (atan3((A.y - B.y), (A.x - B.x)) > atan3((C3.y - B.y), (C3.x - B.x))) && (atan3((C3.y - B.y), (C3.x - B.x)) > (atan3((C.y - B.y), (C.x - B.x))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) < pi) && (atan3((C.y - B.y), (C.x - B.x)) > atan3((C3.y - B.y), (C3.x - B.x))) && (atan3((C3.y - B.y), (C3.x - B.x)) > (atan3((A.y - B.y), (A.x - B.x))))) || ((atan3((A.y - B.y), (A.x - B.x)) - atan3((C.y - B.y), (C.x - B.x)) > pi) && ((atan3((C3.y - B.y), (C3.x - B.x)) > atan3((A.y - B.y), (A.x - B.x))) || (atan3((C3.y - B.y), (C3.x - B.x)) < (atan3((C.y - B.y), (C.x - B.x)))))) || ((atan3((C.y - B.y), (C.x - B.x)) - atan3((A.y - B.y), (A.x - B.x)) > pi) && ((atan3((C3.y - B.y), (C3.x - B.x)) > atan3((C.y - B.y), (C.x - B.x))) || (atan3((C3.y - B.y), (C3.x - B.x)) < (atan3((A.y - B.y), (A.x - B.x)))))))
	{
		Z1.x = C3.x;
		Z1.y = C3.y;
	}
	else
	{
		Z1.x = C4.x;
		Z1.y = C4.y;
	}
	//已知圆心求圆弧起止点坐标
	double k1, b1, k2, b2;
	k1 = (B.y - A.y) / (B.x - A.x + epsilon);
	b1 = B.y - B.x * (B.y - A.y) / (B.x - A.x + epsilon);
	k2 = (C.y - B.y) / (C.x - B.x + epsilon);
	b2 = B.y - B.x * (C.y - B.y) / (C.x - B.x + epsilon);
	M1.x = (Z1.x + k1 * Z1.y - b1 * k1) / (k1 * k1 + 1);
	M1.y = (k1 * Z1.x + Z1.y * k1 * k1 + b1) / (k1 * k1 + 1);
	M2.x = (Z1.x + k2 * Z1.y - b2 * k2) / (k2 * k2 + 1);
	M2.y = (k2 * Z1.x + Z1.y * k2 * k2 + b2) / (k2 * k2 + 1);
	//第二组
	A1 = -(C.y - B.y) / (C.x - B.x + epsilon);
	B1 = 1;
	C11 = R * sqrt(A1 * A1 + B1 * B1) + C.y - C.x * (C.y - B.y) / (C.x - B.x + epsilon);
	C12 = -R * sqrt(A1 * A1 + B1 * B1) + C.y - C.x * (C.y - B.y) / (C.x - B.x + epsilon);
	A2 = -(D.y - C.y) / (D.x - C.x + epsilon);
	B2 = 1;
	C21 = R * sqrt(A2 * A2 + B2 * B2) + C.y - C.x * (D.y - C.y) / (D.x - C.x + epsilon);
	C22 = -R * sqrt(A2 * A2 + B2 * B2) + C.y - C.x * (D.y - C.y) / (D.x - C.x + epsilon);
	//圆心坐标的四种可能
	C5.x = (C11 * B2 - C21 * B1) / (A1 * B2 - A2 * B1);
	C5.y = (A1 * C21 - A2 * C11) / (A1 * B2 - A2 * B1);
	C6.x = (C11 * B2 - C22 * B1) / (A1 * B2 - A2 * B1);
	C6.y = (A1 * C22 - A2 * C11) / (A1 * B2 - A2 * B1);
	C7.x = (C12 * B2 - C21 * B1) / (A1 * B2 - A2 * B1);
	C7.y = (A1 * C21 - A2 * C12) / (A1 * B2 - A2 * B1);
	C8.x = (C12 * B2 - C22 * B1) / (A1 * B2 - A2 * B1);
	C8.y = (A1 * C22 - A2 * C12) / (A1 * B2 - A2 * B1);
	//通过判断角度是否在两条边之间来判断哪个圆心符合条件
	if (((atan3((B.y - C.y), (B.x - C.x)) - atan3((D.y - C.y), (D.x - C.x)) < pi) && (atan3((B.y - C.y), (B.x - C.x)) > atan3((C5.y - C.y), (C5.x - C.x))) && (atan3((C5.y - C.y), (C5.x - C.x)) > (atan3((D.y - C.y), (D.x - C.x))))) || ((atan3((D.y - C.y), (D.x - C.x)) - atan3((B.y - C.y), (B.x - C.x)) < pi) && (atan3((D.y - C.y), (D.x - C.x)) > atan3((C5.y - C.y), (C5.x - C.x))) && (atan3((C5.y - C.y), (C5.x - C.x)) > (atan3((B.y - C.y), (B.x - C.x))))) || ((atan3((B.y - C.y), (B.x - C.x)) - atan3((D.y - C.y), (D.x - C.x)) > pi) && ((atan3((C5.y - C.y), (C5.x - C.x)) > atan3((B.y - C.y), (B.x - C.x))) || (atan3((C5.y - C.y), (C5.x - C.x)) < (atan3((D.y - C.y), (D.x - C.x)))))) || ((atan3((D.y - C.y), (D.x - C.x)) - atan3((B.y - C.y), (B.x - C.x)) > pi) && ((atan3((C5.y - C.y), (C5.x - C.x)) > atan3((D.y - C.y), (D.x - C.x))) || (atan3((C5.y - C.y), (C5.x - C.x)) < (atan3((B.y - C.y), (B.x - C.x)))))))
	{
		Z2.x = C5.x;
		Z2.y = C5.y;
	}
	else if (((atan3((B.y - C.y), (B.x - C.x)) - atan3((D.y - C.y), (D.x - C.x)) < pi) && (atan3((B.y - C.y), (B.x - C.x)) > atan3((C6.y - C.y), (C6.x - C.x))) && (atan3((C6.y - C.y), (C6.x - C.x)) > (atan3((D.y - C.y), (D.x - C.x))))) || ((atan3((D.y - C.y), (D.x - C.x)) - atan3((B.y - C.y), (B.x - C.x)) < pi) && (atan3((D.y - C.y), (D.x - C.x)) > atan3((C6.y - C.y), (C6.x - C.x))) && (atan3((C6.y - C.y), (C6.x - C.x)) > (atan3((B.y - C.y), (B.x - C.x))))) || ((atan3((B.y - C.y), (B.x - C.x)) - atan3((D.y - C.y), (D.x - C.x)) > pi) && ((atan3((C6.y - C.y), (C6.x - C.x)) > atan3((B.y - C.y), (B.x - C.x))) || (atan3((C6.y - C.y), (C6.x - C.x)) < (atan3((D.y - C.y), (D.x - C.x)))))) || ((atan3((D.y - C.y), (D.x - C.x)) - atan3((B.y - C.y), (B.x - C.x)) > pi) && ((atan3((C6.y - C.y), (C6.x - C.x)) > atan3((D.y - C.y), (D.x - C.x))) || (atan3((C6.y - C.y), (C6.x - C.x)) < (atan3((B.y - C.y), (B.x - C.x)))))))
	{
		Z2.x = C6.x;
		Z2.y = C6.y;
	}
	else if (((atan3((B.y - C.y), (B.x - C.x)) - atan3((D.y - C.y), (D.x - C.x)) < pi) && (atan3((B.y - C.y), (B.x - C.x)) > atan3((C7.y - C.y), (C7.x - C.x))) && (atan3((C7.y - C.y), (C7.x - C.x)) > (atan3((D.y - C.y), (D.x - C.x))))) || ((atan3((D.y - C.y), (D.x - C.x)) - atan3((B.y - C.y), (B.x - C.x)) < pi) && (atan3((D.y - C.y), (D.x - C.x)) > atan3((C7.y - C.y), (C7.x - C.x))) && (atan3((C7.y - C.y), (C7.x - C.x)) > (atan3((B.y - C.y), (B.x - C.x))))) || ((atan3((B.y - C.y), (B.x - C.x)) - atan3((D.y - C.y), (D.x - C.x)) > pi) && ((atan3((C7.y - C.y), (C7.x - C.x)) > atan3((B.y - C.y), (B.x - C.x))) || (atan3((C7.y - C.y), (C7.x - C.x)) < (atan3((D.y - C.y), (D.x - C.x)))))) || ((atan3((D.y - C.y), (D.x - C.x)) - atan3((B.y - C.y), (B.x - C.x)) > pi) && ((atan3((C7.y - C.y), (C7.x - C.x)) > atan3((D.y - C.y), (D.x - C.x))) || (atan3((C7.y - C.y), (C7.x - C.x)) < (atan3((B.y - C.y), (B.x - C.x)))))))
	{
		Z2.x = C7.x;
		Z2.y = C7.y;
	}
	else
	{
		Z2.x = C8.x;
		Z2.y = C8.y;
	}
	//已知圆心求圆弧起止点坐标
	k1 = (C.y - B.y) / (C.x - B.x + epsilon);
	b1 = C.y - C.x * (C.y - B.y) / (C.x - B.x + epsilon);
	k2 = (D.y - C.y) / (D.x - C.x + epsilon);
	b2 = C.y - C.x * (D.y - C.y) / (D.x - C.x + epsilon);
	M3.x = (Z2.x + k1 * Z2.y - b1 * k1) / (k1 * k1 + 1);
	M3.y = (k1 * Z2.x + Z2.y * k1 * k1 + b1) / (k1 * k1 + 1);
	M4.x = (Z2.x + k2 * Z2.y - b2 * k2) / (k2 * k2 + 1);
	M4.y = (k2 * Z2.x + Z2.y * k2 * k2 + b2) / (k2 * k2 + 1);
	//若两次平滑能够满足，返回true，否则返回false
	if ((B.x < M2.x && M2.x < M3.x && M3.x < C.x) || (B.x > M2.x && M2.x > M3.x && M3.x > C.x) || (B.y < M2.y && M2.y < M3.y && M3.y < C.y) || (B.y > M2.y && M2.y > M3.y && M3.y > C.y))
		return true;
	else return false;
}

double RoutePointGenerator::atan3(double a, double b)
{
	double c;
	if (b == 0)
		c = pi / 2 * a / abs(a);
	else
		c = atan2(a, b);
	return c;
}

RoutePointGenerator::RoutePointGenerator(std::vector<Car_Info>& obstacle_info, const double length, const double width, double speed, double radius):car_length(length),car_width(width),R(radius),car_speed(speed)
{	
	//利用障碍物信息获取全部障碍物载具四个边界点的信息
	cars_boundary_points.reserve(obstacle_info.size() * 4);
	for (auto& car_info : obstacle_info)
	{
		//1、获取障碍物左后方坐标以及障碍物车长、宽、倾斜角
		PPoint _center=car_info.car_center;
		vector<PPoint> points(4);
		//2、根据左后方获得中心点坐标
		/*CalculateCarCenterPoint(car_info.car_length, car_info.car_width, car_info.back_left, car_info.angle, _center);*/
		//3、根据中心点坐标获得边界点坐标并存储
		CalculateCarBoundaryPoint(car_info.car_length, car_info.car_width, _center, car_info.angle, points[0], points[1], points[2], points[3]);
		cars_boundary_points.push_back(points);
	}
}
//根据全部障碍物载具四个边界点的信息、载具长宽构造
RoutePointGenerator::RoutePointGenerator(std::vector<std::vector<PPoint>>& _boundary_points, const double length, const double width, double radius) :car_length(length), car_width(width),R(radius)
{
	cars_boundary_points = _boundary_points;
}


void RoutePointGenerator::SetGuidePoints(const vector<PPoint>& _guide_points, double d_begin, double d_end)
{
	//设置起点、终点方向
	end_direction = d_end;
	begin_direction = d_begin;

	route_points.clear();
	route_curve_equation.clear();
	bad_guide_points.clear();
	guide_points.clear();

	route_points.reserve(_guide_points.size() * 20);
	route_curve_equation.reserve(_guide_points.size() * 2);
	bad_guide_points.reserve(_guide_points.size());
	guide_points.reserve(_guide_points.size());
	for (const auto p : _guide_points)
		guide_points.emplace_back(p);
}
void RoutePointGenerator::GenerateRoutePoints()
{
	//设置状态初值为true，即可用
	is_available = true;
	//若导引点序列为空，直接退出并设置状态为不可用
	if (guide_points.empty())
	{
		is_available = false;
		return;
	}
	GenerateRouteCurveEquationByGuidePoints();
	//对转弯点进行输入检查，避免死循环
	//每三个点进行检查
	if (guide_points.size() >= 3) {
		for (int i = 0; i <= guide_points.size() - 3; ++i) {
			auto prePoint = guide_points[i];
			auto midPoint = guide_points[i+ 1];
			auto posPoint = guide_points[i + 2];
			if (Judge3pointsmooth(prePoint, midPoint, posPoint) == false) {
				//三点不能平滑，设置状态为false，表示不可用
				is_available = false;
				return;
			}
		}
	}
	////每四个点进行检查
	//if (guide_points.size() >= 4) {
	//	for (int i = 0; i <= guide_points.size() - 4; ++i) {
	//		auto Point1 = guide_points[i];
	//		auto Point2 = guide_points[i + 1];
	//		auto Point3 = guide_points[i + 2];
	//		auto Point4 = guide_points[i + 3];
	//		if (Judge4pointsmooth(Point1, Point2, Point3, Point4) == false) {
	//			//四点不能平滑，设置状态为false，表示不可用
	//			is_available = false;
	//			return;
	//		}
	//	}
	//}

	GenerateRoutePointsByRouteCurveEquation();
}
std::vector<RoutePoint> RoutePointGenerator::GetRoutePoints() const
{
	return route_points;
}
int RoutePointGenerator::GetCountsOfRoutePoints() const
{
	return route_points.size();
}

//************************************
// Method:    StraightLineGeneration_Storage
// FullName:  RoutePointGenerator::::StraightLineGeneration_Storage
// Access:    private 
// Returns:   void
// Qualifier:获得2个转向点之间的直线方程并存储到route_curve_equation，直线方程分为斜截式和斜率不存在的直线
// Parameter: PPoint front 第一个点
// Parameter: PPoint back 第二个点
//************************************
void RoutePointGenerator::StraightLineGeneration_Storage(PPoint front, PPoint back)
{
	//直线方程
	vector<double> equation;
	//类型为2，存储直线线段信息
	equation.push_back(2);
	//存储两个点的值
	equation.push_back(front.x);
	equation.push_back(front.y);
	equation.push_back(back.x);
	equation.push_back(back.y);
	route_curve_equation.push_back(equation);
}
//************************************
// Method:    CurveGeneration
// FullName:  RoutePointGenerator::CurveGeneration
// Access:    private 
// Returns:   void
// Qualifier:获得3个转向点之间的曲线方程，这里是一段圆弧，返回值为类内private:圆心坐标(X,Y),切点坐标M1(Mx1,My1),M2(Mx2,My2)以及车辆转过角度Θ
// Parameter: PPoint front 第一个点
// Parameter: PPoint middle 第二个点
// Parameter: PPoint back 第三个点
//************************************
void RoutePointGenerator::CurveGeneration(PPoint front, PPoint middle, PPoint back)
{
	double X1 = front.x;
	double Y1 = front.y;
	double X2 = middle.x;
	double Y2 = middle.y;
	double X3 = back.x;
	double Y3 = back.y;
	//先求圆心
	double A1, B1, A2, B2, C11, C12, C21, C22, X11, Y11, X12, Y12, X21, Y21, X22, Y22;

	A1 = -(Y2 - Y1) / (X2 - X1 + 0.000001);
	B1 = 1;
	C11 = R * sqrt(A1 * A1 + B1 * B1) + Y2 - X2 * (Y2 - Y1) / (X2 - X1 + 0.000001);
	C12 = -R * sqrt(A1 * A1 + B1 * B1) + Y2 - X2 * (Y2 - Y1) / (X2 - X1 + 0.000001);
	A2 = -(Y3 - Y2) / (X3 - X2 + 0.000001);
	B2 = 1;
	C21 = R * sqrt(A2 * A2 + B2 * B2) + Y2 - X2 * (Y3 - Y2) / (X3 - X2 + 0.000001);
	C22 = -R * sqrt(A2 * A2 + B2 * B2) + Y2 - X2 * (Y3 - Y2) / (X3 - X2 + 0.000001);
	//圆心坐标的四种可能
	X11 = (C11 * B2 - C21 * B1) / (A1 * B2 - A2 * B1);
	Y11 = (A1 * C21 - A2 * C11) / (A1 * B2 - A2 * B1);
	X12 = (C11 * B2 - C22 * B1) / (A1 * B2 - A2 * B1);
	Y12 = (A1 * C22 - A2 * C11) / (A1 * B2 - A2 * B1);
	X21 = (C12 * B2 - C21 * B1) / (A1 * B2 - A2 * B1);
	Y21 = (A1 * C21 - A2 * C12) / (A1 * B2 - A2 * B1);
	X22 = (C12 * B2 - C22 * B1) / (A1 * B2 - A2 * B1);
	Y22 = (A1 * C22 - A2 * C12) / (A1 * B2 - A2 * B1);
	//通过判断角度是否在两条边之间来判断哪个圆心符合条件
	if (((atan3((Y1 - Y2), (X1 - X2)) - atan3((Y3 - Y2), (X3 - X2)) < pi) && (atan3((Y1 - Y2), (X1 - X2)) > atan3((Y11 - Y2), (X11 - X2))) && (atan3((Y11 - Y2), (X11 - X2)) > (atan3((Y3 - Y2), (X3 - X2))))) || ((atan3((Y3 - Y2), (X3 - X2)) - atan3((Y1 - Y2), (X1 - X2)) < pi) && (atan3((Y3 - Y2), (X3 - X2)) > atan3((Y11 - Y2), (X11 - X2))) && (atan3((Y11 - Y2), (X11 - X2)) > (atan3((Y1 - Y2), (X1 - X2))))) || ((atan3((Y1 - Y2), (X1 - X2)) - atan3((Y3 - Y2), (X3 - X2)) > pi) && ((atan3((Y11 - Y2), (X11 - X2)) > atan3((Y1 - Y2), (X1 - X2))) || (atan3((Y11 - Y2), (X11 - X2)) < (atan3((Y3 - Y2), (X3 - X2)))))) || ((atan3((Y3 - Y2), (X3 - X2)) - atan3((Y1 - Y2), (X1 - X2)) > pi) && ((atan3((Y11 - Y2), (X11 - X2)) > atan3((Y3 - Y2), (X3 - X2))) || (atan3((Y11 - Y2), (X11 - X2)) < (atan3((Y1 - Y2), (X1 - X2)))))))
	{
		X = X11;
		Y = Y11;
	}
	else if (((atan3((Y1 - Y2), (X1 - X2)) - atan3((Y3 - Y2), (X3 - X2)) < pi) && (atan3((Y1 - Y2), (X1 - X2)) > atan3((Y12 - Y2), (X12 - X2))) && (atan3((Y12 - Y2), (X12 - X2)) > (atan3((Y3 - Y2), (X3 - X2))))) || ((atan3((Y3 - Y2), (X3 - X2)) - atan3((Y1 - Y2), (X1 - X2)) < pi) && (atan3((Y3 - Y2), (X3 - X2)) > atan3((Y12 - Y2), (X12 - X2))) && (atan3((Y12 - Y2), (X12 - X2)) > (atan3((Y1 - Y2), (X1 - X2))))) || ((atan3((Y1 - Y2), (X1 - X2)) - atan3((Y3 - Y2), (X3 - X2)) > pi) && ((atan3((Y12 - Y2), (X12 - X2)) > atan3((Y1 - Y2), (X1 - X2))) || (atan3((Y12 - Y2), (X12 - X2)) < (atan3((Y3 - Y2), (X3 - X2)))))) || ((atan3((Y3 - Y2), (X3 - X2)) - atan3((Y1 - Y2), (X1 - X2)) > pi) && ((atan3((Y12 - Y2), (X12 - X2)) > atan3((Y3 - Y2), (X3 - X2))) || (atan3((Y12 - Y2), (X12 - X2)) < (atan3((Y1 - Y2), (X1 - X2)))))))
	{
		X = X12;
		Y = Y12;

	}
	else if (((atan3((Y1 - Y2), (X1 - X2)) - atan3((Y3 - Y2), (X3 - X2)) < pi) && (atan3((Y1 - Y2), (X1 - X2)) > atan3((Y21 - Y2), (X21 - X2))) && (atan3((Y21 - Y2), (X21 - X2)) > (atan3((Y3 - Y2), (X3 - X2))))) || ((atan3((Y3 - Y2), (X3 - X2)) - atan3((Y1 - Y2), (X1 - X2)) < pi) && (atan3((Y3 - Y2), (X3 - X2)) > atan3((Y21 - Y2), (X21 - X2))) && (atan3((Y21 - Y2), (X21 - X2)) > (atan3((Y1 - Y2), (X1 - X2))))) || ((atan3((Y1 - Y2), (X1 - X2)) - atan3((Y3 - Y2), (X3 - X2)) > pi) && ((atan3((Y21 - Y2), (X21 - X2)) > atan3((Y1 - Y2), (X1 - X2))) || (atan3((Y21 - Y2), (X21 - X2)) < (atan3((Y3 - Y2), (X3 - X2)))))) || ((atan3((Y3 - Y2), (X3 - X2)) - atan3((Y1 - Y2), (X1 - X2)) > pi) && ((atan3((Y21 - Y2), (X21 - X2)) > atan3((Y3 - Y2), (X3 - X2))) || (atan3((Y21 - Y2), (X21 - X2)) < (atan3((Y1 - Y2), (X1 - X2)))))))
	{
		X = X21;
		Y = Y21;

	}
	else if (((atan3((Y1 - Y2), (X1 - X2)) - atan3((Y3 - Y2), (X3 - X2)) < pi) && (atan3((Y1 - Y2), (X1 - X2)) > atan3((Y22 - Y2), (X22 - X2))) && (atan3((Y22 - Y2), (X22 - X2)) > (atan3((Y3 - Y2), (X3 - X2))))) || ((atan3((Y3 - Y2), (X3 - X2)) - atan3((Y1 - Y2), (X1 - X2)) < pi) && (atan3((Y3 - Y2), (X3 - X2)) > atan3((Y22 - Y2), (X22 - X2))) && (atan3((Y22 - Y2), (X22 - X2)) > (atan3((Y1 - Y2), (X1 - X2))))) || ((atan3((Y1 - Y2), (X1 - X2)) - atan3((Y3 - Y2), (X3 - X2)) > pi) && ((atan3((Y22 - Y2), (X22 - X2)) > atan3((Y1 - Y2), (X1 - X2))) || (atan3((Y22 - Y2), (X22 - X2)) < (atan3((Y3 - Y2), (X3 - X2)))))) || ((atan3((Y3 - Y2), (X3 - X2)) - atan3((Y1 - Y2), (X1 - X2)) > pi) && ((atan3((Y22 - Y2), (X22 - X2)) > atan3((Y3 - Y2), (X3 - X2))) || (atan3((Y22 - Y2), (X22 - X2)) < (atan3((Y1 - Y2), (X1 - X2)))))))
	{
		X = X22;
		Y = Y22;

	}
	//已知圆心求圆弧起止点坐标
	double k1, b1, k2, b2;
	k1 = (Y2 - Y1) / (X2 - X1 + 0.000001);
	b1 = Y2 - X2 * (Y2 - Y1) / (X2 - X1 + 0.000001);
	k2 = (Y3 - Y2) / (X3 - X2 + 0.000001);
	b2 = Y2 - X2 * (Y3 - Y2) / (X3 - X2 + 0.000001);
	Mx1 = (X + k1 * Y - b1 * k1) / (k1 * k1 + 1);
	My1 = (k1 * X + Y * k1 * k1 + b1) / (k1 * k1 + 1);
	Mx2 = (X + k2 * Y - b2 * k2) / (k2 * k2 + 1);
	My2 = (k2 * X + Y * k2 * k2 + b2) / (k2 * k2 + 1);

}
//************************************
// Method:    GenerateRouteCurveEquationByGuidePoints
// FullName:  GuidePointGenerator::GenerateRouteCurveEquationByGuidePoints
// Access:    public 
// Returns:   void
// Qualifier:根据导引点获取路线中各个曲线/直线的方程
//************************************
void RoutePointGenerator::GenerateRouteCurveEquationByGuidePoints()
{
	//route_curve_equation
	//guide_points
	//如果导引点没有两个，返回
	if (guide_points.size() < 2) return;
	//如果导引点只有两个，即只有起点和终点
	if (guide_points.size() == 2)
	{
		//生成直线方程并存储
		StraightLineGeneration_Storage(guide_points[0], guide_points[1]);
	}
	else
	{
		//如果导引点至少3个点
		//初始化曲线/直线的左右界点
		Mx2 = Mx1 = guide_points.front().x;
		My2 = My1 = guide_points.front().y;
		for (size_t i = 0; i < guide_points.size() - 2; ++i)
		{
			auto front_point = guide_points[i];
			auto middle_point = guide_points[i + 1];
			auto back_point = guide_points[i + 2];
			//存储上一个曲线的最后一个点
			double previous_Mx2 = Mx2, previous_My2 = My2;
			//生成曲线信息
			CurveGeneration(front_point, middle_point, back_point);
			//double X, Y, Mx1, My1, Mx2, My2; 
			//生成直线方程并存储
			PPoint line_p1(previous_Mx2, previous_My2);
			PPoint line_p2(Mx1, My1);
			StraightLineGeneration_Storage(line_p1, line_p2);
			//存储曲线方程
			vector<double> equation;
			//类型为1,圆弧
			equation.push_back(1);
			//存储圆心坐标,两个界点		
			equation.push_back(Mx1);
			equation.push_back(My1);
			equation.push_back(Mx2);
			equation.push_back(My2);
			equation.push_back(X);
			equation.push_back(Y);
			route_curve_equation.push_back(equation);

		}
		//最后到终点的一段直线
		//生成直线方程并存储
		PPoint line_p1(Mx2, My2);
		PPoint line_p2(guide_points.back().x, guide_points.back().y);
		StraightLineGeneration_Storage(line_p1, line_p2);

	}
}
//************************************
// Method:    GenerateRoutePointsByRouteCurveEquation
// FullName:  RoutePointGenerator::GenerateRoutePointsByRouteCurveEquation
// Access:    public 
// Returns:   void
// Qualifier:根据路径曲线/直线方程产生路径点，若圆弧导引点下的路线点发生碰撞，则存储对应的导引点
//************************************
void RoutePointGenerator::GenerateRoutePointsByRouteCurveEquation()
{
	//如果当前路径不可用，直接退出该函数
	if (is_available == false) return;

	//设置载具速度与运动半径
	SetCarSpeed_Radius(car_speed, R);
	//剩余时间，初始为0.1s
	double time_remain = 0.1;
	//存储上一个线段的最后一个点，用于圆弧中轨迹点的生成
	PPoint last_point;

	//首先存储起点
	auto e = route_curve_equation.front();
	PPoint first_point(e[1], e[2]);
	//第一个点的方向为外界参数
	RoutePoint first_route_point(first_point, begin_direction, 0, 0);
	route_points.push_back(first_route_point);
	//若首个路线点发生碰撞，存储第一个导引点
	if (CollisionCheckForRoutePoints(first_route_point))
		bad_guide_points.push_back(guide_points.front());

	//圆弧导引点下标
	int counts = 0;
	for (auto equation : route_curve_equation)
	{
		//如果当前路径不可用，直接退出该函数
		if (is_available == false) return;
		//根据曲线/直线方程类型生成离散点
		switch (static_cast<int>(equation.front()))
		{
		case 1: {
			//圆弧段判断其对应的导引点是否已经存储
			bool is_storage = false;
			counts++;
			//类型为1，一段圆弧		
			//当前点坐标为route_points.back().coordinate，圆弧终点与圆心坐标
			PPoint start_point(equation[1], equation[2]), end_point(equation[3], equation[4]), circle_center(equation[5], equation[6]);
			//计算得到的下一个点的坐标与角度
			PPoint next_point;
			double next_point_angle = 0;
			//如果从上个直线/曲线过渡到圆弧还有剩余时间，则单独计算圆弧段第一个点，以起点和剩余时间计算
			if (time_remain < 0.1)
			{
				double time_tem;
				//特殊情况：如果前一个轨迹点为上一个线段/曲线的尾边界点
				if (time_remain == 0)
					time_tem = CalculateRoutePointInArcLine(last_point, start_point, next_point, next_point_angle, end_point, circle_center);
				else
					time_tem = CalculateRoutePointInArcLine(last_point, start_point, next_point, next_point_angle, end_point, circle_center, time_remain);

				//如果第一个点就生成不了（圆弧太短），则将time_remain置为0，并进入下一个方程，否则存储路径点
				if (time_tem == 0.1)
				{
					time_remain = 0.1;
					RoutePoint route_point(next_point, next_point_angle, 0, 0);
					route_points.push_back(route_point);
					//若当前路线点发生碰撞，存储对应导引点
					if (CollisionCheckForRoutePoints(route_point) && is_storage == false)
					{
						bad_turning_guide_points.push_back(guide_points.at(counts));
						bad_guide_points.push_back(guide_points.at(counts));
						is_storage = true;
					}
				}
				else
				{
					time_remain = 0;
					continue;
				}
			}
			
			//生成圆弧上所有轨迹点，最后的剩余时间存储在time_remain中
			while (time_remain == 0.1)
			{
				time_remain = CalculateRoutePointInArcLine(last_point, route_points.back().coordinate, next_point, next_point_angle, end_point, circle_center);
				//存储路径点
				if (time_remain == 0.1)
				{
					RoutePoint route_point(next_point, next_point_angle, 0, 0);
					route_points.push_back(route_point);
					//若当前路线点发生碰撞，存储对应导引点
					if (CollisionCheckForRoutePoints(route_point) && is_storage == false)
					{
						bad_turning_guide_points.push_back(guide_points.at(counts));
						bad_guide_points.push_back(guide_points.at(counts));
						is_storage = true;
					}
				}
				//若当前路径的个数超过5000，则表示陷入死循环，跳出当前switch
				if (route_points.size() > 5000) {
					is_available = false;
					break;
				}
			}

		} break;
		case 2: {
			//直线段判断其对应的两个导引点是否已经存储
			bool is_storage = false;
			//类型为2，直线线段两点
			//当前点坐标为route_points.back().coordinate、线段起点与终点信息
			PPoint start_point(equation[1], equation[2]), end_point(equation[3], equation[4]);
			//计算得到的下一个点的坐标与角度
			PPoint next_point;
			double next_point_angle = 0;
			//如果从上个直线/曲线过渡到直线还有剩余时间，则单独计算直线段第一个点，以起点和剩余时间计算
			if (time_remain < 0.1)
			{
				double time_tem;
				//特殊情况：如果前一个轨迹点为上一个线段/曲线的尾边界点
				if (time_remain == 0)
					time_tem = CalculateRoutePointInStraightLine(start_point, next_point, next_point_angle, start_point, end_point);
				else
					time_tem = CalculateRoutePointInStraightLine(start_point, next_point, next_point_angle, start_point, end_point, time_remain);
				////如果第一个点就生成不了（直线太短），则将time_remain置为0，并进入下一个方程，否则存储路径点
				if (time_tem == 0.1)
				{
					time_remain = time_tem;
					RoutePoint route_point(next_point, next_point_angle, 0, 0);
					route_points.push_back(route_point);
					//若当前路线点发生碰撞，存储对应导引点
					if (CollisionCheckForRoutePoints(route_point) && is_storage == false)
					{
						bad_guide_points.push_back(guide_points.at(counts));
						bad_guide_points.push_back(guide_points.at(counts + 1));
						is_storage = true;
					}
				}
				else
				{
					time_remain = 0;
					continue;
				}
			}
			
			//生成直线线段上所有轨迹点
			while (time_remain == 0.1)
			{
				time_remain = CalculateRoutePointInStraightLine(route_points.back().coordinate, next_point, next_point_angle, start_point, end_point);
				//存储路径点
				if (time_remain == 0.1)
				{
					RoutePoint route_point(next_point, next_point_angle, 0, 0);
					route_points.push_back(route_point);
					//若当前路线点发生碰撞，存储对应导引点
					if (CollisionCheckForRoutePoints(route_point) && is_storage == false)
					{
						bad_guide_points.push_back(guide_points.at(counts));
						bad_guide_points.push_back(guide_points.at(counts + 1));
						is_storage = true;
					}
				}
				//若当前路径的个数超过5000，则表示陷入死循环，跳出当前switch
				if (route_points.size() > 5000) {
					is_available = false;
					break;
				}
			}
			//更新线段的最后一个点
			last_point.x = route_points.back().coordinate.x;
			last_point.y = route_points.back().coordinate.y;

		} break;

		}
		
	}
	//最后查看终点是否存储，若有，则设置终点的方向，若没有，则存储终点并设置终点的方向
	e = route_curve_equation.back();
	PPoint end_point(e[3], e[4]);

	if (abs((route_points.back().coordinate.x) - (end_point.x)) < EPSINON  && abs((route_points.back().coordinate.y) - (end_point.y)) < EPSINON)
		route_points.back().angle_z=end_direction;
	else
	{
		//终点的方向
		RoutePoint last_route_point(end_point, end_direction, 0, 0);
		route_points.push_back(last_route_point);
		//若最后一个路线点发生碰撞，存储第最后一个导引点
		if (CollisionCheckForRoutePoints(last_route_point))
			bad_guide_points.push_back(guide_points.back());
	}
}


//************************************
// Method:    PrintRoutePointsToGSV
// FullName:  RoutePointGenerator::PrintRoutePointsToGSV
// Access:    public 
// Returns:   void
// Qualifier:将离散的路径点序列输出到文件中
//************************************
void RoutePointGenerator::PrintRoutePointsToGSV()
{
	//将转折点转换，并输出到文件中
	FILE* fd = NULL;
	if ((fd = fopen("route_points.csv", "wt+")) != NULL)
	{
		for (auto point : route_points)
		{

			fprintf(fd, "%f,%f\n", point.coordinate.x, point.coordinate.y);
		}
	}
	fclose(fd);
	fd = NULL;
}

//************************************
// Method:    CollisionCheckForRoutePoints
// FullName:  RoutePointGenerator::CollisionCheckForRoutePoints
// Access:    private 
// Returns:   bool 发生碰撞 返回true 否则返回false
// Qualifier:检测载具在当前路径点时是否会触碰障碍物,若碰撞，则存储对应的信息
// Parameter: PPoint route_point
//************************************
bool RoutePointGenerator::CollisionCheckForRoutePoints(RoutePoint route_point) 
{
	vector<PPoint> cur_car_points(4);
	CalculateCarBoundaryPoint(car_length, car_width, route_point.coordinate, route_point.angle_z, cur_car_points[0], cur_car_points[1], cur_car_points[2], cur_car_points[3]);
	//判断载具在当前位置是否与其它障碍物碰撞
	for (const auto& car_info : cars_boundary_points)
	{
		if (CalculateCollisionOfTwoCars(car_info, cur_car_points))
		{			
			//第一次发生碰撞时的路径点序号
			first_collison_id = first_collison_id == 0 ? route_points.size(): first_collison_id;
			//设置路径是否可用 true-可用 false—发生碰撞，不可用
			is_available = is_available == true ? false : is_available;
			return true;
		}
	}
	return false;
}

//************************************
// Method:    PrintRectangleOfCollisionToGSV
// FullName:  RoutePointGenerator::PrintRectangleOfCollisionToGSV
// Access:    public 
// Returns:   void
// Qualifier:将第一次发生碰撞时的矩形输出到文件中
//************************************
void RoutePointGenerator::PrintRectangleOfCollisionToGSV()
{
	//如果碰撞了，输出
	/*if (is_available == false)
	{
		cout << "发生了碰撞！" << endl;
		cout << "碰撞导引点为：" << endl;
		for (auto i : bad_guide_points)
			cout << i.x<<"   "<<i.y << endl;
	}*/

	//若没发生碰撞，则直接返回
	if (is_available == true) return;
	//若first_collison_id无效，返回
	if (route_points.size() < first_collison_id + 1) return;
	RoutePoint route_point = route_points.at(first_collison_id);
	vector<PPoint> cur_car_points(4);
	CalculateCarBoundaryPoint(car_length, car_width, route_point.coordinate, route_point.angle_z, cur_car_points[0], cur_car_points[1], cur_car_points[2], cur_car_points[3]);
	//将障碍物与此时载具的位置存储，并输出到文件中
	FILE* fd = NULL;
	if ((fd = fopen("rectangle_of_collision.csv", "wt+")) != NULL)
	{
		for (auto point : cars_boundary_points)
		{

			fprintf(fd, "%f,%f,%f,%f,%f\n", point[0].x, point[1].x, point[2].x, point[3].x, point[0].x);
			fprintf(fd, "%f,%f,%f,%f,%f\n", point[0].y, point[1].y, point[2].y, point[3].y, point[0].y);
		}
		fprintf(fd, "%f,%f,%f,%f,%f\n", cur_car_points[0].x, cur_car_points[1].x, cur_car_points[2].x, cur_car_points[3].x, cur_car_points[0].x);
		fprintf(fd, "%f,%f,%f,%f,%f\n", cur_car_points[0].y, cur_car_points[1].y, cur_car_points[2].y, cur_car_points[3].y, cur_car_points[0].y);
	}
	fclose(fd);
	fd = NULL;
}
void RoutePointGenerator::PrintRectangleOfObstaclesToGSV()
{
	//将障碍物与此时载具的位置存储，并输出到文件中
	FILE* fd = NULL;
	if ((fd = fopen("rectangle_of_obstacles.csv", "wt+")) != NULL)
	{
		for (auto point : cars_boundary_points)
		{

			fprintf(fd, "%f,%f,%f,%f,%f\n", point[0].x, point[1].x, point[2].x, point[3].x, point[0].x);
			fprintf(fd, "%f,%f,%f,%f,%f\n", point[0].y, point[1].y, point[2].y, point[3].y, point[0].y);
		}
	}
	fclose(fd);
	fd = NULL;
}

//************************************
// Method:    GetBadGuidePoints
// FullName:  RoutePointGenerator::GetBadGuidePoints
// Access:    public 
// Returns:   std::vector<PPoint>
// Qualifier:  获取发生碰撞的路径点对应的导引点序列
//************************************
std::vector<PPoint> RoutePointGenerator::GetBadGuidePoints()
{
	//去除重复碰撞导引点
	auto ite = unique(bad_guide_points.begin(), bad_guide_points.end());
	bad_guide_points.erase(ite, bad_guide_points.end());
	return bad_guide_points;
}
//************************************
// Method:    GetBadTurningGuidePoints
// FullName:  RoutePointGenerator::GetBadTurningGuidePoints
// Access:    public 
// Returns:   std::vector<PPoint>
// Qualifier:const 获取在转弯时发生碰撞的路径点对应的导引点序列
//************************************
std::vector<PPoint> RoutePointGenerator::GetBadTurningGuidePoints() const
{
	return bad_turning_guide_points;
}
//************************************
// Method:    GetPathStatus
// FullName:  RoutePointGenerator::GetPathStatus
// Access:    public 
// Returns:   bool
// Qualifier:获得当前路线的状态,true-可用 false—发生碰撞，不可用
//************************************
bool RoutePointGenerator::GetPathStatus()
{
	return is_available;
}

//************************************
// Method:    GetFirstCollisonRoutePointId
// FullName:  RoutePointGenerator::GetFirstCollisonRoutePointId
// Access:    public 
// Returns:   int	值从1开始
// Qualifier:第一次发生碰撞时的路径点序号
//************************************
int RoutePointGenerator::GetFirstCollisonRoutePointId()
{
	return first_collison_id;
}