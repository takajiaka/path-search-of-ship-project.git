#include "pch.h"
#include "ConfigurationPlan.h"
#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include "AStar.h"
#include "GuidePointGenerator.h"
#include "RoutePointGenerator.h"
#include "Classes.h"
#include "Calculate.h"
#define _USE_MATH_DEFINES
#include<math.h>
#include<vector>
using namespace std;

vector<cars_info>Cars;
vector<hovercraft_info>Hovercraft;
vector<obstacles_info>Obstacle;
vector<Layout_Plan_End> result;
//生成布列方案
class ConfigurationPlanCreator
{
private:
	double workshop_length = 72;
	double workshop_width = 18.2;
	struct already_coordinate
	{
		double A_x;   //矩形左上角为A点(0度摆放时)
		double A_y;
		double B_x;   //矩形右上角为B点(0度摆放时)
		double B_y;
		double C_x;   //矩形右下角为C点(0度摆放时)
		double C_y;
		double D_x;   //矩形左下角为D点(0度摆放时)
		double D_y;
	};
	vector<already_coordinate>already_car;  //已布列车辆四角坐标
	struct current_coordinate
	{
		double A_x;   //矩形左上角为A点(0度摆放时)
		double A_y;
		double B_x;   //矩形右上角为B点(0度摆放时)
		double B_y;
		double C_x;   //矩形右下角为C点(0度摆放时)
		double C_y;
		double D_x;   //矩形左下角为D点(0度摆放时)
		double D_y;
	};
	vector<current_coordinate>current_car;  //正在布列车辆四角坐标

	//函数：计算坐标。将矩形的四角视作在圆心为（a,b），半径为R的圆上，知道角度angle的话，就能计算相应坐标
	double* calculate_coordinate(double a, double b, double R, double angle)
	{
		double x = 0, y = 0;
		double* result = new double[2];
		angle = fmod(angle, 2 * M_PI);
		if (angle > 0 && angle < (M_PI / 2))
		{
			x = a + R * cos(angle);
			y = b + R * sin(angle);
		}
		if (angle > (M_PI / 2) && angle < M_PI)
		{
			x = a - R * cos(M_PI - angle);
			y = b + R * sin(M_PI - angle);

		}
		if (angle > M_PI && angle < (3 * M_PI / 2))
		{
			x = a - R * sin((3 * M_PI / 2) - angle);
			y = b - R * cos((3 * M_PI / 2) - angle);
		}
		if (angle > (3 * M_PI / 2) && angle < (2 * M_PI))
		{
			x = a + R * cos(2 * M_PI - angle);
			y = b - R * sin(2 * M_PI - angle);
		}
		result[0] = x;
		result[1] = y;
		return result;
	}

	//函数：判断线段AB和线段CD是否相交(结果为0=没有相交，结果为1=相交)
	int judge_intersect(double A_x, double A_y, double B_x, double B_y, double C_x, double C_y, double D_x, double D_y)
	{
		int result = 0;
		double AB_x = B_x - A_x;
		double AB_y = B_y - A_y;
		double AC_x = C_x - A_x;
		double AC_y = C_y - A_y;
		double AD_x = D_x - A_x;
		double AD_y = D_y - A_y;

		double CD_x = D_x - C_x;
		double CD_y = D_y - C_y;
		double CA_x = A_x - C_x;
		double CA_y = A_y - C_y;
		double CB_x = B_x - C_x;
		double CB_y = B_y - C_y;

		double temp1 = 0;
		temp1 = (AB_x * AC_y - AB_y * AC_x) * (AB_x * AD_y - AB_y * AD_x);
		double temp2 = 0;
		temp2 = (CD_x * CA_y - CD_y * CA_x) * (CD_x * CB_y - CD_y * CB_x);

		if (temp1 < -1e-15 && temp2 < -1e-15)
		{
			result = 1;
		}

		if (-1e-15 < temp1 && temp1 < 1e-15)
		{
			if (temp2 < -1e-15)
			{
				result = 1;
			}
		}
		if (-1e-15 < temp2 && temp2 < 1e-15)
		{
			if (temp1 < -1e-15)
			{
				result = 1;
			}
		}

		if (-1e-15 < temp1 && temp1 < 1e-15)
		{
			if (-1e-15 < temp2 && temp2 < 1e-15)
			{
				if (-1e-15 < (AB_x * CD_y - AB_y * CD_x) && (AB_x * CD_y - AB_y * CD_x) < 1e-15)
				{
					double flags1[4] = { A_x,B_x,C_x,D_x };
					double flags2[4] = { A_y,B_y,C_y,D_y };
					int flags3[4] = { -1,-1,1,1 };
					for (int i = 0; i < 3; i++)
					{
						for (int j = i + 1; j < 4; j++)
						{
							if ((flags1[i] > flags1[j]) || ((flags1[i] == flags1[j]) && (flags2[i] > flags2[j])))
							{
								swap(flags1[i], flags1[j]);
								swap(flags2[i], flags2[j]);
								swap(flags3[i], flags3[j]);
							}
						}
					}
					if (flags3[0] * flags3[2] > 0)
					{
						result = 1;
					}
				}
			}
		}

		if ((A_x == C_x && A_y == C_y) || (A_x == D_x && A_y == D_y) || (B_x == C_x && B_y == C_y) || (B_x == D_x && B_y == D_y))
		{
			result = 1;
		}


		return result;
	}

	//函数：判断一个点E是否在矩形ABCD中
	int judge_contain(double E_x, double E_y, double A_x, double A_y, double B_x, double B_y, double C_x, double C_y, double D_x, double D_y)
	{
		int result = 0;
		double AB_x = B_x - A_x;
		double AB_y = B_y - A_y;
		double DC_x = C_x - D_x;
		double DC_y = C_y - D_y;
		double AD_x = D_x - A_x;
		double AD_y = D_y - A_y;
		double BC_x = C_x - B_x;
		double BC_y = C_y - B_y;

		double AE_x = E_x - A_x;
		double AE_y = E_y - A_y;
		double DE_x = E_x - D_x;
		double DE_y = E_y - D_y;
		double BE_x = E_x - B_x;
		double BE_y = B_y - E_y;

		if ((AB_x * AE_y - AB_y * AE_x) * (DC_x * DE_y - DC_y * DE_x) <= 0)
		{
			if ((AD_x * AE_y - AD_y * AE_x) * (BC_x * BE_y - BC_y * BE_x) <= 0)
			{
				result = 1;
			}
		}

		return result;
	}

	//函数：判断现有车辆是否被已有车辆包含或者包含已有车辆
	int judge_carscontain(vector<current_coordinate>current_car, vector<already_coordinate>already_car)
	{
		int result = 0;
		int temp[4] = { 0 };
		int temp0[4] = { 0 };

		//判断现有车辆是否被已有车辆包含
		for (int i = 0; i < (int)already_car.size(); i++)
		{
			temp[0] = judge_contain(current_car[0].A_x, current_car[0].A_y,
				already_car[i].A_x, already_car[i].A_y,
				already_car[i].B_x, already_car[i].B_y,
				already_car[i].C_x, already_car[i].C_y,
				already_car[i].D_x, already_car[i].D_y);
			temp[1] = judge_contain(current_car[0].B_x, current_car[0].B_y,
				already_car[i].A_x, already_car[i].A_y,
				already_car[i].B_x, already_car[i].B_y,
				already_car[i].C_x, already_car[i].C_y,
				already_car[i].D_x, already_car[i].D_y);
			temp[2] = judge_contain(current_car[0].C_x, current_car[0].C_y,
				already_car[i].A_x, already_car[i].A_y,
				already_car[i].B_x, already_car[i].B_y,
				already_car[i].C_x, already_car[i].C_y,
				already_car[i].D_x, already_car[i].D_y);
			temp[3] = judge_contain(current_car[0].D_x, current_car[0].D_y,
				already_car[i].A_x, already_car[i].A_y,
				already_car[i].B_x, already_car[i].B_y,
				already_car[i].C_x, already_car[i].C_y,
				already_car[i].D_x, already_car[i].D_y);
			if (temp[0] == 1 && temp[1] == 1 && temp[2] == 1 && temp[3] == 1)
			{
				result = 1;
				break;
			}
		}

		//判断现有车辆是否包含已有车辆
		for (int i = 0; i < (int)already_car.size(); i++)
		{
			temp0[0] = judge_contain(already_car[i].A_x, already_car[i].A_y,
				current_car[0].A_x, current_car[0].A_y,
				current_car[0].B_x, current_car[0].B_y,
				current_car[0].C_x, current_car[0].C_y,
				current_car[0].D_x, current_car[0].D_y);
			temp0[1] = judge_contain(already_car[i].B_x, already_car[i].B_y,
				current_car[0].A_x, current_car[0].A_y,
				current_car[0].B_x, current_car[0].B_y,
				current_car[0].C_x, current_car[0].C_y,
				current_car[0].D_x, current_car[0].D_y);
			temp0[2] = judge_contain(already_car[i].C_x, already_car[i].C_y,
				current_car[0].A_x, current_car[0].A_y,
				current_car[0].B_x, current_car[0].B_y,
				current_car[0].C_x, current_car[0].C_y,
				current_car[0].D_x, current_car[0].D_y);
			temp0[3] = judge_contain(already_car[i].D_x, already_car[i].D_y,
				current_car[0].A_x, current_car[0].A_y,
				current_car[0].B_x, current_car[0].B_y,
				current_car[0].C_x, current_car[0].C_y,
				current_car[0].D_x, current_car[0].D_y);
			if (temp0[0] == 1 && temp0[1] == 1 && temp0[2] == 1 && temp0[3] == 1)
			{
				result = 1;
				break;
			}
		}

		return result;
	}

	//函数：判断正在布列车辆与所有已经布列车辆是否发生碰撞(结果为0=没有相交，结果为1=相交)
	int judge_collision(vector<current_coordinate>current_car, vector<already_coordinate>already_car)
	{
		int result = 0;
		int result_copy = 0;
		int temp[16] = { 0 };
		for (unsigned int i = 0; i < already_car.size(); i++)
		{
			temp[0] = judge_intersect(current_car[0].A_x, current_car[0].A_y, current_car[0].B_x, current_car[0].B_y,
				already_car[i].A_x, already_car[i].A_y, already_car[i].B_x, already_car[i].B_y);
			temp[1] = judge_intersect(current_car[0].A_x, current_car[0].A_y, current_car[0].B_x, current_car[0].B_y,
				already_car[i].B_x, already_car[i].B_y, already_car[i].C_x, already_car[i].C_y);
			temp[2] = judge_intersect(current_car[0].A_x, current_car[0].A_y, current_car[0].B_x, current_car[0].B_y,
				already_car[i].C_x, already_car[i].C_y, already_car[i].D_x, already_car[i].D_y);
			temp[3] = judge_intersect(current_car[0].A_x, current_car[0].A_y, current_car[0].B_x, current_car[0].B_y,
				already_car[i].D_x, already_car[i].D_y, already_car[i].A_x, already_car[i].A_y);

			temp[4] = judge_intersect(current_car[0].B_x, current_car[0].B_y, current_car[0].C_x, current_car[0].C_y,
				already_car[i].A_x, already_car[i].A_y, already_car[i].B_x, already_car[i].B_y);
			temp[5] = judge_intersect(current_car[0].B_x, current_car[0].B_y, current_car[0].C_x, current_car[0].C_y,
				already_car[i].B_x, already_car[i].B_y, already_car[i].C_x, already_car[i].C_y);
			temp[6] = judge_intersect(current_car[0].B_x, current_car[0].B_y, current_car[0].C_x, current_car[0].C_y,
				already_car[i].C_x, already_car[i].C_y, already_car[i].D_x, already_car[i].D_y);
			temp[7] = judge_intersect(current_car[0].B_x, current_car[0].B_y, current_car[0].C_x, current_car[0].C_y,
				already_car[i].D_x, already_car[i].D_y, already_car[i].A_x, already_car[i].A_y);

			temp[8] = judge_intersect(current_car[0].C_x, current_car[0].C_y, current_car[0].D_x, current_car[0].D_y,
				already_car[i].A_x, already_car[i].A_y, already_car[i].B_x, already_car[i].B_y);
			temp[9] = judge_intersect(current_car[0].C_x, current_car[0].C_y, current_car[0].D_x, current_car[0].D_y,
				already_car[i].B_x, already_car[i].B_y, already_car[i].C_x, already_car[i].C_y);
			temp[10] = judge_intersect(current_car[0].C_x, current_car[0].C_y, current_car[0].D_x, current_car[0].D_y,
				already_car[i].C_x, already_car[i].C_y, already_car[i].D_x, already_car[i].D_y);
			temp[11] = judge_intersect(current_car[0].C_x, current_car[0].C_y, current_car[0].D_x, current_car[0].D_y,
				already_car[i].D_x, already_car[i].D_y, already_car[i].A_x, already_car[i].A_y);

			temp[12] = judge_intersect(current_car[0].D_x, current_car[0].D_y, current_car[0].A_x, current_car[0].A_y,
				already_car[i].A_x, already_car[i].A_y, already_car[i].B_x, already_car[i].B_y);
			temp[13] = judge_intersect(current_car[0].D_x, current_car[0].D_y, current_car[0].A_x, current_car[0].A_y,
				already_car[i].B_x, already_car[i].B_y, already_car[i].C_x, already_car[i].C_y);
			temp[14] = judge_intersect(current_car[0].D_x, current_car[0].D_y, current_car[0].A_x, current_car[0].A_y,
				already_car[i].C_x, already_car[i].C_y, already_car[i].D_x, already_car[i].D_y);
			temp[15] = judge_intersect(current_car[0].D_x, current_car[0].D_y, current_car[0].A_x, current_car[0].A_y,
				already_car[i].D_x, already_car[i].D_y, already_car[i].A_x, already_car[i].A_y);


			for (int i = 0; i < 16; i++)
			{
				if (temp[i] == 1)
				{
					result = 1;
					break;
				}
			}
			if (result == 1)
			{
				break;
			}
		}
		result_copy = judge_carscontain(current_car, already_car);
		if (result_copy == 1)
		{
			result = 1;
		}

		return result;
	}
public:
	ConfigurationPlanCreator()
	{
		//从理论上判断布列空间能够摆放得下
		int skip = 0;
		double total_area = 0;
		//载具的长度，两类：9.6 3.4 和8.2 3.4 这里需要将载具的长宽都膨胀
		//数组下标对应载具类型：2代表长9.6宽3.4    3 4 代表长8.2宽3.4
		double length[5] = { 0,0,10.4 ,9 ,9 };
		double width[5] = { 0,0,4.2,4.2,4.2 };
		for (unsigned i = 0; i < Obstacle.size(); i++)
		{
			total_area = total_area + (Obstacle[i].car_length * Obstacle[i].car_width);
		}
		for (unsigned i = 0; i < Cars.size(); i++)
		{
			total_area = total_area + (length[Cars[i].vehicle_type] * width[Cars[i].vehicle_type]);
		}
		for (unsigned i = 0; i < Hovercraft.size(); i++)
		{
			for (unsigned j = 0; j < Hovercraft[i].vehicles.size(); j++)
			{
				total_area = total_area + (length[Hovercraft[i].vehicles[j].vehicle_type] * width[Hovercraft[i].vehicles[j].vehicle_type]);
			}
		}
		if (total_area > (workshop_length * workshop_width))
		{
			skip = 1;
		}


		if (skip == 0)
		{
			//根据障碍物的长宽以及角度，将障碍物视作已布列车辆，摆放进布列空间
			if (Obstacle.size() != 0)
			{
				for (unsigned int i = 0; i < Obstacle.size(); i++)
				{
					if (Obstacle[i].angle == 0)
					{
						already_car.push_back
						({ Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2,
							Obstacle[i].center_coordinate_x + Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2,
							Obstacle[i].center_coordinate_x + Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_width / 2,
							Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_width / 2 });

						//测试用
						//x坐标输出
						//cout << "x=[";
						//cout << Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2 << "," << Obstacle[i].center_coordinate_x + Obstacle[i].car_length / 2 << ","
						//	<< Obstacle[i].center_coordinate_x + Obstacle[i].car_length / 2 << "," << Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2 << "," << Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2 << "]" << endl;


						////y坐标输出
						//cout << "y=[";
						//cout << Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2 << "," << Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2 << ","
						//	<< Obstacle[i].center_coordinate_y - Obstacle[i].car_width / 2 << "," << Obstacle[i].center_coordinate_y - Obstacle[i].car_width / 2 << "," << Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2 << "]" << endl;


					}
					else if (Obstacle[i].angle == 90)
					{
						already_car.push_back
						({ Obstacle[i].center_coordinate_x - Obstacle[i].car_width / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_length / 2,
							Obstacle[i].center_coordinate_x - Obstacle[i].car_width / 2 ,Obstacle[i].center_coordinate_y + Obstacle[i].car_length / 2 ,
							Obstacle[i].center_coordinate_x + Obstacle[i].car_width / 2 ,Obstacle[i].center_coordinate_y + Obstacle[i].car_length / 2 ,
							Obstacle[i].center_coordinate_x + Obstacle[i].car_width / 2 ,Obstacle[i].center_coordinate_y - Obstacle[i].car_length / 2 });
					}
					else if (Obstacle[i].angle == 180)
					{
						already_car.push_back
						({ Obstacle[i].center_coordinate_x + Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_width / 2,
							Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_width / 2 ,
							Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2 ,
							Obstacle[i].center_coordinate_x + Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2 });
					}
					else if (Obstacle[i].angle == 270)
					{
						already_car.push_back
						({ Obstacle[i].center_coordinate_x + Obstacle[i].car_width / 2,Obstacle[i].center_coordinate_y + Obstacle[i].car_length / 2,
							Obstacle[i].center_coordinate_x + Obstacle[i].car_width / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_length / 2,
							Obstacle[i].center_coordinate_x - Obstacle[i].car_width / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_length / 2,
							Obstacle[i].center_coordinate_x - Obstacle[i].car_width / 2,Obstacle[i].center_coordinate_y + Obstacle[i].car_length / 2 });
					}
					else if (Obstacle[i].angle == 360)
					{
						already_car.push_back
						({ Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2,
							Obstacle[i].center_coordinate_x + Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y + Obstacle[i].car_width / 2,
							Obstacle[i].center_coordinate_x + Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_width / 2,
							Obstacle[i].center_coordinate_x - Obstacle[i].car_length / 2,Obstacle[i].center_coordinate_y - Obstacle[i].car_width / 2 });
					}
					else
					{
						double L = 0;
						L = sqrt(pow(Obstacle[i].car_width / 2, 2) + pow(Obstacle[i].car_length / 2, 2));
						double Theta = 0, A_Theta = 0, B_Theta = 0, C_Theta = 0, D_Theta = 0, Obstacle_Theta = 0;
						Theta = atan(Obstacle[i].car_width / Obstacle[i].car_length);
						Obstacle_Theta = (Obstacle[i].angle * M_PI / 180);
						A_Theta = M_PI - Theta + Obstacle_Theta;
						B_Theta = Theta + Obstacle_Theta;
						C_Theta = 2 * M_PI - Theta + Obstacle_Theta;
						D_Theta = M_PI + Theta + Obstacle_Theta;
						double A_x = 0, A_y = 0, B_x = 0, B_y = 0, C_x = 0, C_y = 0, D_x = 0, D_y = 0;
						double* p;
						p = calculate_coordinate(Obstacle[i].center_coordinate_x, Obstacle[i].center_coordinate_y, L, A_Theta);
						A_x = *p;
						A_y = *(p + 1);
						p = calculate_coordinate(Obstacle[i].center_coordinate_x, Obstacle[i].center_coordinate_y, L, B_Theta);
						B_x = *p;
						B_y = *(p + 1);
						p = calculate_coordinate(Obstacle[i].center_coordinate_x, Obstacle[i].center_coordinate_y, L, C_Theta);
						C_x = *p;
						C_y = *(p + 1);
						p = calculate_coordinate(Obstacle[i].center_coordinate_x, Obstacle[i].center_coordinate_y, L, D_Theta);
						D_x = *p;
						D_y = *(p + 1);
						delete[] p;
						already_car.push_back({ A_x ,A_y,B_x,B_y,C_x,C_y,D_x,D_y });
						//cout << A_x << "," << A_y << "," << B_x << "," << B_y << "," << C_x << "," << C_y << "," << D_x << "," << D_y << endl;
					}
				}
			}

			//针对不在气垫船上的车，生成进入顺序
			vector<int>order1;
			srand((int)time(0));
			vector<int>flags1;
			for (unsigned int i = 0; i < Cars.size(); i++)
			{
				flags1.push_back(0);
			}
			int temp = 0;
			for (unsigned int i = 0; i < Cars.size(); i++)
			{
				temp = rand() % (Cars.size());
				while (flags1[temp])
				{
					temp = rand() % (Cars.size());
				}
				flags1[temp] = 1;
				order1.push_back(temp);
			}

			//根据生成的顺序，开始布列
			//载具的长度，两类：9.6 3.4 和8.2 3.4 这里将载具的长宽都膨胀
			//数组下标对应载具类型：2代表长9.6宽3.4    3 4 代表长8.2宽3.4
			double length[5] = { 0,0,10.4 ,9 ,9 };
			double width[5] = { 0,0,4.2,4.2,4.2 };
			double A_x = 0;
			double A_y = 0;
			double B_x = 0;
			double B_y = 0;
			double C_x = 0;
			double C_y = 0;
			double D_x = 0;
			double D_y = 0;
			double temp_x = 72;
			double temp_y = 0;
			for (unsigned int i = 0; i < Cars.size(); i++)
			{
				int j = order1[i];
				//给予初始化坐标
				C_x = temp_x;
				C_y = temp_y;
				A_x = C_x - length[Cars[j].vehicle_type];
				A_y = C_y + width[Cars[j].vehicle_type];
				B_x = C_x;
				B_y = C_y + width[Cars[j].vehicle_type];
				D_x = C_x - length[Cars[j].vehicle_type];
				D_y = C_y;
				if (A_y > 18.2)
				{
					A_x -= 0.1;
					B_x -= 0.1;
					C_x -= 0.1;
					D_x -= 0.1;

					A_y = width[Cars[j].vehicle_type];
					B_y = width[Cars[j].vehicle_type];
					C_y = 0;
					D_y = 0;
				}
				if (A_x < 0)
				{
					result.clear();
					goto label_stop;
				}
				current_car.push_back({ A_x,A_y,B_x,B_y,C_x,C_y,D_x,D_y });

				//开始布列
				if (judge_collision(current_car, already_car) == 1)
				{
					while (!(current_car[0].A_x > -0.1 - 1e-10 && current_car[0].A_x < -0.1 + 1e-10))
					{
						while (!(current_car[0].A_y > 18.2 - 1e-10 && current_car[0].A_y < 18.2 + 1e-10))
						{
							current_car[0].A_y += 0.1;
							current_car[0].B_y += 0.1;
							current_car[0].C_y += 0.1;
							current_car[0].D_y += 0.1;
							if (judge_collision(current_car, already_car) == 0)
							{
								break;
							}
						}
						if (judge_collision(current_car, already_car) == 0)
						{
							break;
						}
						current_car[0].A_y = width[Cars[j].vehicle_type];
						current_car[0].B_y = width[Cars[j].vehicle_type];
						current_car[0].C_y = 0;
						current_car[0].D_y = 0;
						current_car[0].A_x -= 0.1;
						current_car[0].B_x -= 0.1;
						current_car[0].C_x -= 0.1;
						current_car[0].D_x -= 0.1;
						if (judge_collision(current_car, already_car) == 0)
						{
							break;
						}
					}
				}

				//布列完成
				temp_x = current_car[0].B_x;
				temp_y = current_car[0].B_y;

				already_car.push_back({ current_car[0].A_x,current_car[0].A_y,current_car[0].B_x,current_car[0].B_y,
					current_car[0].C_x,current_car[0].C_y,current_car[0].D_x,current_car[0].D_y });
				current_car.pop_back();

				int id = Cars[order1[i]].vehicle_id;
				unsigned int SerialNumber = i + 1;
				double coordinate_x = (already_car[Obstacle.size() + i].A_x + already_car[Obstacle.size() + i].B_x) / 2;
				double coordinate_y = (already_car[Obstacle.size() + i].A_y + already_car[Obstacle.size() + i].C_y) / 2;
				double Direction = 0;
				Layout_Plan_End cur;
				cur.direction = Direction;
				cur.end_coordinates_x = coordinate_x;
				cur.end_coordinates_y = coordinate_y;
				cur.vehicle_id = id;
				cur.series_number = SerialNumber;
				result.push_back(cur);
				//result.push_back({ id,SerialNumber ,coordinate_x ,coordinate_y ,Direction });
			}

			//布列气垫船上的车辆
			if (!Hovercraft.empty())
			{
				vector<int>order2;
				for (unsigned int i = 0; i < Hovercraft.size(); i++)
				{
					order2.push_back(i);
				}
				vector<int>flags2;
				for (unsigned int i = 0; i < Hovercraft.size(); i++)
				{
					flags2.push_back(Hovercraft[i].hovercraft_id);
				}
				for (unsigned int i = 0; i < Hovercraft.size() - 1; i++)
				{
					for (unsigned int j = i + 1; j < Hovercraft.size(); j++)
					{
						if (Hovercraft[i].hovercraft_id > Hovercraft[j].hovercraft_id)
						{
							swap(flags2[i], flags2[j]);
							swap(order2[i], order2[j]);
						}
					}
				}
				int counter = 0;
				for (unsigned int i = 0; i < Hovercraft.size(); i++)
				{
					int x = order2[i];
					double A_x = 0;
					double A_y = 0;
					double B_x = 0;
					double B_y = 0;
					double C_x = 0;
					double C_y = 0;
					double D_x = 0;
					double D_y = 0;
					double temp_x = 72;
					double temp_y = 0;
					for (unsigned int j = 0; j < Hovercraft[x].vehicles.size(); j++)
					{
						//给予初始化坐标
						C_x = temp_x;
						C_y = temp_y;
						A_x = C_x - length[Cars[j].vehicle_type];
						A_y = C_y + width[Cars[j].vehicle_type];
						B_x = C_x;
						B_y = C_y + width[Cars[j].vehicle_type];
						D_x = C_x - length[Cars[j].vehicle_type];
						D_y = C_y;
						if (A_y > 18.2)
						{
							A_x -= 0.1;
							B_x -= 0.1;
							C_x -= 0.1;
							D_x -= 0.1;

							A_y = width[Cars[j].vehicle_type];
							B_y = width[Cars[j].vehicle_type];
							C_y = 0;
							D_y = 0;
						}
						if (A_x < 0)
						{
							result.clear();
							goto label_stop;
						}
						current_car.push_back({ A_x,A_y,B_x,B_y,C_x,C_y,D_x,D_y });

						//开始布列
						if (judge_collision(current_car, already_car) == 1)
						{
							while (!(current_car[0].A_x > -0.1 - 1e-10 && current_car[0].A_x < -0.1 + 1e-10))
							{
								while (!(current_car[0].A_y > 18.2 - 1e-10 && current_car[0].A_y < 18.2 + 1e-10))
								{
									current_car[0].A_y += 0.1;
									current_car[0].B_y += 0.1;
									current_car[0].C_y += 0.1;
									current_car[0].D_y += 0.1;
									if (judge_collision(current_car, already_car) == 0)
									{
										break;
									}
								}
								if (judge_collision(current_car, already_car) == 0)
								{
									break;
								}
								current_car[0].A_y = width[Cars[j].vehicle_type];
								current_car[0].B_y = width[Cars[j].vehicle_type];
								current_car[0].C_y = 0;
								current_car[0].D_y = 0;
								current_car[0].A_x -= 0.1;
								current_car[0].B_x -= 0.1;
								current_car[0].C_x -= 0.1;
								current_car[0].D_x -= 0.1;
								if (judge_collision(current_car, already_car) == 0)
								{
									break;
								}
							}
						}

						//布列完成
						temp_x = current_car[0].B_x;
						temp_y = current_car[0].B_y;

						already_car.push_back({ current_car[0].A_x,current_car[0].A_y,current_car[0].B_x,current_car[0].B_y,
							current_car[0].C_x,current_car[0].C_y,current_car[0].D_x,current_car[0].D_y });
						current_car.pop_back();

						int id = Hovercraft[x].vehicles[j].vehicle_id;
						unsigned int SerialNumber = Obstacle.size() + Cars.size() + counter;
						double coordinate_x = (already_car[Obstacle.size() + Cars.size() + counter].A_x + already_car[Obstacle.size() + Cars.size() + counter].B_x) / 2;
						double coordinate_y = (already_car[Obstacle.size() + Cars.size() + counter].A_y + already_car[Obstacle.size() + Cars.size() + counter].C_y) / 2;
						double Direction = 0;
						Layout_Plan_End cur;
						cur.direction = Direction;
						cur.end_coordinates_x = coordinate_x;
						cur.end_coordinates_y = coordinate_y;
						cur.vehicle_id = id;
						cur.series_number = SerialNumber;
						result.push_back(cur);
						counter++;
					}
				}
			}

		label_stop:
			skip = 0;
		}
	}
};


//************************************
// Method:    ConfigurationPlanCreation
// FullName:  ConfigurationPlanCreation
// Access:    public 
// Returns:   std::vector<Layout_Plan_End> 布列方案，各个载具的最终布列的位置及方向
// Qualifier:生成布列方案
// Parameter: vector<cars_info> others_car_info	不在气垫船的载具数组
// Parameter: std::vector<hovercraft_info> Hovercrafts	气垫船数组
// Parameter: std::vector<obstacles_info> obstacle_cars	障碍物载具数组
//************************************
std::vector<Layout_Plan_End> ConfigurationPlanCreation(const std::vector<std::vector<int>>& others_car_info, std::vector<hovercraft_info> hovercrafts, std::vector<Car_Info> obstacle_cars)
{
	//清除历史的数据
	Cars.clear();
	Hovercraft.clear();
	Obstacle.clear();
	result.clear();
	//获取输入
	for (auto& car_info_tmp : others_car_info)
	{
		cars_info info_car;
		info_car.vehicle_id = car_info_tmp.at(0);
		info_car.vehicle_type = car_info_tmp.at(1);
		Cars.push_back(info_car);
	}
	Hovercraft = hovercrafts;
	for (auto & obstcle_car_info : obstacle_cars)
	{
		obstacles_info tmp_info;
		tmp_info.angle = obstcle_car_info.angle;
		tmp_info.car_length = obstcle_car_info.car_length;
		tmp_info.car_width = obstcle_car_info.car_width;
		tmp_info.center_coordinate_x = obstcle_car_info.car_center.x;
		tmp_info.center_coordinate_y = obstcle_car_info.car_center.y;
		Obstacle.push_back(tmp_info);
	}
	//运算
	ConfigurationPlanCreator cpc;


	//根据每个载具的终点到起点的距离进行排序——升序
	//首先获取所有载具的路径长度
	vector<pair<int, int>> vehicle_id_len;
	PPoint PP_start(9.1, 0);
	for (auto car_end : result) {
		auto vehicle_id = car_end.vehicle_id;
		PPoint PP_end(car_end.end_coordinates_x, car_end.end_coordinates_y);
		//使用A星算法路径搜索器生成栅格路径
		Astar astar(91, 360, obstacle_cars);
		
		//设置中心点栅格与边界栅格的距离,这里为了更好的找到路径，将载具“缩小”了
		double car_width = 1.2, car_length = 2.2;
		astar.SetCarInfo(5, 5, 4, 4);
		
		//设置与障碍物的安全栅格距离
		astar.SetCollisionRadius(2);
		//获取起点与终点,并将将起点与终点的直角坐标转换为栅格坐标
		auto start = PPointToPoint(PP_start);
		auto end = PPointToPoint(PP_end);
		//A*算法找寻路径
		list<Point> path;
		path = astar.GetPath(start, end, false);

		//找不到栅格路径，记该载具的路径长度为-1
		if (path.empty())
			vehicle_id_len.push_back(make_pair(vehicle_id, -1));
		else
		{
			//使用导引点生成器生成导引点序列
			GuidePointGenerator gpg(obstacle_cars, car_length, car_width);
			gpg.GenerateGuidePointsByPathSearch(start, end, path);
			auto guide_points = gpg.GetGuidePoints();
			int path_len = 0;
			for (int i = 1; i < guide_points.size(); ++i) {
				path_len += LengthOfTwoPoints(guide_points[i - 1], guide_points[i]);
			}
			vehicle_id_len.push_back(make_pair(vehicle_id, path_len));
		}

	}
	//然后将其根据长度排序
	sort(vehicle_id_len.begin(), vehicle_id_len.end(), [](pair<int,int>& a, pair<int, int>& b) {
		return a.second > b.second;	
	});
	
	//最后重新设置布列方案中 的每个载具的布列顺序
	for (int i = 0; i < vehicle_id_len.size(); ++i) {
		for (auto& info : result) {
			if (info.vehicle_id == vehicle_id_len[i].first) {
				info.series_number = i + 1;
				break;
			}
		}
	}



	//清除历史的数据
	Cars.clear();
	Hovercraft.clear();
	Obstacle.clear();

	return result;
}



