/*!
 * \file JsonFuctions.h
 * \date 2021/01/9 10:20
 *
 * \author lzk
 * Contact: 1274962676@qq.com
 *
 * \brief Json格式数据解析与封装函数模块
 *
 * TODO: long description
 *		（1）实现将各种json数据进行解析与打包；
  *		（2）给项目中的WebInterface的接口提供json的解析打包服务；
 * \note
*/
#pragma once
#include "json/json.h"
#include "CLogFile.h"
#include <iostream>
#include <unordered_map>
#include "Classes.h"
//json解析与打包示例
std::string createJson();
bool parseJson(const std::string &info);
void JsonTest();
//实际解析函数
void ParseJsonForLayoutPlan(const std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector < std::vector<std::vector<int>>>& cars_info,double& len_unit, std::unordered_map<int, int>& cars_id_type);
std::string CreateJsonForLayoutPlan(std::vector<Layout_Plan_End>& layout_plan, double& len_unit);
void ParseJsonForIntelligentPathPlan(const std::string &info, std::vector<Car_Info>&  obstacles_info, Ipp_Input& car_info, double& len_unit);
std::string CreateJsonForIntelligentPathPlan(std::vector<PPoint>& guide_points,int path_status,double cost_time, double& len_unit);
void ParseJsonForPathPlanCheck(const std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector<PPoint>& guide_points,Ipp_Input& car_info, double& len_unit);
std::string CreateJsonForPathPlanCheck(std::vector<int>& bad_guide_points, int path_status, double cost_time, double& len_unit);
void ParseJsonForSimulation_Demonstration(std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector<PPoint>& guide_points, Ipp_Input& car_info, double& len_unit);
std::string CreateJsonForSimulation_Demonstration(std::vector<RoutePoint> routePoints, size_t first_collison_id,  double& len_unit);
void ParseJsonForScheduling_Simulation(std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector<Car_Guide_Points>& all_guide_points , std::vector<Ipp_Input>& all_cars_info,std::vector<int>& vehicles_id, double& len_unit);
std::string CreateJsonForScheduling_Simulation(int vehicle_id, int route_point_id, int route_point_status, PPoint coordinate, double direction, double grade, double time_used, double& len_unit, int vehicle_type);


//作业态势相关的接口
void ParseJsonForVehicleLocationRequest(std::string &info, std::vector<Car_Info>&  obstacles_info, std::vector<PPoint>& guide_points, Ipp_Input& car_info, int& vihicle_id, int& coordinate_id);
std::string CreateJsonForVehicleLocationRequest(const std::vector<RoutePoint>& points, const int& route_point_status, const int& route_point_id);
void ParseJsonForHovercraftInitiativeRequest(std::string &info, double& start_x,double& end_x, int& vihicle_id, int& coordinate_id);
std::string CreateJsonForHovercraftInitiativeRequest(const std::vector<RoutePoint>& points, const int& route_point_status, const int& route_point_id);
void ParseJsonForHovercraftPassivelyRequest(std::string &info, int& left_gear,int& right_gear,double& cur_x);
std::string CreateJsonForHovercraftPassivelyRequest(double cur_x, double angle);


//测试函数
std::string createNullJson();
std::string createJsonfuncForTest1(std::vector<Car_Info>& obstacle_info);
std::string createJsonfuncForTest2(std::vector<Car_Info>& obstacle_info, std::vector<PPoint>& guide_points);
std::string createJsonfuncForTest3(std::vector<Car_Info>& obstacle_info, std::vector<PPoint>& guide_points);
std::string createJsonfuncForTest4(std::vector<Car_Info>& obstacle_info, std::vector<PPoint>& guide_points);

std::string createJsonfuncForTestVehicleLocationRequest(std::vector<Car_Info>& obstacle_info, std::vector<PPoint>& guide_points,int vehicle_id, int route_point_id);
std::string createJsonfuncForTestHovercraftInitiativeRequest(double start_x, double end_x, int vehicle_id, int route_point_id);
std::string createJsonfuncForTestHovercraftPassivelyRequest(double cur_x, int left_gear, int right_gear);


std::string createJsonfuncForNewCars();
