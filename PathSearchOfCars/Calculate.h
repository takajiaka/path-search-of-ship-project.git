/*!
 * \file Calculate.h
 * \date 2021/01/9 10:20
 *
 * \author lzk
 * Contact: 1274962676@qq.com
 *
 * \brief 辅助计算模块
 *
 * TODO: long description
 *		（1）实现有大量其它类需要的计算函数；
 * \note
*/
#pragma once
#include <math.h>
#include "GuidePointGenerator.h"
#include <algorithm>    // std::max
const double EPSINON = 0.0001;//比较浮点数

//根据载具长、宽、左后方点、倾斜角度计算载具中心点坐标
void CalculateCarCenterPoint(double car_length, double car_width, PPoint &left_back, double angle, PPoint &center);
//根据载具长、宽、左后方点、倾斜角度、膨胀边界大小计算载具所占的栅格位置并将对应栅格置位
void Caculate_SetGridesOfCar(double car_length, double car_width, PPoint car_center, double angle, double expand_boundary,std::vector<std::vector<int>>& maze);
/*根据载具长、宽、中心点位置、倾斜角度计算载具四个边界点坐标*/
void CalculateCarBoundaryPoint(double car_length, double car_width,PPoint &center,double angle, PPoint &head_left, PPoint &heel_left, PPoint &heel_right, PPoint &head_right);
/*根据两个载具的四个边界点判断载具是否发生碰撞*/
bool CalculateCollisionOfTwoCars(const std::vector<PPoint> & car1, const std::vector<PPoint> & car2);
//计算相交的函数
inline double mult(PPoint a, PPoint b, PPoint c)
{
	return (a.x - c.x)*(b.y - c.y) - (b.x - c.x)*(a.y - c.y);
}
//aa, bb为一条线段两端点 cc, dd为另一条线段的两端点 相交返回true, 不相交返回false 
bool intersect(PPoint aa, PPoint bb, PPoint cc, PPoint dd);
// Qualifier:向上取整函数，结果为正整数
inline int GetIntBig(double a)
{
 return static_cast<int>(a > (int)a ? (int)a + 1 : (int)a);
}
//
inline double LengthOfTwoPoints(PPoint a, PPoint b)
{
	return sqrt(abs((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y)));
}
//将直角坐标转换为栅格坐标
inline Point PPointToPoint(PPoint p,double width_of_gride=0.2 ,int rows= 92)
{
	int row_id=0, col_id = 0;
	row_id = static_cast<int>((rows * width_of_gride - p.y) / width_of_gride);
	col_id = static_cast<int>(p.x / width_of_gride);
	return Point(row_id, col_id);
}
//************************************
// Method:    PointToPPoint
// FullName:  PointToPPoint
// Access:    public 
// Returns:   PPoint
// Qualifier:将栅格坐标转换为直角坐标,取栅格中心点坐标
// Parameter: Point p	栅格对象
// Parameter: double width_of_gride	栅格的宽度，此处栅格为正方向
// Parameter: int rows	栅格地图的行数
//************************************
inline PPoint PointToPPoint(Point p, double width_of_gride = 0.2, int rows = 92)
{
	auto y = rows * width_of_gride - ((p.x + 1)*width_of_gride - width_of_gride / 2);//转换过的y坐标
	auto x = (p.y + 1)*width_of_gride - width_of_gride / 2;//转换过的x坐标
	return PPoint(x, y);
}
//设置载具速度和转弯半径
void SetCarSpeed_Radius(double car_speed, double radius);
//路线点生成函数
double CalculateRoutePointInStraightLine(PPoint& cur_point, PPoint& next_point, double& next_point_angle, PPoint& start_point, PPoint& end_point, double use_time=0.1);
double CalculateRoutePointInArcLine(PPoint& last_line_end_point, PPoint& cur_point, PPoint& next_point, double& next_point_angle, PPoint& end_point, PPoint& circle_center, double use_time=0.1);