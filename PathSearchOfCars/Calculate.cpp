#include "pch.h"
#include "Calculate.h"
#include "Classes.h"
#include <cmath>
using namespace std;
//************************************
// Method:    CalculateCarCenterPoint
// FullName:  CalculateCarCenterPoint
// Access:    public 
// Returns:   void
// Qualifier:根据载具长、宽、左后方点、倾斜角度计算载具中心点坐标
// Parameter: double car_length		车长
// Parameter: double car_width		车宽
// Parameter: PPoint & left_back	左后方点坐标
// Parameter: double angle			倾斜角 非弧度角 [0,360)
// Parameter: PPoint & center		计算得到的中心点坐标
//************************************
void CalculateCarCenterPoint(double car_length, double car_width, PPoint &left_back, double angle, PPoint &center)
{
	//角度为负值，错误输入，返回
	if (angle < 0) return;
	//角度转换
	angle = angle / 180 * 3.1415926;
	center.x = left_back.x + cos(angle)*car_length / 2 + sin(angle)*car_width / 2;
	center.y = left_back.y - cos(angle)*car_width / 2 + sin(angle)*car_length / 2;

}

//************************************
// Method:    CaculateGridesOfCar
// FullName:  CaculateGridesOfCar
// Access:    public 
// Returns:   void
// Qualifier:根据障碍物的位置将其覆盖的栅格置位
// Parameter: double car_length		车长
// Parameter: double car_width		车宽	
// Parameter: PPoint & left_back	车中心点的直角坐标
// Parameter: double angle			倾斜角度 非弧度角 [0,360)
// Parameter: double expand_boundary膨胀边界
// Parameter: std::vector<std::vector<int>> & maze	栅格地图
//************************************
void Caculate_SetGridesOfCar(double car_length, double car_width, PPoint car_center, double angle, double expand_boundary, std::vector<std::vector<int>>& maze)
{
	//0、计算车中心点坐标
	//PPoint car_center;
	//CalculateCarCenterPoint(car_length,car_width,left_back,angle,car_center);
	//1、根据长、宽和膨胀边界大小计算膨胀之后四个边界点的坐标：
	vector<PPoint> boundary_points(4);
	CalculateCarBoundaryPoint(car_length + expand_boundary, car_width + expand_boundary, car_center, angle, boundary_points[0], boundary_points[1], boundary_points[2], boundary_points[3]);
	//2、取车边界四个点的栅格行列下标[_x_min,_x_max]、[_y_min,_y_max]范围
	//2.1获得坐标的x、y的范围
	double max_x=0, min_x= DBL_MAX, max_y=0, min_y= DBL_MAX;
	for (const auto &point : boundary_points)
	{
		if (point.x > max_x)
			max_x = point.x;
		if (point.y > max_y)
			max_y = point.y;
		if (point.x < min_x)
			min_x = point.x;
		if (point.y < min_y)
			min_y = point.y;
	}
	// 2.2转换坐标的x、y的范围得到栅格行列下标[_x_min,_x_max]、[_y_min,_y_max]范围范围；
	size_t _y_min = static_cast<int>(min_x / 0.2);
	size_t _y_max = GetIntBig(max_x / 0.2);//向上取整
	size_t _x_min = static_cast<int>((91 * 0.2 - max_y) / 0.2);
	size_t _x_max = GetIntBig((91 * 0.2 - min_y) / 0.2);//向上取整
	// 2.3整理得到的栅格行列下标_[_x_min,_x_max]、[_y_min,_y_max]范围范围，使其不越界；
	_y_min = _y_min <= 0 ? 0 : _y_min;
	_y_max = _y_max >= maze.front().size() ? maze.front().size() - 1 : _y_max;
	_x_min = _x_min <= 0 ? 0 : _x_min;
	_x_max = _x_max >= maze.size() ? maze.size() - 1 : _x_max;
	//3、在[_x_min,_x_max]、[_y_min,_y_max]范围之内的每一个栅格，判断其栅格中心点与车中心点之间的线段是否与车的边界线段相交
	for(size_t i=_x_min;i<=_x_max;++i)
		for (size_t j = _y_min; j <= _y_max; ++j)
		{
			auto y = 91 * 0.2 - ((i + 1)*0.2 - 0.1);//转换过的y坐标
			auto x = (j + 1)*0.2 - 0.1;//转换过的x坐标
			PPoint gride_center(x,y);
			bool is_outside = false;
			for (int t = 0; t < 4; t++)
			{
				//与任意一条边相交，说明点在外面,直接判断下一点
				if (intersect(gride_center, car_center, boundary_points[t], boundary_points[(t + 1) % 4]))
				{
					is_outside = true;
					break;
				}				
			}
			//4、将所有判断为不在外面的栅格置位
			if (is_outside == false)
				maze[i][j] = 1;
		}
	
}
//************************************
// Method:    GenerateCarBoundaryPoint
// FullName:  GenerateCarBoundaryPoint
// Access:    public 
// Returns:   void
// Qualifier:根据载具中心坐标生成载具四个边界点的坐标
// Parameter: double car_length		载具长
// Parameter: double car_width		载具宽
// Parameter: PPoint & center 		中心点
// Parameter: double angle			倾斜角度 非弧度角 [0,360)
// Parameter: PPoint & front_left	左前点
// Parameter: PPoint & back_left	左后点
// Parameter: PPoint & back_right	右后点
// Parameter: PPoint & front_right	右前点
//************************************
void CalculateCarBoundaryPoint(double car_length, double car_width, PPoint &center, double angle, PPoint &front_left, PPoint &back_left, PPoint &back_right, PPoint &front_right)
{
	//角度为负值，错误输入，返回
	if (angle < 0) return;
	//角度转换
	angle = angle / 180 * 3.1415926;
	double X1, Y1, X2, Y2, X3, Y3, X4, Y4;
	//以(x,y)为中心点，不旋转的情况下四个顶点的坐标
	back_right.x = (center.x - car_length / 2);
	back_right.y = (center.y - car_width / 2);
	back_left.x = (center.x - car_length / 2);
	back_left.y = (center.y + car_width / 2);
	front_left.x = (center.x + car_length / 2);
	front_left.y = (center.y + car_width / 2);
	front_right.x = (center.x + car_length / 2);
	front_right.y = (center.y - car_width / 2);
	if (angle <= 0.00001)
		return;
	else
	{
		//按逆时针旋转角度center.x后的四个点坐标
		X1 = (back_right.x - center.x) * cos(angle) - (back_right.y - center.y) * sin(angle) + center.x;
		Y1 = (back_right.y - center.y) * cos(angle) + (back_right.x - center.x) * sin(angle) + center.y;
		X2 = (back_left.x - center.x) * cos(angle) - (back_left.y - center.y) * sin(angle) + center.x;
		Y2 = (back_left.y - center.y) * cos(angle) + (back_left.x - center.x) * sin(angle) + center.y;
		X3 = (front_left.x - center.x) * cos(angle) - (front_left.y - center.y) * sin(angle) + center.x;
		Y3 = (front_left.y - center.y) * cos(angle) + (front_left.x - center.x) * sin(angle) + center.y;
		X4 = (front_right.x - center.x) * cos(angle) - (front_right.y - center.y) * sin(angle) + center.x;
		Y4 = (front_right.y - center.y) * cos(angle) + (front_right.x - center.x) * sin(angle) + center.y;
		back_right.x = X1;
		back_right.y = Y1;
		back_left.x = X2;
		back_left.y = Y2;
		front_left.x = X3;
		front_left.y = Y3;
		front_right.x = X4;
		front_right.y = Y4;
	}
}

//************************************
// Method:    CalculateCollisionSignOfTwoCars
// FullName:  CalculateCollisionSignOfTwoCars
// Access:    public 
// Returns:   bool 是-返回true，否-返回false
// Qualifier:根据两个载具的四个边界点判断载具是否发生碰撞
// Parameter: const vector<PPoint> & car1	第一个车的四个边界点,边界点坐标顺序为，以左前角为起点的逆时针
// Parameter: const vector<PPoint> & car2	第二个车的四个边界点，边界点坐标顺序为，以左前角为起点的逆时针
//************************************
bool CalculateCollisionOfTwoCars(const vector<PPoint> & car1, const vector<PPoint> & car2)
{
	//错误输入判断
	if (car1.size() != 4 || car2.size() != 4)
	{
		cerr << "CollisionCheckOfTwoCars:车的边界点数量错误" << endl;
		return true;
	}
	//对两个车的四条边分别判断是否相交，一共16种组合
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
		{
			//任意两条边相交，返回true
			if (intersect(car1[i], car1[(i + 1) % 4], car2[j], car2[(j + 1) % 4]))
				return true;
		}
	return false;
}
//************************************
// Method:    intersect
// FullName:  intersect
// Access:    public 
// Returns:   bool
// Qualifier:aa, bb为一条线段两端点 cc, dd为另一条线段的两端点 相交返回true, 不相交返回false 
// Parameter: PPoint aa	第一条线段的端点1
// Parameter: PPoint bb	第一条线段的端点2
// Parameter: PPoint cc	第二条线段的端点1
// Parameter: PPoint dd	第二条线段的端点1
//************************************
bool intersect(PPoint aa, PPoint bb, PPoint cc, PPoint dd)
{
	if (max(aa.x, bb.x) < min(cc.x, dd.x))
	{
		return false;
	}
	if (max(aa.y, bb.y) < min(cc.y, dd.y))
	{
		return false;
	}
	if (max(cc.x, dd.x) < min(aa.x, bb.x))
	{
		return false;
	}
	if (max(cc.y, dd.y) < min(aa.y, bb.y))
	{
		return false;
	}
	if (mult(cc, bb, aa)*mult(bb, dd, aa) < 0)
	{
		return false;
	}
	if (mult(aa, dd, cc)*mult(dd, bb, cc) < 0)
	{
		return false;
	}
	return true;
}



double R = 1.5;                  //转弯半径
double V = 4.2;                  //坦克转弯时的速度，假设恒定
const double T = 0.1;
const double pi = 3.14159;
int aa;                                 //aa为1代表直线，aa为2代表弧线



typedef struct           //点坐标加半径，圆心点
{
	double x;
	double y;
	double r;
}Circle;



typedef struct           //最后所求point.next表达形式，包含坐标和角度
{
	double x;
	double y;
	double theta;
}*PNext, Next;


//atan2的升级版，加入了x等于0的情况
double atan3(double a, double b)
{
	double c;
	if (b == 0)
		c = pi / 2 * a / abs(a);
	else
		c = atan2(a, b);
	return c;
}
//轨迹点解算相关函数
inline double square(double val)
{
	return val * val;
}
inline bool equal(Circle c1, Circle last2)
{
	return (abs((c1.x) - (last2.x)) < EPSINON) && (abs((c1.y) - (last2.y)) < EPSINON) && (abs((c1.r) - (last2.r)) < EPSINON);
}
inline bool equalx(PPoint c1, PPoint last2)
{
	return (abs((c1.x) - (last2.x)) < EPSINON);
}
inline bool equaly(PPoint c1, PPoint last2)
{
	return (abs((c1.y) - (last2.y)) < EPSINON);
}
inline bool yequal(Circle c1, Circle last2)
{
	return (abs((c1.y) - (last2.y)) < EPSINON);
}
inline bool xequal(Circle c1, Circle last2)
{
	return (abs((c1.x) - (last2.x)) < EPSINON);
}

double calculatenextpoint_staightline(PNext d, PPoint a1, PPoint a2, PPoint last, double t)   //a1是线段起始点，a2是线段终止点
{
	double tt, k;
	if (equalx(a1, a2) != 1)
		k = (a2.y - a1.y) / (a2.x - a1.x);
	if (equalx(a1, a2))                           //两个点x相等，竖直移动
	{
		if (a2.y > a1.y)
		{
			d->x = last.x;
			d->y = last.y + V * t;
			d->theta = 90;
			if (d->y > a2.y)                      //这里先不输出point.next
			{
				tt = T - (d->y - a2.y) / V;           //tt为这一次位移的时长
				d->y = a2.y;                          //先将顶点作为point.next，作为下次运算的初始点
				aa = 2;                                    //下次启用弧线函数
				return tt;
			}
			else
			{
				tt = T;
				return tt;
			}
		}
		else
		{
			d->x = last.x;
			d->y = last.y - V * t;
			d->theta = 270;
			if (d->y < a2.y)                      //这里先不输出point.next
			{
				tt = T - (a2.y - d->y) / V;           //tt为这一次位移的时长
				d->y = a2.y;                          //先将顶点作为point.next，作为下次运算的初始点
				aa = 2;                                    //下次启用弧线函数
				return tt;
			}
			else
			{
				tt = T;

				return tt;
			}
		}
	}
	else if (equaly(a1, a2))                             //两个点y相等，水平移动
	{
		if (a2.x > a1.x)
		{
			d->y = last.y;
			d->x = last.x + V * t;
			d->theta = 0;
			if (d->x > a2.x)                      //这里先不输出point.next
			{
				tt = T - (d->x - a2.x) / V;           //tt为这一次位移的时长
				d->x = a2.x;                          //先将顶点作为point.next，作为下次运算的初始点
				aa = 2;                                    //下次启用弧线函数
				return tt;
			}
			else
			{
				tt = T;
				return tt;
			}
		}
		else
		{
			d->y = last.y;
			d->x = last.x - V * t;
			d->theta = 180;
			if (d->x < a2.x)                      //这里先不输出point.next
			{
				tt = T - (a2.x - d->x) / V;           //tt为这一次位移的时长
				d->x = a2.x;                          //先将顶点作为point.next，作为下次运算的初始点
				aa = 2;                                    //下次启用弧线函数
				return tt;
			}
			else
			{
				tt = T;
				return tt;
			}
		}
	}

	else if ((a2.x > a1.x) && (a2.y > a1.y))                         //直线朝向右上
	{
		d->y = last.y + sin(atan2((a2.y - a1.y), (a2.x - a1.x))) * V * t;
		d->x = last.x + cos(atan2((a2.y - a1.y), (a2.x - a1.x))) * V * t;
		d->theta = atan2((a2.y - a1.y), (a2.x - a1.x)) / pi * 180;
		if (d->y > a2.y)                                //这里先不输出point.next
		{
			tt = T - (d->y - a2.y) / sin(atan2((a2.y - a1.y), (a2.x - a1.x))) / V; //tt为这一次位移的时长
			d->x = a2.x;                          //先将顶点作为point.next，作为下次运算的初始点
			d->y = a2.y;
			aa = 2;                                    //下次启用弧线函数
			return tt;
		}
		else
		{
			tt = T;
			return tt;
		}
	}
	else if ((a2.x < a1.x) && (a2.y > a1.y))                         //直线朝向左上
	{
		d->y = last.y + sin(atan2((a2.y - a1.y), (a2.x - a1.x))) * V * t;
		d->x = last.x + cos(atan2((a2.y - a1.y), (a2.x - a1.x))) * V * t;
		d->theta = atan2((a2.y - a1.y), (a2.x - a1.x)) / pi * 180;
		if (d->y > a2.y)                                //这里先不输出point.next
		{
			tt = T - (d->y - a2.y) / sin(atan2((a2.y - a1.y), (a2.x - a1.x))) / V; //tt为这一次位移的时长
			d->x = a2.x;                          //先将顶点作为point.next，作为下次运算的初始点
			d->y = a2.y;
			aa = 2;                                    //下次启用弧线函数
			return tt;
		}
		else
		{
			tt = T;
			return tt;
		}
	}
	else if ((a2.x < a1.x) && (a2.y < a1.y))                         //直线朝向左下
	{
		d->y = last.y + sin(atan2((a2.y - a1.y), (a2.x - a1.x))) * V * t;
		d->x = last.x + cos(atan2((a2.y - a1.y), (a2.x - a1.x))) * V * t;
		d->theta = atan2((a2.y - a1.y), (a2.x - a1.x)) / pi * 180 + 360;
		if (d->y < a2.y)                                //这里先不输出point.next
		{
			tt = T - (a2.y - d->y) / sin(pi + atan2((a2.y - a1.y), (a2.x - a1.x))) / V; //tt为这一次位移的时长
			d->x = a2.x;                          //先将顶点作为point.next，作为下次运算的初始点
			d->y = a2.y;
			aa = 2;                                    //下次启用弧线函数
			return tt;
		}
		else
		{
			tt = T;
			return tt;
		}
	}
	else if ((a2.x > a1.x) && (a2.y < a1.y))                         //直线朝向右下
	{
		d->y = last.y + sin(atan2((a2.y - a1.y), (a2.x - a1.x))) * V * t;
		d->x = last.x + cos(atan2((a2.y - a1.y), (a2.x - a1.x))) * V * t;
		d->theta = atan2((a2.y - a1.y), (a2.x - a1.x)) / pi * 180 + 360;
		if (d->y < a2.y)                                //这里先不输出point.next
		{
			tt = T - (a2.y - d->y) / sin(-atan2((a2.y - a1.y), (a2.x - a1.x))) / V; //tt为这一次位移的时长
			d->x = a2.x;                          //先将顶点作为point.next，作为下次运算的初始点
			d->y = a2.y;
			aa = 2;                                    //下次启用弧线函数
			return tt;
		}
		else
		{
			tt = T;
			return tt;
		}
	}
}
//通过两圆相交计算出两个相交点坐标
int GetIntersectionPointCoordinate(PNext d1, PNext d2, Circle c1, Circle last2)
{
	double error, A, B, C, K, S, x1, y1, x2, y2, x, y;

	if (yequal(c1, last2))
	{
		x = (square((c1.x)) + square((c1.y)) + square((last2.r)) - square((last2.x)) - square((last2.y)) - square((c1.r))) / (2 * (c1.x) - 2 * (last2.x));
		A = 1;
		B = (-2) * (c1.y);
		C = square(x) + square((c1.x)) + square((c1.y)) - square((c1.r)) - 2 * (c1.x) * x;
		error = square(B) - 4 * A * C;

		if (error < 0)
			return -1;
		else
		{
			x1 = x2 = x;
			y1 = ((-B) + sqrt(error)) / (2 * A);
			y2 = ((-B) - sqrt(error)) / (2 * A);
		}
	}
	else
		if (xequal(c1, last2))
		{
			y = (square((c1.x)) + square((c1.y)) + square((last2.r)) - square((last2.x)) - square((last2.y)) - square((c1.r))) / (2 * (c1.y) - 2 * (last2.y));
			A = 1;
			B = (-2) * (c1.x);
			C = square((c1.x)) + square((c1.y)) + square(y) - square((c1.r)) - 2 * y * (c1.y);
			error = square(B) - 4 * A * C;

			if (error < 0)
				return -1;
			else
			{
				y1 = y2 = y;
				x1 = ((-B) + sqrt(error)) / (2 * A);
				x2 = ((-B) - sqrt(error)) / (2 * A);
			}
		}
		else
		{
			K = (2 * (last2.x) - 2 * (c1.x)) / (2 * (c1.y) - 2 * (last2.y));
			S = (square((c1.x)) + square((c1.y)) + square((last2.r)) - square((last2.x)) - square((last2.y)) - square((c1.r))) / (2 * (c1.y) - 2 * (last2.y));

			//解二次方程
			A = (1 + square(K));
			B = 2 * K * S - 2 * (c1.x) - 2 * (c1.y) * K;
			C = square(S) + square((c1.x)) + square((c1.y)) - square((c1.r)) - 2 * (c1.y) * S;
			error = square(B) - 4 * A * C;

			if (error < 0)
				return -1;
			else
			{
				x1 = ((-B) + sqrt(error)) / (2 * A);
				x2 = ((-B) - sqrt(error)) / (2 * A);
				y1 = K * x1 + S;
				y2 = K * x2 + S;
			}
		}
	d1->x = x1;
	d1->y = y1;

	d2->x = x2;
	d2->y = y2;
	return 1;

}


//根据前一个函数求得的两个点，通过线段的有向性判断哪个点是真正需要的下一个点，这里只判断一次，第一个不是就确定第二个是。判断依据是先看c1是在有向线段M-last2的哪一侧，再看c1是在有向线段last2-d1的哪一侧，如果是同一侧，则d1是要选择的点，否则d2就是要选择的点
void Select_one_point(PNext d1, PNext d2, PNext dd, Circle c1, Circle last2, PPoint M)
{
	double A1, B1, C1, D1, A2, B2, C2, D2, n1, n2;
	A1 = last2.y - M.y;
	B1 = M.x - last2.x;
	C1 = last2.x * M.y - M.x * last2.y;
	D1 = A1 * c1.x + B1 * c1.y + C1;
	if (D1 < 0)                                                       //点在有向线段左侧
		n1 = -1;
	else if (D1 > 0)                                                    //点在有向线段右侧
		n1 = 1;
	else n1 = 0;                                                       //点在有向线段上
	A2 = d1->y - last2.y;
	B2 = last2.x - d1->x;
	C2 = d1->x * last2.y - last2.x * d1->y;
	D2 = A2 * c1.x + B2 * c1.y + C2;
	if (D2 < 0)                                                       //点在有向线段左侧
		n2 = -1;
	else if (D2 > 0)                                                    //点在有向线段右侧
		n2 = 1;
	else n2 = 0;                                                       //点在有向线段上

	if (n1 == n2)
	{
		if (n1 < 0)
		{
			//d1，逆时针
			dd->x = d1->x;
			dd->y = d1->y;
			dd->theta = atan3(dd->y - c1.y, dd->x - c1.x) / pi * 180 + 90;
			if (dd->theta < 0)
				dd->theta = dd->theta + 360;

		}

		else
		{
			//d1，顺时针
			dd->x = d1->x;
			dd->y = d1->y;
			dd->theta = atan3(dd->y - c1.y, dd->x - c1.x) / pi * 180 - 90;
			if (dd->theta < 0)
				dd->theta = dd->theta + 360;

		}
	}
	else
	{
		if (n1 < 0)
		{
			//d2，逆时针
			dd->x = d2->x;
			dd->y = d2->y;
			dd->theta = atan3(dd->y - c1.y, dd->x - c1.x) / pi * 180 + 90;
			if (dd->theta < 0)
				dd->theta = dd->theta + 360;

		}
		else
		{
			//d2，顺时针
			dd->x = d2->x;
			dd->y = d2->y;
			dd->theta = atan3(dd->y - c1.y, dd->x - c1.x) / pi * 180 - 90;
			if (dd->theta < 0)
				dd->theta = dd->theta + 360;

		}
	}
}
//判断Select_one_point所求点还在不在圆弧上，还是已经出了圆弧段。若出了圆弧，更新dd为M2，不输出坐标和角度，计算t2作为函数输出；若没出圆弧，输出dd坐标和角度，t2=T作为函数输出
double Judge_point_on_arc(PNext dd, Circle last2, PPoint M2)
{
	double a, b, tt;                   //a为last2到M2距离，b为last2到d2距离（即小圆半径长度）,若a>=b，则没出圆弧；若a<b，则出了圆弧
	a = sqrt(square(last2.y - M2.y) + square(last2.x - M2.x));
	b = sqrt(square(last2.y - dd->y) + square(last2.x - dd->x));
	if (a >= b)
	{
		tt = T;
		return tt;
	}
	else
	{
		dd->x = M2.x;
		dd->y = M2.y;
		aa = 1;
		tt = T * a / b;
		return tt;
	}
}


//************************************
// Method:    CalculateRoutePointInStraightLine
// FullName:  CalculateRoutePointInStraightLine
// Access:    public 
// Returns:   double 若不为T，则表示下一点已经超过当前线段且为下一小段的所用时间,否则表示
// Qualifier:计算在直线上的路径点
// Parameter: PPoint & cur_point	直线上的当前点坐标
// Parameter: PPoint & next_point	计算出的下一点坐标
// Parameter: double & next_point_angle	下一点坐标的方向
// Parameter: PPoint & start_point	直线线段的起点
// Parameter: PPoint & end_point	直线线段的终点
// Parameter: double use_time		当前点到达下一点的耗费时间
//************************************
double CalculateRoutePointInStraightLine(PPoint& cur_point,PPoint& next_point,double& next_point_angle,PPoint& start_point,PPoint& end_point,double use_time)
{
	//实际花费时间
	double time_used_real;
	Next d;//下一个点
	time_used_real = calculatenextpoint_staightline(&d, start_point, end_point, cur_point, use_time);
	//如果算出的时间为0，表示轨迹点为当前线段最后一点，返回0
	if (abs(time_used_real) < 0.000001)
		return 0;
	//若下一个点已经不再当前线段/弧线上了
	else if (time_used_real < T)
		return T-time_used_real;//返回下一段曲线/线段计算第一个点的所用时间
	else
	{
		//记录坐标与角度
		next_point.x = d.x;
		next_point.y = d.y;
		next_point_angle = d.theta;
	}
	return T;
}
//************************************
// Method:    CalculateRoutePointInArcLine
// FullName:  CalculateRoutePointInArcLine
// Access:    public 
// Returns:   double 剩余时间，则表示下一点已经超过当前线段且为下一小段的所用时间,否则返回T
// Qualifier:计算在圆弧上的路径点
// Parameter: PPoint & last_line_end_point 前线段的最后一个点，用于判断方向
// Parameter: PPoint & cur_point		当前点坐标
// Parameter: PPoint & next_point		计算出的下一点坐标
// Parameter: double & next_point_angle	计算出的下一点方向
// Parameter: PPoint & end_point		圆弧段终点
// Parameter: PPoint & circle_center	圆心坐标			
// Parameter: double use_time			当前点到达下一点的耗费时间
//************************************
double CalculateRoutePointInArcLine(PPoint& last_line_end_point, PPoint& cur_point, PPoint& next_point, double& next_point_angle, PPoint& end_point, PPoint& circle_center,double use_time)
{
	double time_used_real;
	//弧线函数所需点，M为前一段直线最后一个点，M1为圆弧起点，M2为圆弧终点
	Next d1, d2, dd;                      //弧线所求两组坐标，最终取一个放到dd
	Circle c1, last2;                      //弧线函数所需两相交圆的圆心坐标，c1是转弯圆弧的圆心，last2是前一个点的坐标

	c1.r = R;
	c1.x = circle_center.x;                                //大圆心
	c1.y = circle_center.y;

	last2.r = 2 * R * sin(V * use_time / 2 / R);
	last2.x = cur_point.x;                                //小圆心
	last2.y = cur_point.y;
	GetIntersectionPointCoordinate(&d1, &d2, c1, last2);    //通过两圆相交计算出两个相交点坐标
	Select_one_point(&d1, &d2, &dd, c1, last2, last_line_end_point);
	time_used_real = Judge_point_on_arc(&dd, last2, end_point);

	//如果算出的时间为0，表示轨迹点为当前圆弧最后一点，返回0
	if (abs(time_used_real) <= 0.000001)
		return 0;
	//若下一个点已经不再当前弧线上了
	else if (time_used_real < T)
		return T-time_used_real;//返回下一段曲线/线段的计算第一个点的所用时间
	else
	{
		//记录坐标与角度
		next_point.x = dd.x;
		next_point.y = dd.y;
		next_point_angle = dd.theta;
	}
	return T;
}
void SetCarSpeed_Radius(double car_speed, double radius)
{
	V = car_speed;
	R = radius;
}