#include "pch.h"
#include <iostream>
#include <vector>
#include <cstdio>
#include "../PathSearchOfCars/ConfigurationPlan.h"
#include "../PathSearchOfCars/AStar.h"
#include "../PathSearchOfCars/GuidePointGenerator.h"
#include "../PathSearchOfCars/RoutePointGenerator.h"
#include "../PathSearchOfCars/Calculate.h"

#include "../PathSearchOfCars/AStar.cpp"
#include "../PathSearchOfCars/GuidePointGenerator.cpp"
#include "../PathSearchOfCars/RoutePointGenerator.cpp"
#include "../PathSearchOfCars/Calculate.cpp"
#include "../PathSearchOfCars/pch.h"

#include <time.h>
#include <math.h>
#include <algorithm>    // std::max
void printGraphToCSV()
{

	FILE* fd = NULL;
	if ((fd = fopen("Graph.csv", "wt+")) != NULL)
	{
		//存储图信息
		for (auto row : maze)
		{
			for (auto i : row)
				fprintf(fd, "%d,", 1 - i);
			fprintf(fd, "\n");
		}
	}
	fclose(fd);
	fd = NULL;
}
TEST(TestCaseName, TestName) {
	////车舱栅格地图
	//vector<vector<int>> maze;
	////生成地图：360列 91行
	//maze.reserve(91);
	//vector<int> row(360, 0);
	//for (int i = 0; i < 91; i++)
	//	maze.push_back(row);

	////test
	//int row_id = 3, col_id = 4;
	//int _x = (row_id - 1) * 17 + 4 * row_id;
	//int _y = (col_id - 1) * 48 + 3 * col_id;
	//auto ry = 91 * 0.2 - ((_x + 1)*0.2 - 0.1);//转换过的y坐标
	//auto rx = (_y + 1)*0.2 - 0.1;//转换过的x坐标
	//PPoint test_p(rx, ry);
	////0.2828
	//Caculate_SetGridesOfCar(9.6, 3.4, test_p, 300, 0.2828, maze);
	//printGraphToCSV();
	//end test
  EXPECT_EQ(1, 1);
  EXPECT_TRUE(true);
}
